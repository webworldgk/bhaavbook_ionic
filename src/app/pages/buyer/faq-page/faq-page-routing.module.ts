import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FaqPagePage } from './faq-page.page';

const routes: Routes = [
  {
    path: '',
    component: FaqPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FaqPagePageRoutingModule {}
