import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FaqPagePageRoutingModule } from './faq-page-routing.module';

import { FaqPagePage } from './faq-page.page';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FaqPagePageRoutingModule
  ],
  declarations: [FaqPagePage]
})
export class FaqPagePageModule {}
