import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RaiseTicketPageRoutingModule } from './raise-ticket-routing.module';

import { RaiseTicketPage } from './raise-ticket.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RaiseTicketPageRoutingModule
  ],
  declarations: [RaiseTicketPage]
})
export class RaiseTicketPageModule {}
