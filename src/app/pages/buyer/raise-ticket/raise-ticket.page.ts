import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { Capacitor } from "@capacitor/core";
import { ActivatedRoute, Router } from "@angular/router";
import { LoadingController, ToastController } from "@ionic/angular";
import { ApiService } from "src/app/service/api.service";
import { UserData } from "../../../providers/user-data";
import { Storage } from "@ionic/storage";
import { NgForm, Validators } from "@angular/forms";

@Component({
  selector: "app-raise-ticket",
  templateUrl: "./raise-ticket.page.html",
  styleUrls: ["./raise-ticket.page.scss"],
})
export class RaiseTicketPage implements OnInit {
  supportTicket: any;
  defaultName: any;
  description: any;
  galleryFile: any;
  profile_pic_preview: "";
  actualFile: any;
  isValidated: boolean;
  acceptableExtensions = "image/*";
  photoViewer: any;
  type: any;

  customActionCommonOptions: any = {
    header: "Choose Type",
  };

  constructor(
    public router: Router,
    public api: ApiService,
    private http: HttpClient,
    public userData: UserData,
    public storage: Storage,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController
  ) {}

  ngOnInit() {}
  ionViewDidEnter() {
    this.prepareMasters();
  }

  async prepareMasters() {
    if (this.api.supportTicketType.length == 0) {
      const storeVariables = true;
      this.api.doStoreMasters(storeVariables);
    }
    if (this.api.unitList.length == 0) {
      const storeVariables = true;
      this.api.doStoreMasters(storeVariables);
    }
  }

  submitTicket(form: NgForm) {
    //console.log(`form`, form);

    this.isValidated = true;
    if (!this.supportTicket) {
      this.api.presentValidateToast("Type is required.", 4000, "danger");
      this.isValidated = false;
    }

    if (!this.description) {
      this.api.presentValidateToast("Description is required.", 4000, "danger");
      this.isValidated = false;
    }

  

    if (this.isValidated) {
      const postParams = new FormData();
      postParams.append("type_id", this.supportTicket);
      postParams.append("description", this.description);
      postParams.append("file_name", this.actualFile, this.actualFile?.name);

      console.log(`postParams`, postParams);

      this.addTicket(postParams, form);
    }
  }

  async addTicket(postParams: any, form: NgForm) {
    if (postParams) {
      console.log(`postParams`, postParams);
      await this.api.showLoader("Saving...");

      const token = await this.userData.getToken();
    
      const apiName = `api/v1/tickets`;
      const apiUrl = this.api.restApiUrl + apiName;
      const httpOptions = {
        headers: new HttpHeaders({ Authorization: token }),
      };

      this.http.post(apiUrl, postParams, httpOptions).subscribe(
        (response: any) => {
          console.log("addTicket response", response);
          if (response.status == 1) {
            this.api.presentSuccessToast(response.message);
            this.api.dismissLoader();
            this.router.navigateByUrl(`/supports`);
            form.reset();
          } else if (response.statusCode === 401) {
            this.api.clearStorageAndRedirectToLogin(response.message);
            this.api.dismissLoader();
          } else {
            this.api.presentValidateToast(response.message);
            this.api.dismissLoader();
          }
        },
        (error: any) => {
          console.log("error", error.error);
          this.api.presentValidateToast(error.error.message);
          this.api.dismissLoader();
        }
      );
    }
  }

  onFileChange(fileChangeEvent: any) {
    if (fileChangeEvent.target.files[0]) {
      this.actualFile = fileChangeEvent.target.files[0];
      const reader = new FileReader();
      reader.onload = (event: any) => {
        this.profile_pic_preview = event.target.result;
      };
      reader.readAsDataURL(fileChangeEvent.target.files[0]);
      console.log(`this.actualFile`, this.actualFile);
    }
  }

  previewImage(photoCode: number) {
    console.log(`photoCode`, photoCode);
    if (Capacitor.isNativePlatform()) {
      if (photoCode === 1 && this.profile_pic_preview) {
        this.photoViewer.show(this.profile_pic_preview, "Profile Picture", {
          share: false,
        });
      }
    }
  }
  changeSubType(event: any) {
    const selectedSubType = event.detail.value;
    console.log(`event.detail.value`, event.detail.value);
    this.api.supportTicketType.find((item) => {
      if (selectedSubType > 1 && item.id == selectedSubType) {
        this.defaultName = item.name;
      }
    });
  }
}
