import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../../providers/user-data';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-inquiry-list',
  templateUrl: './inquiry-list.page.html',
  styleUrls: ['./inquiry-list.page.scss'],
})
export class InquiryListPage implements OnInit {

  data: any;

 
  constructor(
    public router: Router,
    public api: ApiService,
    public userData: UserData,
    public alertCtrl: AlertController,
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.getInquiryDetails();
  }

  async getInquiryDetails() {

    await this.api.showLoader();

    const token = await this.userData.getToken();
    console.log('token', token)
    this.api.getInquiryInfo(token).subscribe((response: any) => {
      console.log('getInquiryInfo response', response);
      if (response.status === 1) {
        this.data = response.data;
        // this.product =   (this.product && Array.isArray(this.product)) ? this.product.concat(response.data): response.data;

        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }

  async condirmRemoveDailyReport(id: any) {
    const alert = await this.alertCtrl.create({
      header: 'Confirm Remove',
      message: 'Are you sure you want to remove?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: (no) => {
            console.log('Confirm Cancel: ', no);
          }
        }, {
          text: 'Remove',
          handler: () => {
            console.log('Confirm Okay');
            this.removeDailyReport(id);
          }
        }
      ]
    });

    await alert.present();
  }

  async removeDailyReport(id: any) {

    if (id) {

      await this.api.showLoader('Removing...');

      const token = await this.userData.getToken();
      this.api.deleteInquiry(token, id).subscribe((response: any) => {
        console.log('deleteDailyReport response', response);
        if (response.status == 1) {
          this.api.presentSuccessToast(response.message);
          localStorage.setItem('refreshContents', '1');
          this.getInquiryDetails();
          this.api.dismissLoader();
        } else if (response.statusCode === 401) {
          this.api.clearStorageAndRedirectToLogin(response.message);
          this.api.dismissLoader();
        } else {
          this.api.presentValidateToast(response.message);
          this.api.dismissLoader();
        }
      });
    }
  }


}
