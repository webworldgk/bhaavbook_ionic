import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Capacitor } from '@capacitor/core';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../../providers/user-data';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  user: any;
  summary: any;
  attendance: any;
  data: any;
  // isPunchIn: number;

  // punchState = {
  //   punchIn: null,
  //   punchOut: null,
  // };

  userPermissions = {
    attendancePunch: 0,
    dailyReport: 0,
    rawCustomer: 0,
    technicalLead: 0,
    attendanceList: 0,
    addDailyReport: 0,
    addRawCustomer: 0,
    addproduct : 0,
  };

  constructor(
    public router: Router,
    public api: ApiService,
    public userData: UserData,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private photoViewer: PhotoViewer,
    public alertCtrl: AlertController,
  ) { }

  ngOnInit() {
    // this.api.getStart();
    // if (localStorage.getItem('refreshContents') !== '1') {
    //   this.getDashboardDetails();
    // }
  }


  // ionViewDidEnter() {
  //   this.getProfileDetails();
  // }

  // async getProfileDetails() {

  //   const loading = await this.loadingCtrl.create({
  //     message: "Please wait...",
  //   });

  //   await loading.present();

  //   const token = await this.userData.getToken();
  //   console.log('token', token)
  //   this.api.getProfileInfo(token).subscribe((response: any) => {
  //     console.log('getProfileInfo response', response);
  //     if (response.status == 1) {
  //       this.data = response.data;
  //       // this.user = response.data.user;
  //       // this.summary = response.data.summary;
  //       // this.attendance = response.data.attendance;
  //       loading.dismiss();
  //     } else {
  //       this.presentToast(response.message);
  //       loading.dismiss();
  //     }
  //   });
  // }

  // async presentToast(messageStr: string, durationMSec = 2000) {
  //   const toast = await this.toastCtrl.create({
  //     message: messageStr,
  //     duration: durationMSec,
  //   });

  //   toast.present();
  // }


  async getDashboardDetails() {

    await this.api.showLoader();

    const token = await this.userData.getToken();
    // console.log('token', token);

    this.api.getDashboardInfo(token).subscribe((response: any) => {
      console.log('getDashboardInfo response', response);
      if (response.status === 1) {
        this.data = response.data;
        // this.summary = response.data.summary;
        // this.attendance = response.data.attendance;
        // this.isPunchIn = response.data.attendance.isPunchIn;
        console.log(this.data);
        this.api.dismissLoader();

        // reset in order to not refresh again
        localStorage.setItem('refreshContents', '0');

      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();

      } else if (response.message === 'Server Error') {
        this.api.serverErrorToast();
        this.api.dismissLoader();

      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });

  }

  async confirmLogout() {
    const alert = await this.alertCtrl.create({
      header: 'Confirm sign out',
      message: 'Are you sure you want to sign out?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: (no) => {
            console.log('Confirm Cancel: ', no);
          }
        }, {
          text: 'Sign Out',
          handler: () => {
            console.log('Confirm Okay');
            this.api.logout();
          }
        }
      ]
    });

    await alert.present();
  }

  editProfile() {
    this.router.navigate(['/update-profile-page']);
  }

  doRefresh(event) {
    console.log('Begin async operation');
    this.getDashboardDetails();
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }
  previewImage() {
    if (Capacitor.isNativePlatform()) {
      if (this.user.picture) {
        this.photoViewer.show(this.user.picture, 'Profile Picture', { share: false });
      }
    }
  }



}
