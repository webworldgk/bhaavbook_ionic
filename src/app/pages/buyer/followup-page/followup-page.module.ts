import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FollowupPagePageRoutingModule } from './followup-page-routing.module';

import { FollowupPagePage } from './followup-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FollowupPagePageRoutingModule
  ],
  declarations: [FollowupPagePage]
})
export class FollowupPagePageModule {}
