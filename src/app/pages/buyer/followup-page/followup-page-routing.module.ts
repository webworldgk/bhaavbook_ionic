import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FollowupPagePage } from './followup-page.page';

const routes: Routes = [
  {
    path: '',
    component: FollowupPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FollowupPagePageRoutingModule {}
