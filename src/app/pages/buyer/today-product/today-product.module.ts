import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TodayProductPageRoutingModule } from './today-product-routing.module';

import { TodayProductPage } from './today-product.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TodayProductPageRoutingModule
  ],
  declarations: [TodayProductPage]
})
export class TodayProductPageModule {}
