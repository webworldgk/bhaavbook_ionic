import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-today-product',
  templateUrl: './today-product.page.html',
  styleUrls: ['./today-product.page.scss'],
})
export class TodayProductPage implements OnInit {

  constructor(
    public atrCtrl: AlertController,
  ) { }

  ngOnInit() {
  }

  async showPromptAlert() {
    const alert = this.atrCtrl.create({
      header: 'Enquiry Form',
      inputs: [
        {
          name: 'username',
          placeholder: 'Enter Quantity'
        },
        {
          name: 'password',
          placeholder: 'Enter Description',
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('You Clicked on Cancel');
          }
        },
        {
          text: 'Send',
          //  handler: data => {
          // if (User.isValid(data.username, data.password)) {
          //   // login is valid
          // } else {
          //   // invalid login
          //   return false;
          // }
        }
      ]
    });
     (await alert).present();
  }



}
