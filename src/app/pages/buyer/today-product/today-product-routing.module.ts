import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TodayProductPage } from './today-product.page';

const routes: Routes = [
  {
    path: '',
    component: TodayProductPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TodayProductPageRoutingModule {}
