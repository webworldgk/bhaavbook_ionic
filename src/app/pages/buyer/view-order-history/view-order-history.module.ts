import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewOrderHistoryPageRoutingModule } from './view-order-history-routing.module';

import { ViewOrderHistoryPage } from './view-order-history.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewOrderHistoryPageRoutingModule
  ],
  declarations: [ViewOrderHistoryPage]
})
export class ViewOrderHistoryPageModule {}
