import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewOrderHistoryPage } from './view-order-history.page';

const routes: Routes = [
  {
    path: '',
    component: ViewOrderHistoryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewOrderHistoryPageRoutingModule {}
