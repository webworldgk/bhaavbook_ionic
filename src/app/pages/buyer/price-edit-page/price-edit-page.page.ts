import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgForm, FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit, } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../../providers/user-data';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-price-edit-page',
  templateUrl: './price-edit-page.page.html',
  styleUrls: ['./price-edit-page.page.scss'],
})
export class PriceEditPagePage implements OnInit {

  alert:boolean = false;
  productForm: FormGroup;
  isLoadingResults = false;
  defaultName: any;
  Id: string;
  isValidated: boolean;

  data: any;

  priceInfo = {
    product_id:'',
    product_name:'',
    rate:'',
    unit:'',
    ratechange:'',
  }
  
  customActionSubTypeOptions: any = {
    header: 'Choose Sub Type'
  };

  customActionCommonOptions: any = {
    header: 'Select...'
  };
  leadId: string;
  data1: any;
  
  constructor(
    public router: Router,
    public api: ApiService,
    private http: HttpClient,
    public userData: UserData,
    public storage: Storage,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public activatedRoute: ActivatedRoute,
  ) {
    this.Id = this.route.snapshot.paramMap.get('id');
    console.log(`this.Id`, this.Id);
   }

  

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.prepareMasters();
    this.getPriceView();
   this. getProductDetails()
    //this.prepareDate();
  }

  async prepareMasters() {
    if (this.api.categoryList.length == 0) {
      const storeVariables = true;
      this.api.doStoreMasters(storeVariables);
    }
    if (this.api.unitList.length == 0) {
      const storeVariables = true;
      this.api.doStoreMasters(storeVariables);
    }
  }


 
  async getPriceView() {
    const token = await this.userData.getToken();
      console.log('token', token)
      this.api.getDailyReportsView(token,this.Id).subscribe((response: any) => {
        console.log(response);
        if (response && response.data) {
          this.data = response.data;
          console.log(this.data);

        this.priceInfo.product_id = this.data.id;
        this.priceInfo.product_name = this.data.product_name;
        this.priceInfo.rate = this.data.rate;
        this.priceInfo.ratechange = this.data.rate;
        this.priceInfo.unit = this.data.unit_name;
      }
      
    });
  }

  async getProductDetails() {
    this.leadId = this.route.snapshot.paramMap.get('id');

    await this.api.showLoader();

    const token = await this.userData.getToken();
    console.log('token', token)
    this.api.getPriceInfo(token,this.leadId).subscribe((response: any) => {
      console.log('getPriceInfo response', response);
      if (response.status === 1) {
        this.data1 = response.data;
        console.log(this.data1);
        // this.product =   (this.product && Array.isArray(this.product)) ? this.product.concat(response.data): response.data;

        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }
  submitUpdatePrice(form: NgForm) {
    // console.log(`form`, form);
    console.log(this.route.snapshot.paramMap.get('id'));

    this.isValidated = true;
    if (!this.priceInfo.rate) {
      this.api.presentValidateToast('rate is required.', 4000, 'danger');
      this.isValidated = false;
    }
    if (this.isValidated) {
      console.log(`this.priceInfo`, this.priceInfo);

      const postParams = new FormData();
      postParams.append("product_id", this.Id);
      postParams.append("item_rate", this.priceInfo.ratechange);
      postParams.append("product_name", this.priceInfo.product_name);
      postParams.append("unit", this.priceInfo.unit);
      
      this.updateProfile(postParams);
      
    }

  }

  async updateProfile(postParams: any) {

    if (postParams) {

      await this.api.showLoader('Updating...');

      const token = await this.userData.getToken();
      // console.log('token', token);

      const apiName = `api/v1/products/update-product-price`;
      const apiUrl = this.api.restApiUrl + apiName ;
      const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

      this.http.post(apiUrl, postParams, httpOptions).subscribe((response: any) => {
        console.log('updateProduct price response', response);
        if (response.status == 1) {
          this.api.presentSuccessToast(response.message);
          this.api.dismissLoader();

           this.router.navigateByUrl(`/my-product`);
          
        } else if (response.statusCode === 401) {
          this.api.clearStorageAndRedirectToLogin(response.message);
          this.api.dismissLoader();
        } else {
          this.api.presentValidateToast(response.message);
          this.api.dismissLoader();
        }
      }, (error: any) => {
        console.log('error', error.error);
        console.log(error);
        this.api.presentValidateToast(error.error.message);
        this.api.dismissLoader();
      });
    }
  }


  
  onBack() {
    this.router.navigate(['my-product']);
  }

  changeSubType(event: any) {
    const selectedSubType = event.detail.value;
    console.log(`event.detail.value`, event.detail.value);
    this.api.categoryList.find((item) => {
      if (selectedSubType > 1 && item.id == selectedSubType) {
        this.defaultName = item.name;
      }
    });
  }

  backToDashbaord() {
    this.router.navigate(["my-product"]);
  }

}
