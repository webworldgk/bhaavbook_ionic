import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PriceEditPagePageRoutingModule } from './price-edit-page-routing.module';

import { PriceEditPagePage } from './price-edit-page.page';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PriceEditPagePageRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [PriceEditPagePage]
})
export class PriceEditPagePageModule {}
