import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PriceEditPagePage } from './price-edit-page.page';

const routes: Routes = [
  {
    path: '',
    component: PriceEditPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PriceEditPagePageRoutingModule {}
