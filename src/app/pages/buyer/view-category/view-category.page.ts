import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../../providers/user-data';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-view-category',
  templateUrl: './view-category.page.html',
  styleUrls: ['./view-category.page.scss'],
})
export class ViewCategoryPage implements OnInit {

  data: any;
  product: any;
  searchTerm: any;
  page = 1;
  maximumPages = 100;
  apiService: any;
  id: any;


  constructor(
    public router: Router,
    public api: ApiService,
    public userData: UserData,
    public alertCtrl: AlertController,
  ) { }

  loadMore(infiniteScroll){
    this.page++;
    this.getProductDetails(infiniteScroll);

    if(this.page === this.maximumPages){
      infiniteScroll.target.disabled = true;
    }
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.getProductDetails();
  }

  async getProductDetails(infiniteScroll?) {

    await this.api.showLoader();

    const token = await this.userData.getToken();
    console.log('token', token)
    this.api.getCategoryInfo(token, this.page).subscribe((response: any) => {
      console.log('getCategoryInfo response', response);
      if (response.status === 1) {
        this.product = response.data;
       // this.product =   (this.product && Array.isArray(this.product)) ? this.product.concat(response.data): response.data;

        if(infiniteScroll){
          infiniteScroll.target.complete();
        }
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }
  filterItems(searchTerm) {
    return this.product.filter(product => product.product_name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1);
  }


  setFilteredItems() {
    this.product = this.filterItems(this.searchTerm);
  }

  // viewDailyReport(id: any) {
  //   this.router.navigate(['update-product', id]);
  // }

 
  async condirmRemoveDailyReport(id: any) {
    const alert = await this.alertCtrl.create({
      header: 'Confirm Remove',
      message: 'Are you sure you want to remove?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: (no) => {
            console.log('Confirm Cancel: ', no);
          }
        }, {
          text: 'Remove',
          handler: () => {
            console.log('Confirm Okay');
            this.removeDailyReport(id);
          }
        }
      ]
    });

    await alert.present();
  }

  async removeDailyReport(id: any) {

    if (id) {

      await this.api.showLoader('Removing...');

      const token = await this.userData.getToken();
      this.api.deleteCategory(token, id).subscribe((response: any) => {
        console.log('deleteCategory response', response);
        if (response.status == 1) {
          this.api.presentSuccessToast(response.message);
          localStorage.setItem('refreshContents', '1');
          this.getProductDetails();
          this.api.dismissLoader();
        } else if (response.statusCode === 401) {
          this.api.clearStorageAndRedirectToLogin(response.message);
          this.api.dismissLoader();
        } else {
          this.api.presentValidateToast(response.message);
          this.api.dismissLoader();
        }
      });
    }
  }

  
  
  onBack() {
    this.router.navigate(['dashboard']);
  }
  addCategory(){
    this.router.navigate(['/add-category']);
  }

  update(){

  }
}
