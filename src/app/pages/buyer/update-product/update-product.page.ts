import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormControl, FormGroup, NgForm } from '@angular/forms';
import { Component, OnInit, } from '@angular/core';
import { Capacitor } from '@capacitor/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../../providers/user-data';
import { Storage } from '@ionic/storage';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';


@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.page.html',
  styleUrls: ['./update-product.page.scss'],
})
export class UpdateProductPage implements OnInit {

  Product: any[] = [
    { id: 1, name: 'Publish' },
    { id: 0, name: 'Unpublish' },
  ];

  panCard: any;
  todaysDate: any;
  profilePic: any;
  aadharCard: any;
  old_joining_date: any;
  isValidated: boolean;
  data1: any;
  productdata: any;
  updatedata: any;
  defaultName: any;
  defaultSubType: any;
  id: any;
  data: any;
  leadId: string;
  cate: any;
  unit: string;
  category_id: string;
  productpic: any;

  addproductInfo = {
    id: '',
    product_id:'',
    product_name: '',
    // product_type: '',
    rate: '',
    description: '',
    // price: '',
    hsn_code: '',
    hsn_description: '',
    // category: '',
    category_id: '',
    unit_id:'',
    // unit: '',
    product_pic: '',
    aadhar_card_preview:'',
    profile_pic_preview:'',
    category: '',
    unitList: {},
  }
   Id: any;

   updateproductInfo ={
    id: '',
    product_id:'',
    product_name: '',
    rate: '',
    description: '',
    hsn_description: '',
    category_name: '',
    unit_name:'',
    product_pic: '',
    hsn_code:'',
    item_code:'',
    category: '',
    unitList:'',
    unit:'',
    profile_pic_preview:'',
    published:'',
   }
  
  customActionSubTypeOptions: any = {
    header: 'Choose Category'
  };

  customActionCommonOptions: any = {
    header: 'Choose Unit'
  };
  postParams: any;

  ionViewDidEnter() {
    this.prepareMasters();
    this.getProductView();
    
  }

  async prepareMasters() {
    if (this.api.categoryList.length == 0) {
      const storeVariables = true;
      this.api.doStoreMasters(storeVariables);
    }
    if (this.api.unitList.length == 0) {
      const storeVariables = true;
      this.api.doStoreMasters(storeVariables);
    }
  }

  constructor(
    public router: Router,
    public api: ApiService,
    private http: HttpClient,
    public userData: UserData,
    public storage: Storage,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private photoViewer: PhotoViewer,
    public activatedRoute: ActivatedRoute,
  ) {
    this.Id = this.route.snapshot.paramMap.get('id');
    console.log(`this.Id`, this.Id);
  }


  ngOnInit() {
    
  }
  
  async presentToast(message, duration = 2000) {
    const toast = await this.toastCtrl.create({
      message,
      duration,
    });
    toast.present();
  }
  
  
  async getProductView() {
    const token = await this.userData.getToken();
    console.log(token);
    this.api
      .updateItem(token,this.Id)
      .subscribe((response: any) => {
        console.log(response);
        if (response && response.data) {
          this.data = response.data;
          console.log(this.data);

        this.updateproductInfo.product_id = this.data.id;
        this.updateproductInfo.id = this.data.id;
        this.updateproductInfo.product_name = this.data.product_name;
        this.updateproductInfo.rate = this.data.rate;
        this.updateproductInfo.category = this.data.category_id.toString();
        this.updateproductInfo.unit = this.data.unit_id.toString();
        this.updateproductInfo.hsn_description = this.data.hsn_description;
        this.updateproductInfo.product_pic = this.data.product_pic;
        this.updateproductInfo.description = this.data.description;
        this.updateproductInfo.hsn_code = this.data.hsn_code;
        this.updateproductInfo.profile_pic_preview = this.data.product_pic;
        this.updateproductInfo.published = this.data.published.toString();
        this.api.dismissLoader();
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }
  submitUpdateProfile(form: NgForm) {
    // this.leadId = this.route.snapshot.paramMap.get('id');
    // console.log(`form`, form);
    console.log(this.route.snapshot.paramMap.get('id'));
    this.isValidated = true;

    if (!this.updateproductInfo.product_name) {
      this.api.presentValidateToast('product name is required.', 4000, 'danger');
      this.isValidated = false;
    }

    if (!this.updateproductInfo.rate) {
      this.api.presentValidateToast('rate is required.', 4000, 'danger');
      this.isValidated = false;
    }

    if (this.isValidated) {


      console.log(`this.product `, this.updateproductInfo);
    
      const postParams = new FormData();
      postParams.append("product_id", this.Id);
      postParams.append("item_code", this.updateproductInfo.item_code);
      postParams.append("product_name", this.updateproductInfo.product_name);
      postParams.append("item_rate", this.updateproductInfo.rate);
      postParams.append("category_id", this.updateproductInfo.category);//category_name
      postParams.append("unit_id", this.updateproductInfo.unit);
      postParams.append("hsn_description", this.updateproductInfo.hsn_description);
      postParams.append("description", this.updateproductInfo.description);
      postParams.append("hsn_code", this.updateproductInfo.hsn_code);
      postParams.append("published", this.updateproductInfo.published);
     
      if (this.productpic) {
        postParams.append("product_pic", this.productpic, this.productpic?.name);
      }

      this.updateProfile(postParams,form);
    
    }
    
  }

  async updateProfile(postParams: any, form: NgForm) {

    if (postParams) {

      
      await this.api.showLoader('Updating...');

      const token = await this.userData.getToken();
      // console.log('token', token);

      const apiName = `api/v1/products/update-product`;
      const apiUrl = this.api.restApiUrl + apiName ;
      const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

      this.http.post(apiUrl, postParams, httpOptions).subscribe((response: any) => {
        console.log('updateProduct response', response);
        if (response.status == 1) {
          this.api.presentSuccessToast(response.message);
          this.api.dismissLoader();
          this.router.navigateByUrl('/my-product');

        } else if (response.statusCode === 401) {
          this.api.clearStorageAndRedirectToLogin(response.message);
          this.api.dismissLoader();
        } else {
          this.api.presentValidateToast(response.message);
          this.api.dismissLoader();
        }
      }, (error: any) => {
        console.log('error', error.error);
        console.log(error);
        this.api.presentValidateToast(error.error.message);
        this.api.dismissLoader();
      });
    }
  }

  

  onProfileChange(fileEvent: any) {
    if (fileEvent.target.files[0]) {
      this.productpic = fileEvent.target.files[0];
      const reader = new FileReader();
      reader.onload = (event: any) => {
        this.addproductInfo.profile_pic_preview = event.target.result;
      }
      reader.readAsDataURL(fileEvent.target.files[0]);
      console.log(`this.product_pic`, this.productpic);
    }
  }

  previewImage(photoCode: number) {
    console.log(`photoCode`, photoCode);
    if (Capacitor.isNativePlatform()) {
      if (photoCode === 1 && this.addproductInfo.profile_pic_preview) {
        this.photoViewer.show(this.addproductInfo.profile_pic_preview, 'Image', { share: false });
      }

    }
  }
 
   onBack() {
    this.router.navigate(['my-product']);
  }

  
  backToDashbaord() {
    this.router.navigate(["my-product"]);
  }

  changeSubType(event: any) {
    const selectedSubType = event.detail.value;
    console.log(`event.detail.value`, event.detail.value);
    this.api.categoryList.find((item) => {
      if (selectedSubType > 1 && item.id == selectedSubType) {
        this.defaultName = item.name;
      }
    });
  }

}


