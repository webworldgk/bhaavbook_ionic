import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FollowerPagePage } from './follower-page.page';

const routes: Routes = [
  {
    path: '',
    component: FollowerPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FollowerPagePageRoutingModule {}
