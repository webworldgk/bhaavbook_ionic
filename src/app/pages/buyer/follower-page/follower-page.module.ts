import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FollowerPagePageRoutingModule } from './follower-page-routing.module';

import { FollowerPagePage } from './follower-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FollowerPagePageRoutingModule
  ],
  declarations: [FollowerPagePage]
})
export class FollowerPagePageModule {}
