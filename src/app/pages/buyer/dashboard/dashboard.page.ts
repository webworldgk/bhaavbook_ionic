import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../../providers/user-data';
import { Md5 } from 'ts-md5/dist/md5';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  user: any;
  summary: any;
  attendance: any;
  data: any;
  item1: any;
 

  userPermissions = {
    attendancePunch: 0,
    dailyReport: 0,
    rawCustomer: 0,
    technicalLead: 0,
    attendanceList: 0,
    addDailyReport: 0,
    addRawCustomer: 0,
    addproduct : 0,
  };
  order: any;

  constructor(
    public router: Router,
    public userData: UserData,
    public route: ActivatedRoute,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public api: ApiService
  ) { }

  ngOnInit() {
    this.getDashboardDetails();
    }


    ionViewDidEnter() {
     // this.getDashboardDetails();
      if (localStorage.getItem('refreshContents') == '1') {
        this.getDashboardDetails();
      }
    }

  async getDashboardDetails() {

    await this.api.showLoader();

    const token = await this.userData.getToken();
    // console.log('token', token);

    this.api.getDashboardInfo(token).subscribe((response: any) => {
      console.log('getDashboardInfo response', response);
      if (response.status === 1) {
        this.data = response.data;
        this.order = response.data.latestOrders;
        console.log(this.data);
        console.log(this.order);
        this.api.dismissLoader();

        // reset in order to not refresh again
         localStorage.setItem('refreshContents', '0');

      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();

      } else if (response.message === 'Server Error') {
        this.api.serverErrorToast();
        this.api.dismissLoader();

      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });

  }

  
  
  async confirmLogout() {
    const alert = await this.alertCtrl.create({
      header: 'Confirm sign out',
      message: 'Are you sure you want to sign out?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: (no) => {
            console.log('Confirm Cancel: ', no);
          }
        }, {
          text: 'Sign Out',
          handler: () => {
            console.log('Confirm Okay');
            this.api.logout();
          }
        }
      ]
    });

    await alert.present();
  }

 
  refreshDashboard() {
    this.getDashboardDetails();
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }
  myproduct(){
    this.router.navigate(['my-product']);
  }

}
