import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../../providers/user-data';
import { AlertController, IonContent } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Storage } from "@ionic/storage";



@Component({
  selector: 'app-view-user-detail',
  templateUrl: './view-user-detail.page.html',
  styleUrls: ['./view-user-detail.page.scss'],
})
export class ViewUserDetailPage implements OnInit {

  id: any;
  data: any;
  page: number;

  searchParams = {
    product_name: "",
    category_id: "",
    hsn_code: "",
    hsn_description: "",
    description: "",
    published:"",
    category:"",
    search:"",
  };
  statusData: any;
  isValidated: boolean;
 
  constructor(
    public router: Router,
    public api: ApiService,
    public userData: UserData,
    public alertCtrl: AlertController,
    private http: HttpClient,
    public route: ActivatedRoute,
    public storage: Storage
  ) { 
    this.id = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
      
  }

  ionViewDidEnter() {
    this.getOrderDetails();
  }
  async getOrderDetails() {

    await this.api.showLoader();

    const token = await this.userData.getToken();
    console.log('token', token)
    this.api.getOrderInfo(token, this.id).subscribe((response: any) => {
      console.log('getOrderDetails response', response);
      if (response.status === 1) {
        this.data = response.data;
       // this.product =   (this.product && Array.isArray(this.product)) ? this.product.concat(response.data): response.data;
       console.log(this.data);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }

   
  async submitStatus(id: any) {
    //console.log(`form`, form);
    console.log(id);
  
   this.isValidated = true;
     
     const userInfo = await this.storage.get("userInfo");
     console.log("savecat_userInfo", userInfo);
     console.log(id);
   
   const token = await this.userData.getToken();
   console.log('token', token);
  
   if (this.isValidated) {
    const postParams = {
      following_id: id,
    };
  
     console.log(postParams);
      this.addStatus(postParams, id);
    }
    
  
  }
  
  async addStatus(postParams: any, id: any) {
  
   if (postParams) {
  
     console.log(`postParams`, postParams);
     await this.api.showLoader('Saving...');
  
     const token = await this.userData.getToken();
     // console.log('token', token);
  
     const apiName = `api/v1/following`;
     const apiUrl = this.api.restApiUrl + apiName;
     const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
  
     this.http.post(apiUrl, postParams, httpOptions).subscribe((response: any) => {
       console.log('status response', response);
       if (response.status == 1) {
         this.statusData = response.data;
         console.log("status data", this.statusData);
         this.api.presentSuccessToast(response.message);
         this.api.dismissLoader();
        //  this.router.navigateByUrl(`/my-product`);
        //  form.reset();
       } else if (response.statusCode === 401) {
         this.api.clearStorageAndRedirectToLogin(response.message);
         this.api.dismissLoader();
       } else {
         this.api.presentValidateToast(response.message);
         this.api.dismissLoader();
       }
     }, (error: any) => {
       console.log('error', error.error);
       this.api.presentValidateToast(error.error.message);
       this.api.dismissLoader();
     });
   }
  }

}
