import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewUserDetailPage } from './view-user-detail.page';

const routes: Routes = [
  {
    path: '',
    component: ViewUserDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewUserDetailPageRoutingModule {}
