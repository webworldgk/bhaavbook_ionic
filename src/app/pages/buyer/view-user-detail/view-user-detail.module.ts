import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewUserDetailPageRoutingModule } from './view-user-detail-routing.module';

import { ViewUserDetailPage } from './view-user-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewUserDetailPageRoutingModule
  ],
  declarations: [ViewUserDetailPage]
})
export class ViewUserDetailPageModule {}
