import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../../providers/user-data';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertController, IonContent } from '@ionic/angular';
import { Storage } from "@ionic/storage";

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.page.html',
  styleUrls: ['./order-list.page.scss'],
})
export class OrderListPage implements OnInit {
  @ViewChild(IonContent) content: IonContent;
  data: any;
  product: any;
  searchTerm: any;
  page = 1;
  maximumPages = 100;
  apiService: any;
  id: any;
  leadId: string;
  isEnableSearch: boolean;

  isSearchResults: boolean;
  isSearchValidated: boolean;
  searchParams = {
    product_name: "",
    category_id: "",
    hsn_code: "",
    hsn_description: "",
    product_description: "",
    published:"",
    category:"",
    search:"",
  };
  productCount: number;
  defaultName: any;
  data1: any;

  constructor(
    public router: Router,
    public api: ApiService,
    public userData: UserData,
    public route: ActivatedRoute,
    public alertCtrl: AlertController,
    private http: HttpClient,
    public storage: Storage
  ) {
    this.leadId = this.route.snapshot.paramMap.get('id');
   }

   customActionSubTypeOptions: any = {
    header: "Choose Category",
  };

  loadMore(infiniteScroll){
    this.page++;
    this.getOrderDetails(infiniteScroll);

    if(this.page === this.maximumPages){
      infiniteScroll.target.disabled = true;
    }
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.getOrderDetails();
  }

  async getOrderDetails(infiniteScroll?) {

    await this.api.showLoader();

    const token = await this.userData.getToken();
    console.log('token', token)
    this.api.getOrderInfo1(token ,this.page,this.searchParams.product_name,this.searchParams.category,this.searchParams.product_description).subscribe((response: any) => {
      console.log('getOrderDetails response', response);
      if (response.status === 1) {
        this.product = response.data;
        this.productCount = response.data.length;
      // this.product =   (this.product && Array.isArray(this.product)) ? this.product.concat(response.data): response.data;

        if(infiniteScroll){
          infiniteScroll.target.complete();
        }
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }

  async getCategoryDetails() {
    const userInfo = await this.storage.get("userInfo");
    console.log("savecat_userInfo", userInfo);
    console.log(userInfo.id);
    // await this.api.showLoader();

    const token = await this.userData.getToken();
    console.log("token", token);
    this.api.getCategoryInfo(token, this.page).subscribe((response: any) => {
      console.log("getCategoryInfo response", response);
      if (response.status === 1) {
        this.data1 = response.data;
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }
  filterItems(searchTerm) {
    return this.product.filter(product => product.product_name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1);
  }


  setFilteredItems() {
    this.product = this.filterItems(this.searchTerm);
  }

  async confirmLogout() {
    const alert = await this.alertCtrl.create({
      header: 'Confirm sign out',
      message: 'Are you sure you want to sign out?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: (no) => {
            console.log('Confirm Cancel: ', no);
          }
        }, {
          text: 'Sign Out',
          handler: () => {
            console.log('Confirm Okay');
            this.api.logout();
          }
        }
      ]
    });

    await alert.present();
  }

  scrollToTop() {
    this.content.scrollToTop(400);
  }

  openSearchBox() {
    this.scrollToTop();
    this.isEnableSearch = true;
  }

  hideSearchBox() {
    this.isEnableSearch = false;
    this.isSearchResults = false;
  }

  resetSearchBox() {
    this.searchParams = {
      product_name: "",
      category_id: "",
      hsn_code: "",
      hsn_description: "",
      product_description: "",
      published:"",
      category:"",
      search:"",
    };
  }

  applySearch() {
    this.getSearchCustomers();
  }

  async getSearchCustomers() {

    this.isSearchValidated = true;

    if (
      this.searchParams.product_name == '' &&
      this.searchParams.category_id == '' &&
      this.searchParams.hsn_code == '' &&
      this.searchParams.hsn_description == '' &&
      this.searchParams.product_description == '' &&
      this.searchParams.published == ''
    )  {
      this.isSearchValidated = false;
    }

    if (this.isSearchValidated === true) {

      this.isSearchResults = true;

      // await this.api.showLoader('Searching...');

      const token = await this.userData.getToken();
      // console.log('token', token);

      const apiName = `api/v1/orders?page=${this.page}&product_name=${this.searchParams.product_name}&category=${this.searchParams.category}&product_description=${this.searchParams.product_description}`;
      const apiUrl = this.api.restApiUrl + apiName;
      const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

      // console.log(apiName);
      console.log(`this.searchParams`, this.searchParams);

      this.http.get(apiUrl, httpOptions).subscribe((response: any) => {
        console.log('getSearchCustomers response', response);
        if (response.status === 1) {

          this.product = response.data;
          this.productCount = response.data.length;
          this.isEnableSearch = false;
          this.api.dismissLoader();

        } else if (response.statusCode === 401) {
          this.api.clearStorageAndRedirectToLogin(response.message);
          this.api.dismissLoader();

        } else {
          this.api.presentValidateToast(response.message);
          this.api.dismissLoader();
        }
      }, (error: any) => {

        console.log('error', error.error);
        this.api.presentValidateToast(error.error.message);
        this.api.dismissLoader();
      });
    }

  }
  resetSearch() {
    this.resetSearchBox();
    this.hideSearchBox();
    this.getOrderDetails();
  }

  Search(event: any) {
    this.getOrderDetails();
  }
  changeSubType(event: any) {
    const selectedSubType = event.detail.value;
    console.log(`event.detail.value`, event.detail.value);
    this.api.categoryList.find((item) => {
      if (selectedSubType > 1 && item.id == selectedSubType) {
        this.defaultName = item.id;
      }
    });
  }
}
