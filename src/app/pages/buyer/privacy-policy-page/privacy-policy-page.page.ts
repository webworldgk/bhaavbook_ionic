import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from "src/app/service/api.service";
import { UserData } from "../../../providers/user-data";
import {
  AlertController,
  IonContent,
  LoadingController,
  ToastController,
} from "@ionic/angular";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Storage } from "@ionic/storage";

@Component({
  selector: 'app-privacy-policy-page',
  templateUrl: './privacy-policy-page.page.html',
  styleUrls: ['./privacy-policy-page.page.scss'],
})
export class PrivacyPolicyPagePage implements OnInit {

  @ViewChild(IonContent) content: IonContent;

  data: any;
  id: any;
 
  constructor(
    public router: Router,
    public api: ApiService,
    public userData: UserData,
    public alertCtrl: AlertController,
    private http: HttpClient,
    public storage: Storage
  ) {}

  

  ngOnInit() {

  }

  ionViewDidEnter() {
    this.getPrivacyDetails();
  }


  async getPrivacyDetails() {

    await this.api.showLoader();

    const token = await this.userData.getToken();
    console.log("token", token);
    this.api.getPrivacyInfo(token, this.id).subscribe((response: any) => {
      console.log("getPrivacyInfo response", response);
      if (response && response.data) {
        this.data = response.data;
        console.log(this.data);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }
  


}
