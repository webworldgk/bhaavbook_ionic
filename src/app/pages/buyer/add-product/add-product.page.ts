import { HttpClient, HttpHeaders } from "@angular/common/http";
import { NgForm, Validators } from "@angular/forms";
import { Component, OnInit } from "@angular/core";
import { Capacitor } from "@capacitor/core";
import { ActivatedRoute, Router } from "@angular/router";
import { LoadingController, ToastController } from "@ionic/angular";
import { ApiService } from "src/app/service/api.service";
import { UserData } from "../../../providers/user-data";
import { Storage } from "@ionic/storage";
import { PhotoViewer } from "@ionic-native/photo-viewer/ngx";

@Component({
  selector: "app-add-product",
  templateUrl: "./add-product.page.html",
  styleUrls: ["./add-product.page.scss"],
})
export class AddProductPage implements OnInit {
  user: any;
  summary: any;
  attendance: any;
  isPunchIn: number;
  // aadharCard: any;
  // event : any;
 
  Product: any[] = [
    { id: 1, name: 'Publish' },
    { id: 0, name: 'Unpublish' },
  ];

  isValidated: boolean;
  acceptableExtensions = "image/*";
  galleryFile: any;
  actualFile: any;
  categoryList: any;
  code: any;
    product_name: any;
    product_type: any;
    item_rate: any;
    description: any;
    price: any;
    hsn_code: any;
    hsn_description: any;
    category: any;
    category_id: any;
    unit: any;
    id: any;
    product_pic: any;
    profilePic: any;
    aadhar_card_preview: any;
    profile_pic_preview: any;
    unitList: any;
    name: any;
    published: any;
    type: any;

  addproductInfo = {
    code: "",
    product_name: "",
    product_type: "",
    item_rate: "",
    description: "",
    price: "",
    hsn_code: "",
    hsn_description: "",
    category: "",
    category_id: "",
    unit: "",
    id: "",
    galleryFile:"",
    actualFile: "",
    product_pic: "",
    profilePic: "",
    aadhar_card_preview: "",
    profile_pic_preview: "",
    
    unitList: {},
    categoryList:{},
  };

  customActionSubTypeOptions: any = {
    header: "Choose Category",
  };

  customActionCommonOptions: any = {
    header: "Choose Unit",
  };

  defaultName: any;
  defaultSubType: any;
  photoViewer: any;
  myForm: any;
  fb: any;
  isValidated1: boolean;
  product: any;
  page: number;
  leadId: any;

  //category_id: string;

  constructor(
    public router: Router,
    public api: ApiService,
    private http: HttpClient,
    public userData: UserData,
    public storage: Storage,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController
  ) {
    // this.leadId = this.route.snapshot.paramMap.get('id');
    // console.log(this.leadId);
    
  }

  ngOnInit() {}


  ionViewDidEnter() {
    this.prepareMasters();
    this.getProductDetails();
  }

  async prepareMasters() {
    if (this.api.categoryList.length == 0) {
      const storeVariables = true;
      this.api.doStoreMasters(storeVariables);
    }
    if (this.api.unitList.length == 0) {
      const storeVariables = true;
      this.api.doStoreMasters(storeVariables);
    }
  }

  async getProductDetails() {

    const userInfo = await this.storage.get("userInfo");
    console.log("savecat_userInfo", userInfo);
    console.log(userInfo.id);
    await this.api.showLoader();

    const token = await this.userData.getToken();
    console.log('token', token)
    this.api.getCategoryInfo(token, this.page).subscribe((response: any) => {
      console.log('getCategoryInfo response', response);
      if (response.status === 1) {
        this.product = response.data;
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }
  submitProduct(form: NgForm) {
     //console.log(`form`, form);

    this.isValidated = true;
    if (!this.product_name) {
      this.api.presentValidateToast('product name is required.', 4000, 'danger');
      this.isValidated = false;
    }
     
    if (!this.item_rate) {
          this.api.presentValidateToast("Rate is required.", 4000, "danger");
          this.isValidated = false;
        }
    
        if (!this.hsn_description) {
          this.api.presentValidateToast("Descriptionis required.", 4000, "danger");
          this.isValidated = false;
        }
    
        if (!this.categoryList) {
          this.api.presentValidateToast("Category is required.", 4000, "danger");
          this.isValidated = false;
        }
    
        if (!this.unitList) {
          this.api.presentValidateToast("Unit is required.", 4000, "danger");
          this.isValidated = false;
        }
        if (!this.actualFile) {
          this.api.presentValidateToast('image is required.', 4000, 'danger');
          this.isValidated = false;
        }

        if (this.published == '') {
          this.api.presentValidateToast('Publish Type is required.', 4000, 'danger');
          this.isValidated = false;
        }
    if (this.isValidated) {
      const postParams = new FormData();
      postParams.append('item_code', this.code);
      postParams.append('product_name', this.product_name);
      postParams.append('item_rate', this.item_rate);
      postParams.append('category_id', this.categoryList);
      postParams.append('description', this.hsn_description);
      postParams.append('hsn_code', this.hsn_code);
      postParams.append('unit_id', this.unitList);
      postParams.append('hsn_description', this.hsn_description);
      postParams.append('product_pic', this.actualFile, this.actualFile?.name);
      // postParams.append('published', this.published,this.published : null);
      postParams.append('published',  this.published);
      
      console.log(`postParams`, postParams);
      
      this.addProduct(postParams, form);
    }

  }

  async addProduct(postParams: any, form: NgForm) {

    if (postParams) {

      console.log(`postParams`, postParams);
      await this.api.showLoader('Saving...');

      const token = await this.userData.getToken();
      // console.log('token', token);

      const apiName = `api/v1/products`;
      const apiUrl = this.api.restApiUrl + apiName;
      const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

      this.http.post(apiUrl, postParams, httpOptions).subscribe((response: any) => {
        console.log('addProduct response', response);
        if (response.status == 1) {
          this.api.presentSuccessToast(response.message);
          this.api.dismissLoader();
          this.router.navigateByUrl(`/my-product`);
          form.reset();
        } else if (response.statusCode === 401) {
          this.api.clearStorageAndRedirectToLogin(response.message);
          this.api.dismissLoader();
        } else {
          this.api.presentValidateToast(response.message);
          this.api.dismissLoader();
        }
      }, (error: any) => {
        console.log('error', error.error);
        this.api.presentValidateToast(error.error.message);
        this.api.dismissLoader();
      });
    }
  }

 
  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

  backToDashbaord() {
    this.router.navigate(["dashboard"]);
  }


  onFileChange(fileChangeEvent: any) {
    if (fileChangeEvent.target.files[0]) {
      this.actualFile = fileChangeEvent.target.files[0];
      const reader = new FileReader();
      reader.onload = (event: any) => {
        this.profile_pic_preview = event.target.result;
      };
      reader.readAsDataURL(fileChangeEvent.target.files[0]);
      console.log(`this.actualFile`, this.actualFile);
    }
  }


  previewImage(photoCode: number) {
    console.log(`photoCode`, photoCode);
    if (Capacitor.isNativePlatform()) {
      if (photoCode === 1 && this.profile_pic_preview) {
        this.photoViewer.show(
          this.profile_pic_preview,
          "Profile Picture",
          { share: false }
        );
      }
    }
  }
  changeSubType(event: any) {
    const selectedSubType = event.detail.value;
    console.log(`event.detail.value`, event.detail.value);
    this.api.categoryList.find((item) => {
      if (selectedSubType > 1 && item.id == selectedSubType) {
        this.defaultName = item.name;
      }
    });
  }
}
 

