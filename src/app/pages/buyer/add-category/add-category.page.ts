import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import {
  AlertController,
  LoadingController,
  ToastController,
} from "@ionic/angular";
import { ApiService } from "src/app/service/api.service";
import { UserData } from "../../../providers/user-data";
import { Storage } from "@ionic/storage";

@Component({
  selector: "app-add-category",
  templateUrl: "./add-category.page.html",
  styleUrls: ["./add-category.page.scss"],
})
export class AddCategoryPage implements OnInit {
  isValidated = false;

  addcategory = {
    category: "",
    categoryList: {},
    code: "",
  };

  customActionSubTypeOptions: any = {
    header: "Choose Category",
  };

  customActionCommonOptions: any = {
    header: "Choose Unit",
  };

  defaultName: any;
  defaultSubType: any;
  id: any;
  employeeCode: any;
  employeeName: any;
  pageTitle: string;
  userid: string;
  //category_id: string;

  constructor(
    public router: Router,
    public api: ApiService,
    private http: HttpClient,
    public userData: UserData,
    public storage: Storage,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController
  ) {
    this.id = this.route.snapshot.paramMap.get("id");
    console.log(`this.user_id`, this.id);
  }

  ngOnInit() {}

  ionViewDidEnter() {
    this.prepareMasters();
  }

  async prepareMasters() {
    if (this.api.categoryList.length == 0) {
      const storeVariables = true;
      this.api.doStoreMasters(storeVariables);
    }
  }

  async submitFollowUp(form: NgForm) {
 
   const userInfo = await this.storage.get("userInfo");
    console.log("savecat_userInfo", userInfo);
    this.isValidated = true;

    if (this.addcategory.categoryList == "") {
      this.api.presentValidateToast("Category is required.", 4000, "danger");
      this.isValidated = false;
    }

    if (this.isValidated) {
      const postParams = {
        user_id: userInfo.id,
        category_id: this.addcategory.categoryList
          ? this.addcategory.categoryList
          : null,
      };

      console.log(`postParams`, postParams);
      this.addFollowUp(postParams, form);
    }
  }

  async addFollowUp(postParams: any, form: NgForm) {
    if (postParams) {
      await this.api.showLoader("Saving...");

      const token = await this.userData.getToken();
      // console.log('token', token);

      const apiName = `api/v1/usercategory`;
      const apiUrl = this.api.restApiUrl + apiName;
      const httpOptions = {
        headers: new HttpHeaders({ Authorization: token }),
      };

      this.http.post(apiUrl, postParams, httpOptions).subscribe(
        (response: any) => {
          console.log("addFollowUp response", response);
          if (response.status == 1) {
            this.api.presentSuccessToast(response.message);
            this.api.dismissLoader();
            this.router.navigateByUrl(`/view-category`);
            form.reset();
          } else if (response.statusCode === 401) {
            this.api.clearStorageAndRedirectToLogin(response.message);
            this.api.dismissLoader();
          } else {
            this.api.presentValidateToast(response.message);
            this.api.dismissLoader();
          }
        },
        (error: any) => {
          console.log("error", error.error);
          this.api.presentValidateToast(error.error.message);
          this.api.dismissLoader();
        }
      );
    }
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

  backToRawCustomerDashbaord() {
    this.router.navigate(["dashboard"]);
  }

  onBack() {
    this.router.navigate(["daily-report"]);
  }

  changeSubType(event: any) {
    const selectedSubType = event.detail.value;
    console.log(`event.detail.value`, event.detail.value);
    this.api.categoryList.find((item) => {
      if (selectedSubType > 1 && item.id == selectedSubType) {
        this.defaultName = item.name;
      }
    });
  }
}
