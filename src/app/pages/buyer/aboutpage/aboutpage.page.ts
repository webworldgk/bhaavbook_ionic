import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from "src/app/service/api.service";
import { UserData } from "../../../providers/user-data";
import {
  AlertController,
  IonContent,
  LoadingController,
  ToastController,
} from "@ionic/angular";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Storage } from "@ionic/storage";

@Component({
  selector: 'app-aboutpage',
  templateUrl: './aboutpage.page.html',
  styleUrls: ['./aboutpage.page.scss'],
})
export class AboutpagePage implements OnInit {

  @ViewChild(IonContent) content: IonContent;
  data: any;
  id: any;

  constructor(
    public router: Router,
    public api: ApiService,
    public userData: UserData,
    public alertCtrl: AlertController,
    private http: HttpClient,
    public storage: Storage
  ) {}

  

  ngOnInit() {

  }

  ionViewDidEnter() {
    this.getAboutDetails();
  }
  

  async getAboutDetails() {

    await this.api.showLoader();

    const token = await this.userData.getToken();
    console.log("token", token);
    this.api.getPageInfo(token, this.id).subscribe((response: any) => {
      console.log("getAboutInfo response", response);
      if (response && response.data)  {
        this.data = response.data;
        console.log(this.data);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }
  



}
