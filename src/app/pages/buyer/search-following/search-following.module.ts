import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SearchFollowingPageRoutingModule } from './search-following-routing.module';

import { SearchFollowingPage } from './search-following.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SearchFollowingPageRoutingModule
  ],
  declarations: [SearchFollowingPage]
})
export class SearchFollowingPageModule {}
