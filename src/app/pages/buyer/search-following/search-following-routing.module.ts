import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchFollowingPage } from './search-following.page';

const routes: Routes = [
  {
    path: '',
    component: SearchFollowingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SearchFollowingPageRoutingModule {}
