import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConfirmFollowerPageRoutingModule } from './confirm-follower-routing.module';

import { ConfirmFollowerPage } from './confirm-follower.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConfirmFollowerPageRoutingModule
  ],
  declarations: [ConfirmFollowerPage]
})
export class ConfirmFollowerPageModule {}
