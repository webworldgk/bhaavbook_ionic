import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../../providers/user-data';
import { AlertController } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Storage } from "@ionic/storage";

@Component({
  selector: 'app-confirm-follower',
  templateUrl: './confirm-follower.page.html',
  styleUrls: ['./confirm-follower.page.scss'],
})
export class ConfirmFollowerPage implements OnInit {

  Product: any[] = [
    { id: 1, name: 'Accepted' },
    { id: 2, name: 'Rejected' },
  ];
  data: any;
  follower_id: any;
  status=0;
  isValidated: boolean;
  id: any;
  statusData: any;
  followeCount: any;

  constructor(
    public router: Router,
    public api: ApiService,
    public storage: Storage,
    public userData: UserData,
    public alertCtrl: AlertController,
    public route: ActivatedRoute,
    public activatedRoute: ActivatedRoute,
    private http: HttpClient,
    
  ) { 
    this.follower_id = this.route.snapshot.paramMap.get('id');
    console.log(`this.follower_id`, this.follower_id);
  }


  ngOnInit() {
   
  }

  ionViewDidEnter() {
    this.getFollowerDetails();
  }

  async submitStatus(id: any, status: any) {
  //console.log(`form`, form);
  console.log(id);
  console.log(status);

 this.isValidated = true;
   
   const userInfo = await this.storage.get("userInfo");
   console.log("savecat_userInfo", userInfo);
   console.log(id);
 
 const token = await this.userData.getToken();
 console.log('token', token);

 if (this.isValidated) {
  const postParams = {
    id: id,
    status: status,
  };

   console.log(postParams);
     this.addStatus(postParams);
  }
  

}

async addStatus(postParams: any) {

 if (postParams) {

   console.log(`postParams`, postParams);
   await this.api.showLoader('Saving...');

   const token = await this.userData.getToken();
   // console.log('token', token);

   const apiName = `api/v1/followers/status-update`;
   const apiUrl = this.api.restApiUrl + apiName;
   const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

   this.http.post(apiUrl, postParams, httpOptions).subscribe((response: any) => {
     console.log('status response', response);
     if (response.status == 1) {
       this.statusData = response.data;
       console.log("status data", this.statusData);
       this.api.presentSuccessToast(response.message);
       this.api.dismissLoader();
      //  this.router.navigateByUrl(`/my-product`);
      //  form.reset();
     } else if (response.statusCode === 401) {
       this.api.clearStorageAndRedirectToLogin(response.message);
       this.api.dismissLoader();
     } else {
       this.api.presentValidateToast(response.message);
       this.api.dismissLoader();
     }
   }, (error: any) => {
     console.log('error', error.error);
     this.api.presentValidateToast(error.error.message);
     this.api.dismissLoader();
   });
 }
}


async getFollowerDetails() {

    await this.api.showLoader();

    const token = await this.userData.getToken();
    console.log('token', token)
    this.api.getFollowerInfo(token,this.status).subscribe((response: any) => {
      console.log('getFollowerInfo response', response);
      if (response.status === 1) {
        this.data = response.data;
        this.followeCount=response.data.length;
        console.log(this.data);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
      });

  }


  
}
