import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfirmFollowerPage } from './confirm-follower.page';

const routes: Routes = [
  {
    path: '',
    component: ConfirmFollowerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfirmFollowerPageRoutingModule {}
