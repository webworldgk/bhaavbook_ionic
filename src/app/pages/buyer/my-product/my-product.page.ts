import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from "src/app/service/api.service";
import { UserData } from "../../../providers/user-data";
import {
  AlertController,
  IonContent,
  LoadingController,
  ToastController,
} from "@ionic/angular";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Storage } from "@ionic/storage";

@Component({
  selector: "app-my-product",
  templateUrl: "./my-product.page.html",
  styleUrls: ["./my-product.page.scss"],
})
export class MyProductPage implements OnInit {
  @ViewChild(IonContent) content: IonContent;

  
  isBack: boolean;
  customerList: any;
  customerCount: number;
  isEnableSearch: boolean;
  shouldShowCancel: boolean;
  productCount: number;
  product_name:any;
  category_id:any;
  hsn_code:any;
  hsn_description:any;
  category:any;
  description:any;
  published:any


  meta: any;
  data: any;
  data1: any;
  product: any;
  searchTerm: any;
  page = 1;
  maximumPages = 100;
  apiService: any;
  id: any;

  Product: any[] = [
    { id: 1, name: 'Publish' },
    { id: 0, name: 'Unpublish' },
  ];
  
  customActionSubTypeOptions: any = {
    header: "Choose Category",
  };

  customActionCommonOptions: any = {
    header: "Choose Unit",
  };

  isSearchResults: boolean;
  isSearchValidated: boolean;
  searchParams = {
    product_name: "",
    category_id: "",
    hsn_code: "",
    hsn_description: "",
    description: "",
    published:"",
    category:"",
    search:"",
  };

  defaultName: any;
  productProvider: any;
  toggled: boolean;
  constructor(
    public router: Router,
    public api: ApiService,
    public userData: UserData,
    public alertCtrl: AlertController,
    private http: HttpClient,
    public storage: Storage
  ) {}

  loadMore(infiniteScroll) {
    this.page++;
    this.getProductDetails(infiniteScroll);

    if (this.page === this.maximumPages) {
      infiniteScroll.target.disabled = true;
    }
  }

  ngOnInit() {

  }

  ionViewDidEnter() {
    this.getCategoryDetails();
    this.getProductDetails();
  }
  

  async getCategoryDetails() {
    const userInfo = await this.storage.get("userInfo");
    console.log("savecat_userInfo", userInfo);
    console.log(userInfo.id);
    // await this.api.showLoader();

    const token = await this.userData.getToken();
    console.log("token", token);
    this.api.getCategoryInfo(token, this.page).subscribe((response: any) => {
      console.log("getCategoryInfo response", response);
      if (response.status === 1) {
        this.data1 = response.data;
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }
  async getProductDetails(infiniteScroll?) {
    // this.isSearchResults = false;

    await this.api.showLoader();

    const token = await this.userData.getToken();
    console.log("token", token);
    this.api.getProductInfo(token, this.page, this.searchParams.product_name,this.searchParams.category_id,this.searchParams.hsn_code,this.searchParams.hsn_description,this.searchParams.category,this.searchParams.description,this.searchParams.published,this.searchParams.search).subscribe((response: any) => {
      console.log("getProductInfo response", response);
      if (response.status === 1) {
        console.log(response.data);
       // this.product = response.data;
        this.productCount = response.data.length;
        this.product =
          this.product && Array.isArray(this.product)
            ? this.product.concat(response.data)
            : response.data;
            this.meta = response.meta;
        // console.log(this.product);
            
        if (infiniteScroll) {
          infiniteScroll.target.complete();
        }
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }

  filterItems(searchTerm) {
    return this.product.filter(
      (product) =>
       (product.product_name.toLowerCase().indexOf(searchTerm.toLowerCase()) >
        -1),
        
    );
  }
  
  setFilteredItems() {
    this.product = this.filterItems(this.searchTerm);
  }

  resetSearch() {
    this.resetSearchBox();
    this.hideSearchBox();
    this.getProductDetails();
  }

  async confirmLogout() {
    const alert = await this.alertCtrl.create({
      header: "Confirm sign out",
      message: "Are you sure you want to sign out?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: (no) => {
            console.log("Confirm Cancel: ", no);
          },
        },
        {
          text: "Sign Out",
          handler: () => {
            console.log("Confirm Okay");
            this.api.logout();
          },
        },
      ],
    });

    await alert.present();
  }

  changeSubType(event: any) {
    const selectedSubType = event.detail.value;
    console.log(`event.detail.value`, event.detail.value);
    this.api.categoryList.find((item) => {
      if (selectedSubType > 1 && item.id == selectedSubType) {
        this.defaultName = item.id;
      }
    });
  }

  async condirmRemoveDailyReport(id: any) {
    const alert = await this.alertCtrl.create({
      header: "Confirm Remove",
      message: "Are you sure you want to remove?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: (no) => {
            console.log("Confirm Cancel: ", no);
          },
        },
        {
          text: "Remove",
          handler: () => {
            console.log("Confirm Okay");
            this.removeDailyReport(id);
          },
        },
      ],
    });

    await alert.present();
  }

  async removeDailyReport(id: any) {
    if (id) {
      await this.api.showLoader("Removing...");

      const token = await this.userData.getToken();
      this.api.deleteDailyReport(token, id).subscribe((response: any) => {
        console.log("deleteDailyReport response", response);
        if (response.status == 1) {
          this.api.presentSuccessToast(response.message);
          localStorage.setItem("refreshContents", "1");
          this.getProductDetails();
          this.api.dismissLoader();
        } else if (response.statusCode === 401) {
          this.api.clearStorageAndRedirectToLogin(response.message);
          this.api.dismissLoader();
        } else {
          this.api.presentValidateToast(response.message);
          this.api.dismissLoader();
        }
      });
    }
  }

  scrollToTop() {
    this.content.scrollToTop(400);
  }

  openSearchBox() {
    this.scrollToTop();
    this.isEnableSearch = true;
  }

  open(){
    this.scrollToTop();
  }

  hideSearchBox() {
    this.isEnableSearch = false;
    this.isSearchResults = false;
  }

  resetSearchBox() {
    this.searchParams = {
      product_name: "",
      category_id: "",
      hsn_code: "",
      hsn_description: "",
      description: "",
      published:"",
      category:"",
      search:"",
    };
  }

  Search(event: any) {
    this.getSearchProduct();
  }
  applySearch() {
    this.getSearchProduct();
  }
  

  async getSearchProduct() {
    this.isSearchValidated = true;

    if (
      this.searchParams.product_name == "" &&
      this.searchParams.category_id == "" &&
      this.searchParams.hsn_code == "" &&
      this.searchParams.hsn_description == "" &&
      this.searchParams.description == "" &&
      this.searchParams.published == "" &&
      this.searchParams.search == ""
    ) {
      this.isSearchValidated = false;
    }

    if (this.isSearchValidated === true) {
      this.isSearchResults = true;

      // await this.api.showLoader("Searching...");

      const token = await this.userData.getToken();
      // console.log('token', token);

      const apiName = `api/v1/products?page=${this.page}&product_name=${this.searchParams.product_name}&category_id=${this.searchParams.category_id}&hsn_code=${this.searchParams.hsn_code}&hsn_description=${this.searchParams.hsn_description}&category=${this.searchParams.category}&description=${this.searchParams.description}&published=${this.searchParams.published}&search=${this.searchParams.search}`;
      const apiUrl = this.api.restApiUrl + apiName;
      const httpOptions = {
        headers: new HttpHeaders({ Authorization: token }),
       
      };

      this.http.get(apiUrl, httpOptions).subscribe(
        (response: any) => {
          console.log("getSearchCustomers response", response);
          if (response.status === 1) {
            this.product = response.data;
            this.productCount = response.data.length;
            this.isEnableSearch = false;
            this.api.dismissLoader();
          } else if (response.statusCode === 401) {
            this.api.clearStorageAndRedirectToLogin(response.message);
            this.api.dismissLoader();
          } else {
            this.api.presentValidateToast(response.message);
            this.api.dismissLoader();
          }
        },
        (error: any) => {
          console.log("error", error.error);
          this.api.presentValidateToast(error.error.message);
          this.api.dismissLoader();
        }
      );
    }
  }

 
  doRefresh(event) {
    console.log('Begin async operation');
    this.getProductDetails();
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }
  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }
}
