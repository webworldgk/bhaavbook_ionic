import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from "src/app/service/api.service";
import { UserData } from "../../../providers/user-data";
import {
  AlertController,
  IonContent,
  LoadingController,
  ToastController,
} from "@ionic/angular";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Storage } from "@ionic/storage";

@Component({
  selector: 'app-search-follower',
  templateUrl: './search-follower.page.html',
  styleUrls: ['./search-follower.page.scss'],
})
export class SearchFollowerPage implements OnInit {
  @ViewChild(IonContent) content: IonContent;

  
  isBack: boolean;
  customerList: any;
  customerCount: number;
  isEnableSearch: boolean;
  shouldShowCancel: boolean;
  productCount: number;
  product_name:any;
  category_id:any;
  hsn_code:any;
  hsn_description:any;
  category:any;
  description:any;
  published:any


  meta: any;
  data: any;
  data1: any;
  follower: any;
  searchTerm: any;
  page = 1;
  maximumPages = 100;
  apiService: any;
  id: any;


  isSearchResults: boolean;
  isSearchValidated: boolean;
  searchParams = {
    search:"",
  };

  defaultName: any;
  productProvider: any;
  toggled: boolean;
  statusData: any;
  isValidated: any;
  constructor(
    public router: Router,
    public api: ApiService,
    public userData: UserData,
    public alertCtrl: AlertController,
    private http: HttpClient,
    public storage: Storage
  ) {}

  ngOnInit() {
  }

  scrollToTop() {
    this.content.scrollToTop(400);
  }

  openSearchBox() {
    this.scrollToTop();
    this.isEnableSearch = true;
  }


  hideSearchBox() {
    this.isEnableSearch = false;
    this.isSearchResults = false;
  }

  
  resetSearch() {
    this.resetSearchBox();
    this.hideSearchBox();
  }
  resetSearchBox() {
    this.searchParams = {
      search:"",
    };
  }

  applySearch(event: any) {
    this.getSearchProduct();
  }

  async getSearchProduct() {
    this.isSearchValidated = true;

    if (
      this.searchParams.search == ""
    ) {
      this.isSearchValidated = false;
    }

    if (this.isSearchValidated === true) {
      this.isSearchResults = true;

      // await this.api.showLoader("Searching...");

      const token = await this.userData.getToken();

      const apiName = `api/v1/followers/search?search=${this.searchParams.search}`;
      const apiUrl = this.api.restApiUrl + apiName;
      const httpOptions = {
        headers: new HttpHeaders({ Authorization: token }),
      };

      console.log(`this.searchParams`, this.searchParams);

      this.http.get(apiUrl, httpOptions).subscribe(
        (response: any) => {
          console.log("getSearchfollower response", response);
          if (response.status === 1) {
            this.follower = response.data;
            this.productCount = response.data.length;
            this.isEnableSearch = false;
            this.api.dismissLoader();
          } else if (response.statusCode === 401) {
            this.api.clearStorageAndRedirectToLogin(response.message);
            this.api.dismissLoader();
          } else {
            this.api.presentValidateToast(response.message);
            this.api.dismissLoader();
          }
        },
        (error: any) => {
          console.log("error", error.error);
          this.api.presentValidateToast(error.error.message);
          this.api.dismissLoader();
        }
      );
    }
  }
  
  async submitStatus(id: any) {
    //console.log(`form`, form);
    console.log(id);
  
   this.isValidated = true;
   
   const token = await this.userData.getToken();
   console.log('token', token);
  
   if (this.isValidated) {
    const postParams = {
      following_id: id,
    };
  
     console.log(postParams);
      this.addStatus(postParams, id);
    }
    
  
  }
  
  async addStatus(postParams: any, id: any) {
  
   if (postParams) {
  
     console.log(`postParams`, postParams);
     await this.api.showLoader('Saving...');
  
     const token = await this.userData.getToken();
     // console.log('token', token);
  
     const apiName = `api/v1/following`;
     const apiUrl = this.api.restApiUrl + apiName;
     const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
  
     this.http.post(apiUrl, postParams, httpOptions).subscribe((response: any) => {
       console.log('status response', response);
       if (response.status == 1) {
         this.statusData = response.data;
         console.log("status data", this.statusData);
         this.api.presentSuccessToast(response.message);
         this.api.dismissLoader();
        //  this.router.navigateByUrl(`/my-product`);
        //  form.reset();
       } else if (response.statusCode === 401) {
         this.api.clearStorageAndRedirectToLogin(response.message);
         this.api.dismissLoader();
       } else {
         this.api.presentValidateToast(response.message);
         this.api.dismissLoader();
       }
     }, (error: any) => {
       console.log('error', error.error);
       this.api.presentValidateToast(error.error.message);
       this.api.dismissLoader();
     });
   }
  }
  
}
