import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SearchFollowerPageRoutingModule } from './search-follower-routing.module';

import { SearchFollowerPage } from './search-follower.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SearchFollowerPageRoutingModule
  ],
  declarations: [SearchFollowerPage]
})
export class SearchFollowerPageModule {}
