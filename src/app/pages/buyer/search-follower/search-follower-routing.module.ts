import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchFollowerPage } from './search-follower.page';

const routes: Routes = [
  {
    path: '',
    component: SearchFollowerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SearchFollowerPageRoutingModule {}
