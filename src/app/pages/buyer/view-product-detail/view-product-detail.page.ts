import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../../providers/user-data';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-view-product-detail',
  templateUrl: './view-product-detail.page.html',
  styleUrls: ['./view-product-detail.page.scss'],
})
export class ViewProductDetailPage implements OnInit {
  id: any;
  data: any;

  constructor(
    public router: Router,
    public api: ApiService,
    public userData: UserData,
    public route: ActivatedRoute,
    public alertCtrl: AlertController,
  ) { 
    this.id = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.getOrderDetails();
  }

  async getOrderDetails() {

    await this.api.showLoader();

    const token = await this.userData.getToken();
    console.log('token', token)
    this.api.getOrderInfo(token, this.id).subscribe((response: any) => {
      console.log('getOrderDetails response', response);
      if (response.status === 1) {
        this.data = response.data;
       // this.product =   (this.product && Array.isArray(this.product)) ? this.product.concat(response.data): response.data;
       console.log(this.data);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }
 


}
