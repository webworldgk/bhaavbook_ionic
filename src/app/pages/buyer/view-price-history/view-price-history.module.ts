import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewPriceHistoryPageRoutingModule } from './view-price-history-routing.module';

import { ViewPriceHistoryPage } from './view-price-history.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewPriceHistoryPageRoutingModule
  ],
  declarations: [ViewPriceHistoryPage]
})
export class ViewPriceHistoryPageModule {}
