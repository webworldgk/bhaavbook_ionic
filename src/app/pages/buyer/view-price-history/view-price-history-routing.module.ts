import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewPriceHistoryPage } from './view-price-history.page';

const routes: Routes = [
  {
    path: '',
    component: ViewPriceHistoryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewPriceHistoryPageRoutingModule {}
