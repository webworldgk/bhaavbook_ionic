import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../../providers/user-data';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';


@Component({
  selector: 'app-view-price-history',
  templateUrl: './view-price-history.page.html',
  styleUrls: ['./view-price-history.page.scss'],
})
export class ViewPriceHistoryPage implements OnInit {
  page: any;
  product: any;
  maximumPages: any;
  searchTerm: any;
  data: any;
  leadId: any;
  product_name: any;
  category_id:any;
  hsn_code:any;
  hsn_description:any;
  category:any;
  description:any;
  published:any;
  search:any;

  
  constructor(
    public router: Router,
    public api: ApiService,
    public userData: UserData,
    public alertCtrl: AlertController,
    public route: ActivatedRoute,
    public activatedRoute: ActivatedRoute,
  ) { 
    this.leadId = this.route.snapshot.paramMap.get('id');
    console.log(`this.Id`, this.leadId);
  }


  ngOnInit() {
  }

  ionViewDidEnter() {
    this.getProductDetails();
    // this.getProductName();
  }

  async getProductDetails() {
    this.leadId = this.route.snapshot.paramMap.get('id');

    await this.api.showLoader();

    const token = await this.userData.getToken();
    console.log('token', token)
    this.api.getPriceInfo(token,this.leadId).subscribe((response: any) => {
      console.log('getPriceInfo response', response);
      if (response.status === 1) {
        this.data = response.data;
        console.log(this.data);
        // this.product =   (this.product && Array.isArray(this.product)) ? this.product.concat(response.data): response.data;

        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
      });

  }
  
  async getProductName(infiniteScroll?) {

    await this.api.showLoader();

    const token = await this.userData.getToken();
    console.log('token', token)
    this.api.getProductInfo(token, this.page,this.product_name,this.category_id,this.hsn_code,this.hsn_description,this.category,this.description,this.published,this.search).subscribe((response: any) => {
      console.log('getProfileInfo response', response);
      if (response.status === 1) {
        this.product = response.data;
        // this.product =   (this.product && Array.isArray(this.product)) ? this.product.concat(response.data): response.data;

        if(infiniteScroll){
          infiniteScroll.target.complete();
        }
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }
  filterItems(searchTerm) {
    return this.product.filter(product => product.product_name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1);
  }


  setFilteredItems() {
    this.product = this.filterItems(this.searchTerm);
  }

  
  onBack() {
    this.router.navigate(['dashboard']);
  }
  addCategory(){
    this.router.navigate(['/add-category']);
  }
}

