import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdateProfilePagePageRoutingModule } from './update-profile-page-routing.module';

import { UpdateProfilePagePage } from './update-profile-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UpdateProfilePagePageRoutingModule
  ],
  declarations: [UpdateProfilePagePage]
})
export class UpdateProfilePagePageModule {}
