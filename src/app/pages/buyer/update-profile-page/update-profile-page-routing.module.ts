import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdateProfilePagePage } from './update-profile-page.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateProfilePagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdateProfilePagePageRoutingModule {}
