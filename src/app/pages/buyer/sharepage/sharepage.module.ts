import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SharepagePageRoutingModule } from './sharepage-routing.module';

import { SharepagePage } from './sharepage.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharepagePageRoutingModule
  ],
  declarations: [SharepagePage]
})
export class SharepagePageModule {}
