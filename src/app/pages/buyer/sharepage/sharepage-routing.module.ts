import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SharepagePage } from './sharepage.page';

const routes: Routes = [
  {
    path: '',
    component: SharepagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SharepagePageRoutingModule {}
