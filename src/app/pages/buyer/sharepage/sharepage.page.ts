import { Component, OnInit } from '@angular/core';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';

@Component({
  selector: 'app-sharepage',
  templateUrl: './sharepage.page.html',
  styleUrls: ['./sharepage.page.scss'],
})
export class SharepagePage implements OnInit {

  constructor(private socialSharing: SocialSharing) {}

  ngOnInit(): void {

  }
  sendShare(message, subject, url) {
    this.socialSharing.share(message, subject, null, url);
  }

}
