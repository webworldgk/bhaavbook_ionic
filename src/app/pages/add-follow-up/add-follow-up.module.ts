import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddFollowUpPageRoutingModule } from './add-follow-up-routing.module';

import { AddFollowUpPage } from './add-follow-up.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddFollowUpPageRoutingModule
  ],
  declarations: [AddFollowUpPage]
})
export class AddFollowUpPageModule {}
