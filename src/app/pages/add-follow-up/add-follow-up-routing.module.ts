import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddFollowUpPage } from './add-follow-up.page';

const routes: Routes = [
  {
    path: '',
    component: AddFollowUpPage
  },
  {
    path: ':customerId',
    component: AddFollowUpPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddFollowUpPageRoutingModule { }
