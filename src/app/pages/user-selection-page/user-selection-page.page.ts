import { Component, OnInit } from '@angular/core';
import { FormGroup, NgForm, Validators } from '@angular/forms';
import { FormBuilder, Validator } from '@angular/forms';
import { ActivatedRoute, Router ,} from '@angular/router';

@Component({
  selector: 'app-user-selection-page',
  templateUrl: './user-selection-page.page.html',
  styleUrls: ['./user-selection-page.page.scss'],
})
export class UserSelectionPagePage implements OnInit {

  ionicForm: FormGroup;
  isSubmitted = false;

    
  followUpTypes: any[] = [
    { id: 1, name: 'Buyer' },
    { id: 2, name: 'Seller' },
 
  ];
  constructor(public formBuilder: FormBuilder,
    public router: Router,
    ) { }

  ngOnInit() {
    this.ionicForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(2)]],
      password: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
      mobile: ['', [Validators.required, Validators.pattern('^[0-9]+$')]]
    })
  }

  get errorControl() {
    return this.ionicForm.controls;
  }
  //  submitForm() {
  //   this.isSubmitted = true;
  //   if (!this.ionicForm.valid) {
  //     // console.log('Please provide all the required values!')
  //     // return false;
  //   } else {
  //     console.log(this.ionicForm.value)
  //     this.router.navigateByUrl('/package-page');
  //   }
  // }
  onsubmit(){
    this.router.navigateByUrl('/package-page');
  }

}
