import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserSelectionPagePage } from './user-selection-page.page';

const routes: Routes = [
  {
    path: '',
    component: UserSelectionPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserSelectionPagePageRoutingModule {}
