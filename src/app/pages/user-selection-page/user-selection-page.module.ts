import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UserSelectionPagePageRoutingModule } from './user-selection-page-routing.module';

import { UserSelectionPagePage } from './user-selection-page.page';
import { ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    UserSelectionPagePageRoutingModule
  ],
  declarations: [UserSelectionPagePage]
})
export class UserSelectionPagePageModule {}
