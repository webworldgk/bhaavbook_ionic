import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController, IonContent } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../providers/user-data';


@Component({
  selector: 'app-leads',
  templateUrl: './leads.page.html',
  styleUrls: ['./leads.page.scss'],
})
export class LeadsPage implements OnInit {

  @ViewChild(IonContent) content: IonContent;

  isBack: boolean;
  leadLists: any;
  leadCount: number;

  dStartDate = '';
  dEndDate = '';
  leadStatus: any;

  isEnableSearch: boolean;
  isSearchResults: boolean;
  isSearchValidated: boolean;
  searchParams = {
    project_name: '',
    status: '',
    priority: '',
    created_by: '',
  };

  sSearchCreatedBy: any = {};

  constructor(
    public router: Router,
    public userData: UserData,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public api: ApiService,
    private http: HttpClient,
  ) {
    this.isBack = (this.route.snapshot.paramMap.get('back') == '1') ? true : false;
    console.log(`this.isBack`, this.isBack);

    this.leadStatus = this.route.snapshot.paramMap.get('leadStatus');
    console.log(this.leadStatus);
  }

  ngOnInit() {
    if (localStorage.getItem('refreshContents') !== '1') {
      this.getLeadListing();
    }
  }

  ionViewDidEnter() {
    if (localStorage.getItem('refreshContents') == '1') {
      this.getLeadListing();
    }
    if (this.api.subTypes.length == 0) {
      const storeVariables = true;
      this.api.doStoreMasters(storeVariables);
    }
  }

  async getLeadListing() {

    await this.api.showLoader();

    const token = await this.userData.getToken();
    // console.log('token', token);

    this.api.getLeadListing(token, this.leadStatus, this.dStartDate, this.dEndDate).subscribe((response: any) => {
      console.log('getLeadListing response', response.data);
      if (response.status === 1) {
        this.leadLists = response.data;
        this.leadCount = response.data.length;
        this.api.dismissLoader();
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });

  }

  scrollToTop() {
    this.content.scrollToTop(400);
  }

  openSearchBox() {
    this.scrollToTop();
    this.isEnableSearch = true;
  }

  hideSearchBox() {
    this.isEnableSearch = false;
    this.isSearchResults = false;
  }

  resetSearchBox() {
    this.searchParams = {
      project_name: '',
      status: '',
      priority: '',
      created_by: '',
    }
  }

  applySearch() {
    this.getSearchLeads();
  }

  async getSearchLeads() {

    this.isSearchValidated = true;

    if (
      this.searchParams.project_name == '' &&
      this.searchParams.status == '' &&
      this.searchParams.priority == '' &&
      this.searchParams.created_by == ''
    ) {
      this.api.presentValidateToast('Minimum one item is mandatory.', 4000, 'danger');
      this.isSearchValidated = false;
    }

    if (this.isSearchValidated === true) {

      this.isSearchResults = true;

      await this.api.showLoader('Searching...');

      const token = await this.userData.getToken();
      // console.log('token', token);

      const apiName = `api/v1/leads/globalsearch`;
      const apiUrl = this.api.restApiUrl + apiName;
      const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

      console.log(`this.searchParams`, this.searchParams);

      this.http.post(apiUrl, this.searchParams, httpOptions).subscribe((response: any) => {
        console.log('getSearchLeads response', response);
        if (response.status === 1) {

          this.leadLists = response.data;
          this.leadCount = response.data.length;
          this.isEnableSearch = false;
          this.api.dismissLoader();

        } else if (response.statusCode === 401) {
          this.api.clearStorageAndRedirectToLogin(response.message);
          this.api.dismissLoader();

        } else {
          this.api.presentValidateToast(response.message);
          this.api.dismissLoader();
        }
      }, (error: any) => {

        console.log('error', error.error);
        this.api.presentValidateToast(error.error.message);
        this.api.dismissLoader();
      });
    }

  }

  resetSearch() {
    this.resetSearchBox();
    this.hideSearchBox();
    this.getLeadListing();
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}
