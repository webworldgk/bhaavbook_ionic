import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddLeadNotesPageRoutingModule } from './add-lead-notes-routing.module';

import { AddLeadNotesPage } from './add-lead-notes.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddLeadNotesPageRoutingModule
  ],
  declarations: [AddLeadNotesPage]
})
export class AddLeadNotesPageModule {}
