import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../providers/user-data';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-add-lead-notes',
  templateUrl: './add-lead-notes.page.html',
  styleUrls: ['./add-lead-notes.page.scss'],
})
export class AddLeadNotesPage implements OnInit {

  leadId: any;
  leadNotes: any;
  isValidated: boolean;

  constructor(
    public router: Router,
    public api: ApiService,
    private http: HttpClient,
    public userData: UserData,
    public storage: Storage,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
  ) {
    this.leadId = this.route.snapshot.paramMap.get('leadId');
    console.log(`this.leadId`, this.leadId);
  }

  ngOnInit() { }

  submitLeadNotes(form: NgForm) {
    // console.log(`form`, form);

    this.isValidated = true;

    if (!this.leadNotes) {
      this.api.presentValidateToast('Notes is required.', 4000, 'danger');
      this.isValidated = false;
    }

    if (this.isValidated) {
      const postParams = {
        lead_id: this.leadId,
        notes: this.leadNotes
      };

      console.log(`postParams`, postParams);
      this.addLeadNotes(postParams, form);
    }
  }

  async addLeadNotes(postParams: any, form: NgForm) {

    if (postParams) {

      await this.api.showLoader('Saving...');

      const token = await this.userData.getToken();
      // console.log('token', token);

      const apiName = `api/v1/leadnotes`;
      const apiUrl = this.api.restApiUrl + apiName;
      const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

      this.http.post(apiUrl, postParams, httpOptions).subscribe((response: any) => {
        console.log('addLeadNotes response', response);
        if (response.status == 1) {
          this.api.presentSuccessToast(response.message);
          this.api.dismissLoader();
          this.router.navigateByUrl(`/leads-detail/${this.leadId}/6`);
          form.reset();
        } else if (response.statusCode === 401) {
          this.api.clearStorageAndRedirectToLogin(response.message);
          this.api.dismissLoader();
        } else {
          this.api.presentValidateToast(response.message);
          this.api.dismissLoader();
        }
      }, (error: any) => {
        console.log('error', error.error);
        this.api.presentValidateToast(error.error.message);
        this.api.dismissLoader();
      });
    }
  }

  backToLeadDashbaord() {
    if (this.leadId) {
      this.router.navigate(['leads-detail', this.leadId, 6]); // 5. Notes
    } else {
      this.router.navigate(['leads-dashboard']);
    }
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}