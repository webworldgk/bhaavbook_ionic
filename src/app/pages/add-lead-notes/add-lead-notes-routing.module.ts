import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddLeadNotesPage } from './add-lead-notes.page';

const routes: Routes = [
  {
    path: '',
    component: AddLeadNotesPage
  },{
    path: ':leadId',
    component: AddLeadNotesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddLeadNotesPageRoutingModule {}
