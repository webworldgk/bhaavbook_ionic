import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../providers/user-data';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-add-reminder',
  templateUrl: './add-reminder.page.html',
  styleUrls: ['./add-reminder.page.scss'],
})
export class AddReminderPage implements OnInit {

  todaysDate: any;
  customerId: any;
  isValidated = false;

  customActionCommonOptions: any = {
    header: 'Select...'
  };

  reminderInfo = {
    date: '',
    description: ''
  };

  constructor(
    public router: Router,
    public api: ApiService,
    private http: HttpClient,
    public userData: UserData,
    public storage: Storage,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
  ) {
    this.customerId = this.route.snapshot.paramMap.get('customerId');
  }

  ngOnInit() { }

  ionViewDidEnter() {
    this.prepareDate();
  }

  prepareDate() {
    const dInputDate = new Date();
    const iInputMonth = (dInputDate.getMonth() + 1);
    const sInputMonth = iInputMonth.toLocaleString().length == 1 ? '0' + iInputMonth.toLocaleString() : iInputMonth.toLocaleString();
    const sInputDate = dInputDate.getDate().toLocaleString().length == 1 ? '0' + dInputDate.getDate().toLocaleString() : dInputDate.getDate().toLocaleString();
    this.todaysDate = dInputDate.getFullYear() + '-' + sInputMonth + '-' + sInputDate;
    console.log(`this.todaysDate`, this.todaysDate);
  }

  submitReminder(form: NgForm) {

    // console.log(`form`, form);
    this.isValidated = true;

    if (this.reminderInfo.date == '') {
      this.api.presentValidateToast('Reminder Date is required.', 4000, 'danger');
      this.isValidated = false;
    }

    if (this.reminderInfo.description == '') {
      this.api.presentValidateToast('Description is required.', 4000, 'danger');
      this.isValidated = false;
    }

    if (this.isValidated) {

      const postParams = {
        customer_id: this.customerId,
        reminder_date: this.reminderInfo.date,
        reminder_description: this.reminderInfo.description
      };

      console.log(`postParams`, postParams);
      this.addRemainder(postParams);
    }
  }

  async addRemainder(postParams: any) {

    if (postParams) {

      await this.api.showLoader('Saving...');

      const token = await this.userData.getToken();
      // console.log('token', token);

      const apiName = `api/v1/raw_customers_reminder`;
      const apiUrl = this.api.restApiUrl + apiName;
      const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

      this.http.post(apiUrl, postParams, httpOptions).subscribe((response: any) => {
        console.log('addRemainder response', response);
        if (response.status == 1) {
          this.api.presentSuccessToast(response.message);
          this.api.dismissLoader();
          this.router.navigateByUrl(`/customers-detail/${this.customerId}/3`);

        } else if (response.statusCode === 401) {
          this.api.clearStorageAndRedirectToLogin(response.message);
          this.api.dismissLoader();

        } else {
          this.api.presentValidateToast(response.message);
          this.api.dismissLoader();
        }
      }, (error: any) => {

        console.log('error', error.error);
        this.api.presentValidateToast(error.error.message);
        this.api.dismissLoader();
      });
    }
  }

  backToRawCustomerDashbaord() {
    this.router.navigate(['customers-dashboard']);
  }

  async ionViewDidLeave() {
    this.reminderInfo.date = '';
    this.reminderInfo.description = '';
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}
