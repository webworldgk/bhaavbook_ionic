import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController, IonContent } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../providers/user-data';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.page.html',
  styleUrls: ['./customer.page.scss'],
})
export class CustomerPage implements OnInit {

  @ViewChild(IonContent) content: IonContent;

  isBack: boolean;
  customerList: any;
  customerCount: number;
  isEnableSearch: boolean;

  customerSubType: any;

  postParams = {
    customerSubType: '',
    dStartDate: '',
    dEndDate: '',
  };

  isSearchResults: boolean;
  isSearchValidated: boolean;
  searchParams = {
    customer_name: '',
    customer_mobile: '',
    customer_email: '',
    firm_name: '',
    owner_info: '',
    project_info: '',
    created_by: '',
  };

  constructor(
    public router: Router,
    public userData: UserData,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public api: ApiService,
    private http: HttpClient,
  ) {
    this.isBack = (this.route.snapshot.paramMap.get('back') == '1') ? true : false;
    console.log(`this.isBack`, this.isBack);

    this.customerSubType = this.route.snapshot.paramMap.get('subType');
    console.log('this.customerSubType', this.customerSubType);
  }

  ngOnInit() {
    if (localStorage.getItem('refreshContents') !== '1') {
      this.getCustomers();
    }
  }

  ionViewDidEnter() {
    if (localStorage.getItem('refreshContents') == '1') {
      this.getCustomers();
    }
    if (this.api.subTypes.length == 0) {
      const storeVariables = true;
      this.api.doStoreMasters(storeVariables);
    }
  }

  async getCustomers() {

    this.isSearchResults = false;

    await this.api.showLoader();

    const token = await this.userData.getToken();
    this.postParams.customerSubType = this.customerSubType;

    this.api.getCustomerListing(token, this.postParams).subscribe((response: any) => {
      console.log('getCustomerListing response', response.data);
      if (response.status === 1) {
        this.customerList = response.data;
        this.customerCount = response.data.length;

        // reset in order to not refresh again
        localStorage.setItem('refreshContents', '0');

        this.api.dismissLoader();
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });

  }

  scrollToTop() {
    this.content.scrollToTop(400);
  }

  openSearchBox() {
    this.scrollToTop();
    this.isEnableSearch = true;
  }

  hideSearchBox() {
    this.isEnableSearch = false;
    this.isSearchResults = false;
  }

  resetSearchBox() {
    this.searchParams = {
      customer_name: '',
      customer_mobile: '',
      customer_email: '',
      firm_name: '',
      owner_info: '',
      project_info: '',
      created_by: '',
    }
  }

  applySearch() {
    this.getSearchCustomers();
  }

  async getSearchCustomers() {

    this.isSearchValidated = true;

    if (
      this.searchParams.customer_name == '' &&
      this.searchParams.customer_mobile == '' &&
      this.searchParams.customer_email == '' &&
      this.searchParams.firm_name == '' &&
      this.searchParams.owner_info == '' &&
      this.searchParams.project_info == '' &&
      this.searchParams.created_by == ''
    ) {
      this.api.presentValidateToast('Minimum one item is mandatory.', 4000, 'danger');
      this.isSearchValidated = false;
    }

    if (this.isSearchValidated === true) {

      this.isSearchResults = true;

      await this.api.showLoader('Searching...');

      const token = await this.userData.getToken();
      // console.log('token', token);

      const apiName = `api/v1/raw_customers/globalsearch`;
      const apiUrl = this.api.restApiUrl + apiName;
      const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

      console.log(`this.searchParams`, this.searchParams);

      this.http.post(apiUrl, this.searchParams, httpOptions).subscribe((response: any) => {
        console.log('getSearchCustomers response', response);
        if (response.status === 1) {

          this.customerList = response.data;
          this.customerCount = response.data.length;
          this.isEnableSearch = false;
          this.api.dismissLoader();

        } else if (response.statusCode === 401) {
          this.api.clearStorageAndRedirectToLogin(response.message);
          this.api.dismissLoader();

        } else {
          this.api.presentValidateToast(response.message);
          this.api.dismissLoader();
        }
      }, (error: any) => {

        console.log('error', error.error);
        this.api.presentValidateToast(error.error.message);
        this.api.dismissLoader();
      });
    }

  }

  resetSearch() {
    this.resetSearchBox();
    this.hideSearchBox();
    this.getCustomers();
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}
