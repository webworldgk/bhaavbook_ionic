import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../providers/user-data';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-add-lead-gallery',
  templateUrl: './add-lead-gallery.page.html',
  styleUrls: ['./add-lead-gallery.page.scss'],
})
export class AddLeadGalleryPage implements OnInit {

  leadId: any;
  galleryType: any;
  galleryFile: any;
  actualFile: any;
  isValidated: boolean;
  acceptableExtensions = 'image/*';

  customActionCommonOptions: any = {
    header: 'Select...'
  };

  configType: any[] = [
    {
      id: 1,
      name: 'Image'
    },
    {
      id: 2,
      name: 'Quotation / Invoice'
    }
  ];

  constructor(
    public router: Router,
    public api: ApiService,
    private http: HttpClient,
    public userData: UserData,
    public storage: Storage,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
  ) {
    this.leadId = this.route.snapshot.paramMap.get('leadId');
    console.log(`this.leadId`, this.leadId);
  }

  ngOnInit() { }

  submitGallery(form: NgForm) {
    // console.log(`form`, form);

    this.isValidated = true;

    if (!this.galleryType) {
      this.api.presentValidateToast('Type is required.', 4000, 'danger');
      this.isValidated = false;
    }

    if (!this.actualFile) {
      this.api.presentValidateToast('File is required.', 4000, 'danger');
      this.isValidated = false;
    }

    if (this.isValidated) {
      const postParams = new FormData();
      postParams.append('lead_id', this.leadId);
      postParams.append('type', this.galleryType);
      postParams.append('filename', this.actualFile, this.actualFile?.name);
      // console.log(`postParams`, postParams);

      this.addLeadGallery(postParams, form);
    }

  }

  async addLeadGallery(postParams: any, form: NgForm) {

    if (postParams) {

      await this.api.showLoader('Saving...');

      const token = await this.userData.getToken();
      // console.log('token', token);

      const apiName = `api/v1/leadgalleries`;
      const apiUrl = this.api.restApiUrl + apiName;
      const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

      this.http.post(apiUrl, postParams, httpOptions).subscribe((response: any) => {
        console.log('addLeadGallery response', response);
        if (response.status == 1) {
          this.api.presentSuccessToast(response.message);
          this.api.dismissLoader();
          this.router.navigateByUrl(`/leads-detail/${this.leadId}/5`);
          form.reset();
        } else if (response.statusCode === 401) {
          this.api.clearStorageAndRedirectToLogin(response.message);
          this.api.dismissLoader();
        } else {
          this.api.presentValidateToast(response.message);
          this.api.dismissLoader();
        }
      }, (error: any) => {
        console.log('error', error.error);
        this.api.presentValidateToast(error.error.message);
        this.api.dismissLoader();
      });
    }
  }

  changeGalleryType(event: any) {
    const selectedValue = event.detail.value;
    console.log(`selectedValue`, selectedValue);
    if (selectedValue === '2') {
      this.acceptableExtensions = '.pdf';
    } else {
      this.acceptableExtensions = 'image/*';
    }
  }

  onFileChange(fileChangeEvent: any) {
    this.actualFile = fileChangeEvent.target.files[0];
    console.log(`this.actualFile`, this.actualFile);
  }

  backToLeadDashbaord() {
    if (this.leadId) {
      this.router.navigate(['leads-detail', this.leadId, 5]); // 5. Gallery
    } else {
      this.router.navigate(['leads-dashboard']);
    }
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}
