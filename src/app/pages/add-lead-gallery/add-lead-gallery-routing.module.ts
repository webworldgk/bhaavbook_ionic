import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddLeadGalleryPage } from './add-lead-gallery.page';

const routes: Routes = [
  {
    path: '',
    component: AddLeadGalleryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddLeadGalleryPageRoutingModule {}
