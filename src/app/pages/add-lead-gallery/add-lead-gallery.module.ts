import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddLeadGalleryPageRoutingModule } from './add-lead-gallery-routing.module';

import { AddLeadGalleryPage } from './add-lead-gallery.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddLeadGalleryPageRoutingModule
  ],
  declarations: [AddLeadGalleryPage]
})
export class AddLeadGalleryPageModule {}
