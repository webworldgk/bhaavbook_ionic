import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../providers/user-data';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-convert-lead',
  templateUrl: './convert-lead.page.html',
  styleUrls: ['./convert-lead.page.scss'],
})
export class ConvertLeadPage implements OnInit {

  token: any;
  leadId: any;
  customerId: any;
  isValidated = false;
  pageTitle = 'Create';

  customActionCommonOptions: any = {
    header: 'Select...'
  };

  leadInfo = {
    projectName: '',
    projectType: '',
    customerName: '',
    customerMobile: '',
    leadName: '',
    generatedDate: '',
    leadSource: '',
    priorityId: '',
    statusId: '',
    estimatedCost: '',
    createdBy: '',
    assignedTo: '',
    notes: '',
  };

  displayProjectInfo = false;
  projectArrowIcon = 'chevron-down-outline';

  constructor(
    public router: Router,
    public api: ApiService,
    public storage: Storage,
    private http: HttpClient,
    public userData: UserData,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
  ) {
    this.leadId = this.route.snapshot.paramMap.get('leadId');
    this.customerId = this.route.snapshot.paramMap.get('customerId');
    console.log(`this.leadId`, this.leadId);
    console.log(`this.customerId`, this.customerId);
  }

  ngOnInit() { }

  async ionViewDidEnter() {
    this.prepareMasters();
    this.token = await this.userData.getToken();
    if (this.leadId) {
      this.pageTitle = 'Update';
      this.getLeadInfo();
    } else {
      this.getCustomerInfo();
    }
  }

  async prepareMasters() {
    if (this.api.subTypes.length === 0) {
      const storeVariables = true;
      this.api.doStoreMasters(storeVariables);
    }
  }

  async getLeadInfo() {

    await this.api.showLoader();

    this.api.getLeadDetails(this.token, this.leadId).subscribe((response: any) => {
      console.log('getLeadDetails response', response);
      if (response.status === 1) {
        this.leadInfo.leadName = response.data.title;
        this.leadInfo.projectName = response.data.title;
        if (response.data.project_type_id) {
          this.leadInfo.projectType = response.data.project_type_id.toString();
        }
        this.leadInfo.customerName = response.data.customer_name;
        this.leadInfo.customerMobile = response.data.customer_mobile;
        this.leadInfo.generatedDate = response.data.generated_date;
        this.leadInfo.leadSource = response.data.source_of_lead_id.toString();
        this.leadInfo.priorityId = response.data.lead_priority_id.toString();
        this.leadInfo.statusId = response.data.status_id.toString();
        this.leadInfo.estimatedCost = response.data.estimated_cost;
        this.leadInfo.assignedTo = response.data.assigned_to_id.toString();
        this.leadInfo.notes = response.data.notes;
        // console.log(`this.leadInfo`, this.leadInfo);
        this.api.dismissLoader();
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }

  async getCustomerInfo() {

    await this.api.showLoader();

    this.api.getRawCustomerDetails(this.token, this.customerId).subscribe((response: any) => {
      console.log('getRawCustomerDetails response', response);
      if (response.status === 1) {
        this.leadInfo.leadName = response.data.project_name;
        this.leadInfo.projectName = response.data.project_name;
        this.leadInfo.projectType = response.data.project_type_id ? response.data.project_type_id.toString() : '';
        this.leadInfo.customerName = response.data.customer_name;
        this.leadInfo.customerMobile = response.data.customer_mobile;
        this.api.dismissLoader();
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }

  submitCreateLead(form: NgForm) {

    let generatedDate: any;
    if (this.leadInfo.generatedDate) {
      const sNewDateObj = new Date(this.leadInfo.generatedDate);
      const iNewMonth = sNewDateObj.getMonth() + 1;
      const sNewMonth = iNewMonth < 9 ? '0' + iNewMonth : iNewMonth;
      const sNewDate = sNewDateObj.getDate() < 9 ? '0' + sNewDateObj.getDate() : sNewDateObj.getDate();
      const sNewHours = sNewDateObj.getHours() < 9 ? '0' + sNewDateObj.getHours() : sNewDateObj.getHours();
      const sNewMinutes = sNewDateObj.getSeconds() < 9 ? '0' + sNewDateObj.getSeconds() : sNewDateObj.getSeconds();
      const sNewSeconds = sNewDateObj.getSeconds() < 9 ? '0' + sNewDateObj.getSeconds() : sNewDateObj.getSeconds();
      generatedDate = sNewDateObj.getFullYear() + '-' + sNewMonth + '-' + sNewDate + ' ' + sNewHours + ':' + sNewMinutes + ':' + sNewSeconds;
      console.log(`generatedDate`, generatedDate);
    }

    const postParams = {
      customer_id: this.customerId,
      title: this.leadInfo.leadName,
      source_of_lead: this.leadInfo.leadSource ? this.leadInfo.leadSource : null,
      lead_priority: this.leadInfo.priorityId ? this.leadInfo.priorityId : null,
      generated_date: generatedDate,
      status: this.leadInfo.statusId ? this.leadInfo.statusId : null,
      estimated_cost: this.leadInfo.estimatedCost,
      assigned_to: this.leadInfo.assignedTo ? this.leadInfo.assignedTo : null,
      // created_by: this.leadInfo.createdBy,
      notes: this.leadInfo.notes,
    };
    console.log(`postParams`, postParams);

    if (this.leadId) {
      this.updateConvertLead(postParams, form);
    } else {
      this.saveConvertLead(postParams, form);
    }
  }

  async saveConvertLead(postParams: any, form: NgForm) {

    await this.api.showLoader();

    const apiName = `api/v1/leads`;
    const apiUrl = this.api.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: this.token }) };

    this.http.post(apiUrl, postParams, httpOptions).subscribe((response: any) => {
      console.log('saveConvertLead response', response);
      if (response.status === 1) {
        this.api.presentSuccessToast(response.message);
        this.api.dismissLoader();
        this.router.navigateByUrl(`/leads-detail/${response.data.id}`);
        form.reset();
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    }, (error: any) => {
      console.log('error', error.error);
      this.api.presentValidateToast(error.error.message);
      this.api.dismissLoader();
    });
  }

  async updateConvertLead(postParams: any, form: NgForm) {

    await this.api.showLoader();

    const apiName = `api/v1/leads/${this.leadId}`;
    const apiUrl = this.api.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: this.token }) };

    this.http.put(apiUrl, postParams, httpOptions).subscribe((response: any) => {
      console.log('updateConvertLead response', response);
      if (response.status == 1) {
        this.api.presentSuccessToast(response.message);
        this.api.dismissLoader();

        this.router.navigateByUrl(`/leads-detail/${this.leadId}`);
        form.reset();

      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();

      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    }, (error: any) => {

      console.log('error', error.error);
      this.api.presentValidateToast(error.error.message);
      this.api.dismissLoader();
    });
  }

  toggleProjectInfo() {
    this.displayProjectInfo = !this.displayProjectInfo;
    this.projectArrowIcon = (this.projectArrowIcon === 'chevron-down-outline') ? 'chevron-up-outline' : 'chevron-down-outline';
  }

  backToLeadDashbaord() {
    if (this.leadId) {
      this.router.navigate(['leads-detail', this.leadId]);
    } else {
      this.router.navigate(['leads-dashboard']);
    }
  }

}
