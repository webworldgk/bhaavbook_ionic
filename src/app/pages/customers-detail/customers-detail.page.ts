import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../providers/user-data';

@Component({
  selector: 'app-customers-detail',
  templateUrl: './customers-detail.page.html',
  styleUrls: ['./customers-detail.page.scss'],
})
export class CustomersDetailPage implements OnInit {

  rawCustomerInfo: any;
  rawCustomerFollowUp: any;
  rawCustomerReminder: any;
  rawCustomerId: any;

  rawCustomerInfoCount: number;
  rawCustomerFollowUpCount: number;
  rawCustomerReminderCount: number;

  dStartDate = '';
  dEndDate = '';
  displayView = 1;

  constructor(
    public router: Router,
    public userData: UserData,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public api: ApiService) {
    //  this.isBack = (this.route.snapshot.paramMap.get('back') == '1') ? true : false;
    // console.log(`this.isBack`, this.isBack);

    this.rawCustomerId = this.route.snapshot.paramMap.get('id');

    console.log(this.rawCustomerId);
    const displayView = parseInt(this.route.snapshot.paramMap.get('display'));
    if (displayView > 0) {
      this.displayView = displayView;
    }
  }

  ngOnInit() { }

  ionViewWillEnter() {
    this.getTabDetails();
  }

  getTabDetails() {

    if (this.displayView == 1) {
      this.getCustomerDetails();
    }

    if (this.displayView == 2) {
      this.getFollowUpListing();
    }

    if (this.displayView == 3) {
      this.getReminderListing();
    }
  }

  async getFollowUpListing() {
    console.log('follow up');

    await this.api.showLoader();

    const token = await this.userData.getToken();
    // console.log('token', token);

    this.api.getRawCustomerFollowUp(token, this.rawCustomerId, this.dStartDate, this.dEndDate).subscribe((response: any) => {
      console.log('getRawCustomerFollowUp response', response.data);
      if (response.status === 1) {
        this.rawCustomerFollowUp = response.data;
        this.rawCustomerFollowUpCount = response.data.length;
        this.api.dismissLoader();
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();

      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }

  async getReminderListing() {

    // console.log('Reminder');
    await this.api.showLoader();

    const token = await this.userData.getToken();

    this.api.getRawCustomerReminder(token, this.rawCustomerId, this.dStartDate, this.dEndDate).subscribe((response: any) => {
      console.log('getRawCustomerReminder response', response.data);
      if (response && response.data) {
        this.rawCustomerReminder = response.data;
        this.rawCustomerReminderCount = response.data.length;
        this.api.dismissLoader();
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }

  async getCustomerDetails() {
    const token = await this.userData.getToken();
    this.api.getRawCustomerDetails(token, this.rawCustomerId).subscribe((response: any) => {
      console.log('getCustomerDetails response', response.data);
      if (response.status === 1) {
        this.rawCustomerInfo = response.data;
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
      } else {
        this.api.presentValidateToast(response.message);
      }
    });

  }

  segmentChanged(event) {
    console.log(event.detail.value);
    this.displayView = event.detail.value;
    this.getTabDetails();
  }

  createRawDetail() {
    console.log(`this.displayView`, this.displayView);
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}
