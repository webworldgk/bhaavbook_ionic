import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CustomersDetailPage } from './customers-detail.page';

const routes: Routes = [
  {
    path: '',
    component: CustomersDetailPage
  },
  {
    path: ':display',
    component: CustomersDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomersDetailPageRoutingModule {}
