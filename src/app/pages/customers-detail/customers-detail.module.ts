import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CustomersDetailPageRoutingModule } from './customers-detail-routing.module';

import { CustomersDetailPage } from './customers-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CustomersDetailPageRoutingModule
  ],
  declarations: [CustomersDetailPage]
})
export class CustomersDetailPageModule {}
