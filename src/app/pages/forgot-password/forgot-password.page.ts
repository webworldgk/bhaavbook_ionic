import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../providers/user-data';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {

  inputMobile: string;
  submitted = false;
  isValidated: boolean;

  constructor(
    public router: Router,
    public api: ApiService,
    private http: HttpClient,
    public userData: UserData,
    public storage: Storage,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
  ) { }

  ngOnInit() { }

  ionViewDidEnter() { }

  async submitForgotPassword(form: NgForm) {
  
    this.submitted = true;
  

  if (form.valid) {

    await this.api.showLoader('Sending OTP...');

    const apiName = `api/v1/auth/forgot-password`;
    const apiUrl = this.api.restApiUrl + apiName;
    const postParams = { mobile_number: this.inputMobile, step: 1};

    this.http.post(apiUrl, postParams).subscribe((response: any) => {
      console.log('submitMobile response', response);
      if (response.status === 1) {
        this.api.presentSuccessToast(response.message);
        this.router.navigateByUrl(`/reset-password-otp/${this.inputMobile}`);
        this.api.dismissLoader();
        form.reset();
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message, 5000, 'danger');
        this.api.dismissLoader();
      }
    }, (error: any) => {
      console.log('error', error.error);
      this.api.presentValidateToast(error.error.message, 5000, 'danger');
      this.api.dismissLoader();
    });

  }
  }
  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}
