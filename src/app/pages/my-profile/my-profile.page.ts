import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Capacitor } from '@capacitor/core';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../providers/user-data';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.page.html',
  styleUrls: ['./my-profile.page.scss'],
})
export class MyProfilePage implements OnInit {

  user: any;
  summary: any;
  attendance: any;
  data: any;

  constructor(
    public router: Router,
    public api: ApiService,
    public userData: UserData,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private photoViewer: PhotoViewer,
  ) { }

  ngOnInit() { }

  ionViewDidEnter() {
    this.getProfileDetails();
  }

  async getProfileDetails() {

    await this.api.showLoader();

    // const token = await this.userData.getToken();
    // console.log('token', token)
    // this.api.getProfileInfo(token).subscribe((response: any) => {
    //   console.log('getProfileInfo response', response);
    //   if (response.status === 1) {
    //     this.user = response.data.user;
    //     this.summary = response.data.summary;
    //     this.attendance = response.data.attendance;
    //     this.api.dismissLoader();
    //   } else {
    //     this.api.presentValidateToast(response.message);
    //     this.api.dismissLoader();
    //   }
    // });
  }

  editProfile() {
    this.router.navigate(['update-profile']);
  }

  previewImage() {
    if (Capacitor.isNativePlatform()) {
      if (this.user.picture) {
        this.photoViewer.show(this.user.picture, 'Profile Picture', { share: false });
      }
    }
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}
