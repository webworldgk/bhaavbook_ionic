import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LeadFollowupListingsPage } from './lead-followup-listings.page';

const routes: Routes = [
  {
    path: '',
    component: LeadFollowupListingsPage
  },
  {
    path: ':followupfor',
    component: LeadFollowupListingsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LeadFollowupListingsPageRoutingModule {}
