import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../providers/user-data';

@Component({
  selector: 'app-lead-followup-listings',
  templateUrl: './lead-followup-listings.page.html',
  styleUrls: ['./lead-followup-listings.page.scss'],
})
export class LeadFollowupListingsPage implements OnInit {

  isBack: boolean;
  followUpLists: any;
  followUpCount: number;

  dStartDate = '';
  dEndDate = '';
  followUpFor: any;
  followUpForLabelName: any;
  followUpForLabel = {
    1: 'Today Follow Up',
    2: 'Missed Follow Up',
    3: 'All Follow Up'
  };

  constructor(
    public router: Router,
    public userData: UserData,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public api: ApiService,
  ) {
    this.isBack = (this.route.snapshot.paramMap.get('back') === '1') ? true : false;
    console.log(`this.isBack`, this.isBack);
    this.followUpFor = this.route.snapshot.paramMap.get('followupfor');

    this.setReminderForLabel();
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.getLeadFollowUpListings();
  }

  async getLeadFollowUpListings() {

    await this.api.showLoader();

    const token = await this.userData.getToken();

    this.api.getLeadFollowUpListings(token, this.followUpFor, this.dStartDate, this.dEndDate).subscribe((response: any) => {
      console.log('getLeadFollowUpListings response', response);
      if (response.status === 1) {
        this.followUpLists = response.data;
        this.followUpCount = response.data.length;
        this.api.dismissLoader();
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }

  setReminderForLabel() {
    if (this.followUpForLabel[this.followUpFor]) {
      this.followUpForLabelName = this.followUpForLabel[this.followUpFor];
    }
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}
