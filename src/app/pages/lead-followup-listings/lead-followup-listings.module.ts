import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LeadFollowupListingsPageRoutingModule } from './lead-followup-listings-routing.module';

import { LeadFollowupListingsPage } from './lead-followup-listings.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LeadFollowupListingsPageRoutingModule
  ],
  declarations: [LeadFollowupListingsPage]
})
export class LeadFollowupListingsPageModule {}
