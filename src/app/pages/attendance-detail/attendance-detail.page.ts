import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../providers/user-data';

@Component({
  selector: 'app-attendance-detail',
  templateUrl: './attendance-detail.page.html',
  styleUrls: ['./attendance-detail.page.scss'],
})
export class AttendanceDetailPage implements OnInit {
  attendanceInfo: any;
  attendanceId: any;

  constructor(
    public userData: UserData,
    public route: ActivatedRoute,
    public router: Router,
    public toastCtrl: ToastController,
    public api: ApiService
  ) {
    this.attendanceId = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.getAttendanceInfo();
  }

  async getAttendanceInfo() {

    await this.api.showLoader();

    const token = await this.userData.getToken();
    console.log('token', token);
    this.api.getAttendanceDetails(token, this.attendanceId).subscribe((response: any) => {
      console.log('getAttendanceInfo response', response);
      if (response && response.data) {
        this.attendanceInfo = response.data;
        this.api.dismissLoader();
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }
  // Dismiss the loader
  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }
}
