import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CustomerReminderListingsPage } from './customer-reminder-listings.page';

const routes: Routes = [
  {
    path: '',
    component: CustomerReminderListingsPage
  },{
    path: ':reminderFor',
    component: CustomerReminderListingsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomerReminderListingsPageRoutingModule {}
