import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../providers/user-data';

@Component({
  selector: 'app-customer-reminder-listings',
  templateUrl: './customer-reminder-listings.page.html',
  styleUrls: ['./customer-reminder-listings.page.scss'],
})
export class CustomerReminderListingsPage implements OnInit {

  isBack: boolean;
  reminderLists: any;
  reminderCount: number;

  dStartDate = '';
  dEndDate = '';
  reminderFor: any;
  reminderForLabelName: any;
  reminderForLabel = {
    1: 'Today Reminder',
    2: 'All Reminder'
  };

  constructor(
    public router: Router,
    public userData: UserData,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public api: ApiService,
  ) {
    this.isBack = (this.route.snapshot.paramMap.get('back') === '1') ? true : false;
    console.log(`this.isBack`, this.isBack);
    this.reminderFor = (this.route.snapshot.paramMap.get('reminderFor')) ? this.route.snapshot.paramMap.get('reminderFor') : '';

    this.setReminderForLabel();
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.getCustomerReminderListings();
  }

  async getCustomerReminderListings() {

    // Loader Showing
    await this.api.showLoader();

    const token = await this.userData.getToken();

    this.api.getCustomerReminderListings(token, this.reminderFor, this.dStartDate, this.dEndDate).subscribe((response: any) => {
      console.log('getCustomerReminderListings response', response);
      if (response && response.status === 1) {
        this.reminderLists = response.data;
        this.reminderCount = response.data.length;
        this.api.dismissLoader();
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }

  setReminderForLabel() {
    console.log('innnn', this.reminderForLabel);
    if (this.reminderForLabel[this.reminderFor]) {
      this.reminderForLabelName = this.reminderForLabel[this.reminderFor];
    }
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }
}
