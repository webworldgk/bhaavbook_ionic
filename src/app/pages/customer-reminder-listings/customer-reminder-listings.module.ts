import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CustomerReminderListingsPageRoutingModule } from './customer-reminder-listings-routing.module';

import { CustomerReminderListingsPage } from './customer-reminder-listings.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CustomerReminderListingsPageRoutingModule
  ],
  declarations: [CustomerReminderListingsPage]
})
export class CustomerReminderListingsPageModule {}
