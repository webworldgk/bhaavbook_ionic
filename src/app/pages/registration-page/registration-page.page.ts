import { Component, OnInit } from '@angular/core';
import { FormGroup, NgForm, Validators } from '@angular/forms';
import { FormBuilder, Validator } from '@angular/forms';
import { ActivatedRoute, Router ,} from '@angular/router';

import { UserOptions } from '../../interfaces/user-options';
import { LoadingController, ToastController } from '@ionic/angular';
import { AppComponent } from 'src/app/app.component';




@Component({
  selector: 'app-registration-page',
  templateUrl: './registration-page.page.html',
  styleUrls: ['./registration-page.page.scss'],
})
export class RegistrationPagePage implements OnInit {

  ionicForm: FormGroup;
  isSubmitted = false;
  constructor(public formBuilder: FormBuilder,
    public router: Router,
    ) { }
  ngOnInit() {
    this.ionicForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(2)]],
      password: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
      mobile: ['', [Validators.required, Validators.pattern('^[0-9]+$')]]
    })
  }
  get errorControl() {
    return this.ionicForm.controls;
  }
  submitForm() {
    this.isSubmitted = true;
    if (!this.ionicForm.valid) {
      console.log('Please provide all the required values!')
      return false;
    } else {
      console.log(this.ionicForm.value)
      this.router.navigateByUrl('/otp-page');
    }
  }

  // get name(){
  //   return this.registrationForm.get('name');
  // }
  // get mobilePin(){
  //   return this.registrationForm.get('mobilePin');
  // }
  // get password(){
  //   return this.registrationForm.get('password');
  // }

  // public errorMessages = {
  //   name:[
  //     {type: 'required', message: 'name is required'}
  //   ],
  //   mobilePin:[
  //     {type: 'required', message: 'mobile no is required'},
  //     {type: 'maxlength', message: 'mobile no is not more than 10'}
  //   ],
  //   password:[
  //     {type: 'required', message: 'password is required'},
  //     {type: 'maxlength', message: 'please enter vaild password'}
  //   ]
  // };

  // submitted = false;

  // registrationForm = this.formBuilder.group({
  //   name: ['',Validators.required],
  //   mobilePin: ['',[Validators.required, Validators.maxLength(10), Validators.pattern('^[+]*[(]{0,1}[0-9]{1,4}[)}{0,1}[-s./0.9]*$')]],
  //   password: ['',[Validators.required, Validators.maxLength(6)]],
  // });


  // constructor(
  //   public router: Router,
  //   private formBuilder: FormBuilder,
  //   public toastCtrl: ToastController,
  //   public loadingCtrl: LoadingController,
  //   public appComponent: AppComponent
  // ) { }

  // public submitreg() {
  //   console.log(this.registrationForm.value);
  //   //this.router.navigateByUrl('/otp-page');
  // }
  // ngOnInit() {
  // }
  // onBack() {
  //   this.router.navigate(['login-page']);
  // }


}
