import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CustomerFollowupListingsPageRoutingModule } from './customer-followup-listings-routing.module';

import { CustomerFollowupListingsPage } from './customer-followup-listings.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CustomerFollowupListingsPageRoutingModule
  ],
  declarations: [CustomerFollowupListingsPage]
})
export class CustomerFollowupListingsPageModule {}
