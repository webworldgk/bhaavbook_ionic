import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CustomerFollowupListingsPage } from './customer-followup-listings.page';

const routes: Routes = [
  {
    path: '',
    component: CustomerFollowupListingsPage
  },
  {
    path: ':followupfor',
    component: CustomerFollowupListingsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomerFollowupListingsPageRoutingModule {}
