import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PackageScreenPage } from './package-screen.page';

const routes: Routes = [
  {
    path: '',
    component: PackageScreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PackageScreenPageRoutingModule {}
