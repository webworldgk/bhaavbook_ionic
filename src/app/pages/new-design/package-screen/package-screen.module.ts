import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PackageScreenPageRoutingModule } from './package-screen-routing.module';

import { PackageScreenPage } from './package-screen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PackageScreenPageRoutingModule
  ],
  declarations: [PackageScreenPage]
})
export class PackageScreenPageModule {}
