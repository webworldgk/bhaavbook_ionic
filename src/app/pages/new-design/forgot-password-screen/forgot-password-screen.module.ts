import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ForgotPasswordScreenPageRoutingModule } from './forgot-password-screen-routing.module';

import { ForgotPasswordScreenPage } from './forgot-password-screen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ForgotPasswordScreenPageRoutingModule
  ],
  declarations: [ForgotPasswordScreenPage]
})
export class ForgotPasswordScreenPageModule {}
