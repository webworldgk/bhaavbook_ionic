import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ForgotPasswordScreenPage } from './forgot-password-screen.page';

const routes: Routes = [
  {
    path: '',
    component: ForgotPasswordScreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ForgotPasswordScreenPageRoutingModule {}
