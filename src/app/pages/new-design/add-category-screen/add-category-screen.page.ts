import { Component, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-add-category-screen',
  templateUrl: './add-category-screen.page.html',
  styleUrls: ['./add-category-screen.page.scss'],
})
export class AddCategoryScreenPage implements OnInit {
  constructor( public actionSheetController: ActionSheetController) { }

  ngOnInit() {
  }


  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Select Category',
      cssClass: 'custom-actionSheet',
      buttons: [{
        text: 'Steel',
        role: 'Steel',
        handler: () => {
          console.log('Steel clicked');
        }
      },
      {
        text: 'Alluminium',
        role: 'Alluminium',
        handler: () => {
          console.log('Alluminium clicked');
        }
      }
      ,
      {
        text: 'Iron',
        role: 'Iron',
        handler: () => {
          console.log('Iron clicked');
        }
      }
    ]
    });
    await actionSheet.present();
  }
}
