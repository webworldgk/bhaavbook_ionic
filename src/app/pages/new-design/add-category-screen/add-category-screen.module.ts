import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddCategoryScreenPageRoutingModule } from './add-category-screen-routing.module';

import { AddCategoryScreenPage } from './add-category-screen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddCategoryScreenPageRoutingModule
  ],
  declarations: [AddCategoryScreenPage]
})
export class AddCategoryScreenPageModule {}
