import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddCategoryScreenPage } from './add-category-screen.page';

const routes: Routes = [
  {
    path: '',
    component: AddCategoryScreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddCategoryScreenPageRoutingModule {}
