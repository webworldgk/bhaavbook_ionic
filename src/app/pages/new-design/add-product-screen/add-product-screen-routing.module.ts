import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddProductScreenPage } from './add-product-screen.page';

const routes: Routes = [
  {
    path: '',
    component: AddProductScreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddProductScreenPageRoutingModule {}
