import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddProductScreenPageRoutingModule } from './add-product-screen-routing.module';

import { AddProductScreenPage } from './add-product-screen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddProductScreenPageRoutingModule
  ],
  declarations: [AddProductScreenPage]
})
export class AddProductScreenPageModule {}
