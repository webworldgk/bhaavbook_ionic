import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductScreenPage } from './product-screen.page';

const routes: Routes = [
  {
    path: '',
    component: ProductScreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductScreenPageRoutingModule {}
