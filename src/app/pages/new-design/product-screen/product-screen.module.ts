import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductScreenPageRoutingModule } from './product-screen-routing.module';

import { ProductScreenPage } from './product-screen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductScreenPageRoutingModule
  ],
  declarations: [ProductScreenPage]
})
export class ProductScreenPageModule {}
