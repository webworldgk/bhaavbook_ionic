import { Component, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-product-screen',
  templateUrl: './product-screen.page.html',
  styleUrls: ['./product-screen.page.scss'],
})
export class ProductScreenPage implements OnInit {

  constructor( public actionSheetController: ActionSheetController) { }

  ngOnInit() {
  }


  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Filter',
      cssClass: 'custom-actionSheet',
      buttons: [{
        text: 'Publish',
        role: 'Publish',
        handler: () => {
          console.log('Publish clicked');
        }
      },
      {
        text: 'Un-publish',
        role: 'Un-publish',
        handler: () => {
          console.log('Un-publish clicked');
        }
      }
    ]
    });
    await actionSheet.present();
  }
}
