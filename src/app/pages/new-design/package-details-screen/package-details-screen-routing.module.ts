import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PackageDetailsScreenPage } from './package-details-screen.page';

const routes: Routes = [
  {
    path: '',
    component: PackageDetailsScreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PackageDetailsScreenPageRoutingModule {}
