import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PackageDetailsScreenPageRoutingModule } from './package-details-screen-routing.module';

import { PackageDetailsScreenPage } from './package-details-screen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PackageDetailsScreenPageRoutingModule
  ],
  declarations: [PackageDetailsScreenPage]
})
export class PackageDetailsScreenPageModule {}
