import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CategoryScreenPage } from './category-screen.page';

const routes: Routes = [
  {
    path: '',
    component: CategoryScreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CategoryScreenPageRoutingModule {}
