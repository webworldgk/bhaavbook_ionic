import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CategoryScreenPageRoutingModule } from './category-screen-routing.module';

import { CategoryScreenPage } from './category-screen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CategoryScreenPageRoutingModule
  ],
  declarations: [CategoryScreenPage]
})
export class CategoryScreenPageModule {}
