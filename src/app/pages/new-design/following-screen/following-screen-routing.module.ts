import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FollowingScreenPage } from './following-screen.page';

const routes: Routes = [
  {
    path: '',
    component: FollowingScreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FollowingScreenPageRoutingModule {}
