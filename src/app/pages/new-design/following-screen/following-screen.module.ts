import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FollowingScreenPageRoutingModule } from './following-screen-routing.module';

import { FollowingScreenPage } from './following-screen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FollowingScreenPageRoutingModule
  ],
  declarations: [FollowingScreenPage]
})
export class FollowingScreenPageModule {}
