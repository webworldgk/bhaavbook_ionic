import { AfterViewInit, Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-profile-screen',
  templateUrl: './profile-screen.page.html',
  styleUrls: ['./profile-screen.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProfileScreenPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
