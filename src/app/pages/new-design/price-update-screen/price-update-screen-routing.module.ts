import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PriceUpdateScreenPage } from './price-update-screen.page';

const routes: Routes = [
  {
    path: '',
    component: PriceUpdateScreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PriceUpdateScreenPageRoutingModule {}
