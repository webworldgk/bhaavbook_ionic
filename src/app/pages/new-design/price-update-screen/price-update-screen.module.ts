import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PriceUpdateScreenPageRoutingModule } from './price-update-screen-routing.module';

import { PriceUpdateScreenPage } from './price-update-screen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PriceUpdateScreenPageRoutingModule
  ],
  declarations: [PriceUpdateScreenPage]
})
export class PriceUpdateScreenPageModule {}
