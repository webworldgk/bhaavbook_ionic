import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FollowersScreenPage } from './followers-screen.page';

const routes: Routes = [
  {
    path: '',
    component: FollowersScreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FollowersScreenPageRoutingModule {}
