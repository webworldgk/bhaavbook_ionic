import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FollowersScreenPageRoutingModule } from './followers-screen-routing.module';

import { FollowersScreenPage } from './followers-screen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FollowersScreenPageRoutingModule
  ],
  declarations: [FollowersScreenPage]
})
export class FollowersScreenPageModule {}
