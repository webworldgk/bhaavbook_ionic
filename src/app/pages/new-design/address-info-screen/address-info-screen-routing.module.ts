import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddressInfoScreenPage } from './address-info-screen.page';

const routes: Routes = [
  {
    path: '',
    component: AddressInfoScreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddressInfoScreenPageRoutingModule {}
