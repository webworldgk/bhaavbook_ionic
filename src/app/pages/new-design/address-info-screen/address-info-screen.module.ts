import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddressInfoScreenPageRoutingModule } from './address-info-screen-routing.module';

import { AddressInfoScreenPage } from './address-info-screen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddressInfoScreenPageRoutingModule
  ],
  declarations: [AddressInfoScreenPage]
})
export class AddressInfoScreenPageModule {}
