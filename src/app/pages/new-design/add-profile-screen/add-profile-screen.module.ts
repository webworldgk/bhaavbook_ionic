import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddProfileScreenPageRoutingModule } from './add-profile-screen-routing.module';

import { AddProfileScreenPage } from './add-profile-screen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddProfileScreenPageRoutingModule
  ],
  declarations: [AddProfileScreenPage]
})
export class AddProfileScreenPageModule {}
