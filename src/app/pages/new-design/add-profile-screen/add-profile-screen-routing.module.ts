import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddProfileScreenPage } from './add-profile-screen.page';

const routes: Routes = [
  {
    path: '',
    component: AddProfileScreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddProfileScreenPageRoutingModule {}
