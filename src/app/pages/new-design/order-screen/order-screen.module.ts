import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderScreenPageRoutingModule } from './order-screen-routing.module';

import { OrderScreenPage } from './order-screen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderScreenPageRoutingModule
  ],
  declarations: [OrderScreenPage]
})
export class OrderScreenPageModule {}
