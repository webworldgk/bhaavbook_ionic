import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderScreenPage } from './order-screen.page';

const routes: Routes = [
  {
    path: '',
    component: OrderScreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderScreenPageRoutingModule {}
