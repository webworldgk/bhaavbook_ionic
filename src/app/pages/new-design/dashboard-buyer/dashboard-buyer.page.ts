import { AfterViewInit, Component, OnInit, ViewEncapsulation  } from '@angular/core';
import { NavController } from '@ionic/angular'; 
@Component({
  selector: 'app-dashboard-buyer',
  templateUrl: './dashboard-buyer.page.html',
  styleUrls: ['./dashboard-buyer.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardBuyerPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
