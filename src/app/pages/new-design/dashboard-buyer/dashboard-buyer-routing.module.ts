import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardBuyerPage } from './dashboard-buyer.page';

const routes: Routes = [
  {
    path: '',
    component: DashboardBuyerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardBuyerPageRoutingModule {}
