import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BusinessTypeScreenPageRoutingModule } from './business-type-screen-routing.module';

import { BusinessTypeScreenPage } from './business-type-screen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BusinessTypeScreenPageRoutingModule
  ],
  declarations: [BusinessTypeScreenPage]
})
export class BusinessTypeScreenPageModule {}
