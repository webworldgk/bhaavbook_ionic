import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusinessTypeScreenPage } from './business-type-screen.page';

const routes: Routes = [
  {
    path: '',
    component: BusinessTypeScreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusinessTypeScreenPageRoutingModule {}
