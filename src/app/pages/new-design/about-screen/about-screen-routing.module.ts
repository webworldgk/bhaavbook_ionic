import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AboutScreenPage } from './about-screen.page';

const routes: Routes = [
  {
    path: '',
    component: AboutScreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AboutScreenPageRoutingModule {}
