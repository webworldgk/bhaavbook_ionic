import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AboutScreenPageRoutingModule } from './about-screen-routing.module';

import { AboutScreenPage } from './about-screen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AboutScreenPageRoutingModule
  ],
  declarations: [AboutScreenPage]
})
export class AboutScreenPageModule {}
