import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderDetailsScreenPage } from './order-details-screen.page';

const routes: Routes = [
  {
    path: '',
    component: OrderDetailsScreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderDetailsScreenPageRoutingModule {}
