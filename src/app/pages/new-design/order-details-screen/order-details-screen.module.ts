import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderDetailsScreenPageRoutingModule } from './order-details-screen-routing.module';

import { OrderDetailsScreenPage } from './order-details-screen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderDetailsScreenPageRoutingModule
  ],
  declarations: [OrderDetailsScreenPage]
})
export class OrderDetailsScreenPageModule {}
