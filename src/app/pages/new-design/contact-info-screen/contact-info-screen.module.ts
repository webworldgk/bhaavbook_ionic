import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ContactInfoScreenPageRoutingModule } from './contact-info-screen-routing.module';

import { ContactInfoScreenPage } from './contact-info-screen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ContactInfoScreenPageRoutingModule
  ],
  declarations: [ContactInfoScreenPage]
})
export class ContactInfoScreenPageModule {}
