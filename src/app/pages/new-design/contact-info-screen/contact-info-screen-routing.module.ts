import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContactInfoScreenPage } from './contact-info-screen.page';

const routes: Routes = [
  {
    path: '',
    component: ContactInfoScreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContactInfoScreenPageRoutingModule {}
