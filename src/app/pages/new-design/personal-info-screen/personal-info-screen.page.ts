import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-personal-info-screen',
  templateUrl: './personal-info-screen.page.html',
  styleUrls: ['./personal-info-screen.page.scss'],
})
export class PersonalInfoScreenPage implements OnInit {

  password;

  show = false;

  ngOnInit() {
    this.password = 'password';
  }

  onClick() {
    if (this.password === 'password') {
      this.password = 'text';
      this.show = true;
    } else {
      this.password = 'password';
      this.show = false;
    }
  }

}
