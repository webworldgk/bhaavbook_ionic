import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PersonalInfoScreenPageRoutingModule } from './personal-info-screen-routing.module';

import { PersonalInfoScreenPage } from './personal-info-screen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PersonalInfoScreenPageRoutingModule
  ],
  declarations: [PersonalInfoScreenPage]
})
export class PersonalInfoScreenPageModule {}
