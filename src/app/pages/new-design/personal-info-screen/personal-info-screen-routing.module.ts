import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PersonalInfoScreenPage } from './personal-info-screen.page';

const routes: Routes = [
  {
    path: '',
    component: PersonalInfoScreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PersonalInfoScreenPageRoutingModule {}
