import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddLeadFollowUpPageRoutingModule } from './add-lead-follow-up-routing.module';

import { AddLeadFollowUpPage } from './add-lead-follow-up.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddLeadFollowUpPageRoutingModule
  ],
  declarations: [AddLeadFollowUpPage]
})
export class AddLeadFollowUpPageModule {}
