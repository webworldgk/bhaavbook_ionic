import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../providers/user-data';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-add-lead-follow-up',
  templateUrl: './add-lead-follow-up.page.html',
  styleUrls: ['./add-lead-follow-up.page.scss'],
})
export class AddLeadFollowUpPage implements OnInit {

  leadId: any;
  todaysDate: any;
  isValidated = false;

  customActionCommonOptions: any = {
    header: 'Select...'
  };

  followUpInfo = {
    outcomes: '',
    date: '',
    person: '',
    type: '',
    notes: '',
    next_date: '',
  }

  followUpTypes: any[] = [
    { id: 1, name: 'Telephonic' },
    { id: 2, name: 'Message' },
    { id: 3, name: 'Email' },
    { id: 4, name: 'Visit' },
    { id: 5, name: 'Others' },
  ];

  constructor(
    public router: Router,
    public api: ApiService,
    private http: HttpClient,
    public userData: UserData,
    public storage: Storage,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
  ) {
    this.leadId = this.route.snapshot.paramMap.get('leadId');
  }

  ngOnInit() { }

  ionViewDidEnter() {
    this.prepareMasters();
    this.prepareDate();
  }

  async prepareMasters() {
    if (this.api.subTypes.length == 0) {
      const storeVariables = true;
      this.api.doStoreMasters(storeVariables);
    }
  }

  prepareDate() {
    const dInputDate = new Date();
    const iInputMonth = (dInputDate.getMonth() + 1);
    const sInputMonth = iInputMonth.toLocaleString().length == 1 ? '0' + iInputMonth.toLocaleString() : iInputMonth.toLocaleString();
    const sInputDate = dInputDate.getDate().toLocaleString().length == 1 ? '0' + dInputDate.getDate().toLocaleString() : dInputDate.getDate().toLocaleString();
    this.todaysDate = dInputDate.getFullYear() + '-' + sInputMonth + '-' + sInputDate;
    console.log(`this.todaysDate`, this.todaysDate);
  }

  submitFollowUp(form: NgForm) {

    // console.log(`form`, form);
    this.isValidated = true;

    if (this.followUpInfo.outcomes == '') {
      this.api.presentValidateToast('Outcomes is required.', 4000, 'danger');
      this.isValidated = false;
    }

    if (this.followUpInfo.date == '') {
      this.api.presentValidateToast('Follow Up Date is required.', 4000, 'danger');
      this.isValidated = false;
    }

    if (this.followUpInfo.next_date == '') {
      this.api.presentValidateToast('Next Follow Up Date is required.', 4000, 'danger');
      this.isValidated = false;
    }

    if (this.followUpInfo.type == '') {
      this.api.presentValidateToast('Type is required.', 4000, 'danger');
      this.isValidated = false;
    }

    if (this.isValidated) {

      const postParams = {
        lead_id: this.leadId,
        outcome_id: this.followUpInfo.outcomes,
        followup_date: this.followUpInfo.date,
        associate_id: this.followUpInfo.person ? this.followUpInfo.person : null,
        type: this.followUpInfo.type ? this.followUpInfo.type : null,
        notes: this.followUpInfo.notes,
        next_followup_date: this.followUpInfo.next_date
      };

      console.log(`postParams`, postParams);
      this.addLeadFollowUp(postParams, form);
    }
  }

  async addLeadFollowUp(postParams: any, form: NgForm) {

    if (postParams) {

      await this.api.showLoader('Saving...');

      const token = await this.userData.getToken();
      // console.log('token', token);

      const apiName = `api/v1/leads_followup`;
      const apiUrl = this.api.restApiUrl + apiName;
      const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

      this.http.post(apiUrl, postParams, httpOptions).subscribe((response: any) => {
        console.log('addLeadFollowUp response', response);
        if (response.status == 1) {
          this.api.presentSuccessToast(response.message);
          this.api.dismissLoader();
          this.router.navigateByUrl(`/leads-detail/${this.leadId}/2`);
          form.reset();
        } else if (response.statusCode === 401) {
          this.api.clearStorageAndRedirectToLogin(response.message);
          this.api.dismissLoader();
        } else {
          this.api.presentValidateToast(response.message);
          this.api.dismissLoader();
        }
      }, (error: any) => {

        console.log('error', error.error);
        this.api.presentValidateToast(error.error.message);
        this.api.dismissLoader();
      });
    }
  }

  backToLeadDashbaord() {
    if (this.leadId) {
      this.router.navigate(['leads-detail', this.leadId, 2]); // 2. Follow UP
    } else {
      this.router.navigate(['leads-dashboard']);
    }
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}
