import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddLeadFollowUpPage } from './add-lead-follow-up.page';

const routes: Routes = [
  {
    path: '',
    component: AddLeadFollowUpPage
  },
  {
    path: ':leadId',
    component: AddLeadFollowUpPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddLeadFollowUpPageRoutingModule { }
