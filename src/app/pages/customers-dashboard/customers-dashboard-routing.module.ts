import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CustomersDashboardPage } from './customers-dashboard.page';

const routes: Routes = [
  {
    path: '',
    component: CustomersDashboardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomersDashboardPageRoutingModule {}
