import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { CustomersDashboardPageRoutingModule } from './customers-dashboard-routing.module';

import { CustomersDashboardPage } from './customers-dashboard.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CustomersDashboardPageRoutingModule
  ],
  declarations: [CustomersDashboardPage]
})
export class CustomersDashboardPageModule { }
