import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../providers/user-data';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-reset-password-otp',
  templateUrl: './reset-password-otp.page.html',
  styleUrls: ['./reset-password-otp.page.scss'],
})
export class ResetPasswordOtpPage implements OnInit {

  inputOtp: number;
  mobile_number: any;
  inputMobile: any;

  constructor(
    public router: Router,
    public api: ApiService,
    private http: HttpClient,
    public userData: UserData,
    public storage: Storage,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
  ) {
    this.mobile_number = this.route.snapshot.paramMap.get('mobile_number');
    console.log(`this.mobile_number`, this.mobile_number);
  }

  ngOnInit() { }

  ionViewDidEnter() { }

  async submitOTP(form: NgForm) {

    // console.log(`form`, form);

    await this.api.showLoader('Validating OTP...');

    const apiName = `api/v1/auth/forgot-password`;
    const apiUrl = this.api.restApiUrl + apiName;
    const postParams = { mobile_number: this.mobile_number, otp: this.inputOtp, step: 2};
    console.log(postParams);

    this.http.post(apiUrl, postParams).subscribe((response: any) => {
      console.log('submitMobile response', response);
      if (response.status === 1) {
        this.api.presentSuccessToast(response.message);
        this.router.navigateByUrl(`/new-password/${this.mobile_number}`);
        this.api.dismissLoader();
        form.reset();
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message, 5000, 'danger');
        this.api.dismissLoader();
      }
    }, (error: any) => {
      console.log('error', error.error);
      this.api.presentValidateToast(error.error.message, 5000, 'danger');
      this.api.dismissLoader();
    });

  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}
