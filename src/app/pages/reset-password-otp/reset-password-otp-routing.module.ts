import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResetPasswordOtpPage } from './reset-password-otp.page';

const routes: Routes = [
  {
    path: '',
    component: ResetPasswordOtpPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResetPasswordOtpPageRoutingModule {}
