import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResetPasswordOtpPageRoutingModule } from './reset-password-otp-routing.module';

import { ResetPasswordOtpPage } from './reset-password-otp.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ResetPasswordOtpPageRoutingModule
  ],
  declarations: [ResetPasswordOtpPage]
})
export class ResetPasswordOtpPageModule {}
