import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../providers/user-data';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.page.html',
  styleUrls: ['./new-password.page.scss'],
})
export class NewPasswordPage implements OnInit {

  isValidated: boolean;
  emailAddress: string;
  newPassword: any;
  confirmPassword: any;
  mobile_number: string;
  showPassword = false;
  passwordType = 'password';
  confirmType = 'password';
  showConfirmPassword= false;

  constructor(
    public router: Router,
    public api: ApiService,
    private http: HttpClient,
    public userData: UserData,
    public storage: Storage,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
  ) {
    this.mobile_number = this.route.snapshot.paramMap.get('mobile_number');
    console.log(`this.mobile_number`, this.mobile_number);
  }

  ngOnInit() { }

  ionViewDidEnter() { }

  async submitNewPassword(form: NgForm) {
    // console.log(`form`, form);

    this.isValidated = true;

    if (!this.newPassword || this.newPassword == '') {
      this.api.presentValidateToast('New Password is required.', 4000, 'danger');
      this.isValidated = false;
    }

    if (!this.confirmPassword || this.confirmPassword == '') {
      this.api.presentValidateToast('Confirm Password is required.', 4000, 'danger');
      this.isValidated = false;
    }

    if (this.newPassword !== this.confirmPassword) {
      this.api.presentValidateToast('Confirm Password does not matched!', 4000, 'danger');
      this.isValidated = false;
    }

    if (this.isValidated === true) {

      await this.api.showLoader();

      const apiName = `api/v1/auth/forgot-password`;
      const apiUrl = this.api.restApiUrl + apiName;
      const postParams = {
        mobile_number: this.mobile_number,
        password: this.newPassword,
        step: 3
      };

      this.http.post(apiUrl, postParams).subscribe((response: any) => {
        console.log('submitForgotPassword response', response);
        if (response.status === 1) {
          this.api.presentSuccessToast(response.message);
          this.router.navigateByUrl(`/app-login`);
          this.api.dismissLoader();
          form.reset();
        } else if (response.statusCode === 401) {
          this.api.clearStorageAndRedirectToLogin(response.message);
          this.api.dismissLoader();
        } else {
          this.api.presentValidateToast(response.message, 5000, 'danger');
          this.api.dismissLoader();
        }
      }, (error: any) => {
        console.log('error', error.error);
        this.api.presentValidateToast(error.error.message, 5000, 'danger');
        this.api.dismissLoader();
      });

    }
  }

  togglePassword() {
    this.showPassword = !this.showPassword;
    this.passwordType = this.passwordType == 'password' ? 'text' : 'password';
  }
  
  toggleConfirmPassword() {
    this.showConfirmPassword = !this.showConfirmPassword;
    this.confirmType = this.confirmType == 'password' ? 'text' : 'password';
  }
  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}
