import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LeadReminderListingsPage } from './lead-reminder-listings.page';

const routes: Routes = [
  {
    path: '',
    component: LeadReminderListingsPage
  },
  {
    path: ':reminderFor',
    component: LeadReminderListingsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LeadReminderListingsPageRoutingModule {}
