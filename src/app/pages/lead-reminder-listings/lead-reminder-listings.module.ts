import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LeadReminderListingsPageRoutingModule } from './lead-reminder-listings-routing.module';

import { LeadReminderListingsPage } from './lead-reminder-listings.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LeadReminderListingsPageRoutingModule
  ],
  declarations: [LeadReminderListingsPage]
})
export class LeadReminderListingsPageModule {}
