import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LeadsDashboardPageRoutingModule } from './leads-dashboard-routing.module';

import { LeadsDashboardPage } from './leads-dashboard.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LeadsDashboardPageRoutingModule
  ],
  declarations: [LeadsDashboardPage]
})
export class LeadsDashboardPageModule {}
