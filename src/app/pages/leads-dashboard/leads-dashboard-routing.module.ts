import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LeadsDashboardPage } from './leads-dashboard.page';

const routes: Routes = [
  {
    path: '',
    component: LeadsDashboardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LeadsDashboardPageRoutingModule {}
