import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { UserData } from '../../providers/user-data';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-leads-dashboard',
  templateUrl: './leads-dashboard.page.html',
  styleUrls: ['./leads-dashboard.page.scss'],
})
export class LeadsDashboardPage implements OnInit {

  dashboardData: any;

  constructor(
    public userData: UserData,
    public router: Router,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public api: ApiService
  ) { }

  ngOnInit() {
    this.getDashboardDetails();
  }

  ionViewDidEnter() {
    if (localStorage.getItem('refreshContents') === '1') {
      this.getDashboardDetails();
    }
  }

  async getDashboardDetails() {
    await this.api.showLoader();
    const token = await this.userData.getToken();
    this.api.getLeadDashboard(token).subscribe((response: any) => {
      console.log('getLeadDashboard response', response);
      if (response.status === 1) {
        this.dashboardData = response.data;

        // reset in order to not refresh again
        localStorage.setItem('refreshContents', '0');

        this.api.dismissLoader();
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });

  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}
