import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { UserData } from '../../providers/user-data';
import { UserOptions } from '../../interfaces/user-options';
import { ApiService } from 'src/app/service/api.service';
import { LoadingController, ToastController } from '@ionic/angular';
import { AppComponent } from 'src/app/app.component';
import { HttpClient } from '@angular/common/http';
import { AddressInfoScreenPage } from '../new-design/address-info-screen/address-info-screen.page';


@Component({
  selector: 'app-app-login',
  templateUrl: './app-login.page.html',
  styleUrls: ['./app-login.page.scss'],
})
export class AppLoginPage implements OnInit {

  login: UserOptions = {
    loginType: 1,
    mobile_number: '',
    password: '',
    mobilePin: '',
    deviceToken: '',
  };
  showPassword = false;
  passwordType = 'password';
  submitted = false;
  isValidated: boolean;
  data: any;
 

  constructor(
    private http: HttpClient,
    public userData: UserData,
    public router: Router,
    public toastCtrl: ToastController,
    public api: ApiService,
    public loadingCtrl: LoadingController,
    public appComponent: AppComponent,
  ) { }

  ngOnInit() { 
   
  }

  async ionViewWillEnter() {
    if (await this.userData.isLoggedIn()) {
      localStorage.setItem('refreshContents', '1');
      this.router.navigateByUrl('/dashboard');
    }
    if (await this.userData.isLoggedIn()) {
      localStorage.setItem('refreshContents', '1');
      this.router.navigateByUrl('/seller-dashboard');
    }
  }

  async onLogin(form: NgForm) {

    this.submitted = true;
    

    if (form.valid) {

      await this.api.showLoader('Sign In...');

      const apiName = `api/v1/auth/login`;
      const apiUrl = this.api.restApiUrl + apiName;

      console.log('perfomLogin login', this.login);
      this.http.post(apiUrl, this.login).subscribe((response: any) => {
        console.log('perfomLogin response', response);
        if (response.status === 1) {
          this.data = response.data;
          this.userData.login(this.login.mobile_number, response);
          this.userData.setMobilePermission(response.mobilePermission);
          this.api.userName = response.data?.first_name + ' ' + response.data?.last_name;
          this.api.userRole = response.data?.role;
          this.api.email = response.data?.email;
          this.api.mobile_number = response.data?.mobile_number;
          this.api.userPicture = response.data?.picture;
          this.api.userType = response.data?.user_type;
          this.api.userTypeName = response.data?.user_type_name;
          this.api.address = response.data?.address + ' ' +response.data?.area + ' ' + response.data?.pincode;


          // call masters api to store common data or list like states, cities, users etc...
          this.api.doStoreMasters();

          // stop loeader
          this.api.dismissLoader();

          // set it to reload dashboard 
          // localStorage.setItem('refreshContents', '1');
          // this.router.navigateByUrl('/dashboard');
        }
        if(this.data.user_type == 1){
          localStorage.setItem('refreshContents', '1');
          this.router.navigateByUrl('/dashboard');
        }else if(this.data.user_type == 2){
          localStorage.setItem('refreshContents', '1');
          this.router.navigateByUrl('/seller-dashboard');
        }
        //setting menus as per roles after login success
        this.api.userTypeapp = this.data.user_type;
        this.appComponent.appPages = (this.api.userTypeapp === 1) ? this.appComponent.appPages1 : this.appComponent.appPages2;

      }, (error: any) => {
        console.log('error', error.error);
        this.api.presentValidateToast(error.error.message, 5000, 'danger');
        this.api.dismissLoader();
      });

    }
  }

  togglePassword() {
    this.showPassword = !this.showPassword;
    this.passwordType = this.passwordType == 'password' ? 'text' : 'password';
  }
 
  async ionViewDidLeave() {
    if (this.api.loading) {
      this.api.dismissLoader();
    }
  }

}
