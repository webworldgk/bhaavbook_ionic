import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdateDailyReportPage } from './update-daily-report.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateDailyReportPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdateDailyReportPageRoutingModule {}
