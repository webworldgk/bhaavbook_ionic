import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../providers/user-data';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-update-daily-report',
  templateUrl: './update-daily-report.page.html',
  styleUrls: ['./update-daily-report.page.scss'],
})
export class UpdateDailyReportPage implements OnInit {

  dailyReportUpdate: any;
  dailyReportView: any;
  user: any;
  summary: any;
  attendance: any;

  public myForm: FormGroup;
  public myForm1: FormGroup;
  public myForm2: FormGroup;
  private textid = 1;

  private txt1 = 1;
  private txt2 = 1;
  private txt3 = 1;

  private tx1 = 1;
  private tx2 = 1;
  constructor(
    public userData: UserData,
    public router: Router,
    public toastCtrl: ToastController,
    public api: ApiService,
    private formBuilder: FormBuilder

  ) {
    this.myForm = formBuilder.group({
      textbox1: ['', Validators.required]
    });

    this.myForm1 = formBuilder.group({
      textbox1: ['', Validators.required]
    });

    this.myForm2 = formBuilder.group({
      textbox1: ['', Validators.required]
    });
  }

  ngOnInit() {
    //this.getDailyReportView();
    this.getProfileDetails();
    this.getDailyReportUpdate();
  }

  async getProfileDetails() {
    // const token = await this.userData.getToken();
    // console.log('token', token);
    // this.api.getProfileInfo(token).subscribe((response: any) => {
    //   console.log('getProfileInfo response', response);
    //   if (response && response.data) {
    //     this.user = response.data.user;
    //     this.summary = response.data.summary;
    //     this.attendance = response.data.attendance;
    //   } else {
    //     this.presentToast(response.message);
    //   }
    // });
  }

  // async getDailyReportView() {
  //   const token = await this.userData.getToken();

  //   this.api
  //     .getDailyReportsView(token)
  //     .subscribe((response: any) => {
  //       //console.log('getAttendances response', response);
  //       if (response && response.data) {
  //         this.dailyReportView = response.data;
  //         console.log(this.dailyReportView);

  //       } else {
  //         this.presentToast(response.message);
  //       }
  //     });
  // }


  async getDailyReportUpdate() {
    const token = await this.userData.getToken();

    this.api
      .getDailyReportsUpdate(token)
      .subscribe((response: any) => {
        // console.log('getUpdateInfo response', response);
        // console.log('getAttendances response', response);
        if (response && response.data) {
          this.dailyReportUpdate = response.data;
          this.attendance = response.data.attendance;
          this.user = response.data.userId;

          console.log(this.dailyReportUpdate);
          // console.log('data', this.dailyReportUpdate);

        } else {
          this.presentToast(response.message);
        }
      });
  }

  async presentToast(message, duration = 2000) {
    const toast = await this.toastCtrl.create({
      message,
      duration,
    });
    toast.present();
  }


  onBack() {
    this.router.navigate(['daily-report']);
  }

  addControl() {
    this.textid++;
    this.myForm.addControl('textbox' + this.textid, new FormControl('', Validators.required));
  }

  removeControl(control) {
    this.myForm.removeControl(control.key);
  }


  addPetrol() {
    this.txt1++;
    this.txt2++;
    this.txt3++;
    this.myForm1.addControl('textbox' + this.txt1, new FormControl('', Validators.required));
    this.myForm1.addControl('textbox' + this.txt2, new FormControl('', Validators.required));
    this.myForm1.addControl('textbox' + this.txt3, new FormControl('', Validators.required));
  }

  removeControl1(control1) {
    this.myForm1.removeControl(control1.key);
  }


  addExpanse() {
    this.tx1++;
    this.tx2++;
    this.myForm2.addControl('textbox' + this.tx1, new FormControl('', Validators.required));
    this.myForm2.addControl('textbox' + this.tx2, new FormControl('', Validators.required));
  }

  removeControl2(control2) {
    this.myForm2.removeControl(control2.key);
  }
}


