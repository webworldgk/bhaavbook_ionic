import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdateDailyReportPageRoutingModule } from './update-daily-report-routing.module';

import { UpdateDailyReportPage } from './update-daily-report.page';

import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UpdateDailyReportPageRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [UpdateDailyReportPage]
})
export class UpdateDailyReportPageModule {}
