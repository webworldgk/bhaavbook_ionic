import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../providers/user-data';
import { Storage } from '@ionic/storage';
import { IonicSelectableComponent } from 'ionic-selectable';

@Component({
  selector: 'app-add-rawcustomer',
  templateUrl: './add-rawcustomer.page.html',
  styleUrls: ['./add-rawcustomer.page.scss'],
})
export class AddRawcustomerPage implements OnInit {

  defaultName = 'Contact';
  isValidated = false;
  rawCustomerId: any;

  leadInfo = {
    code: 'CWCO-21',
    sub_type: '',
    notes: '',
  };

  customActionSubTypeOptions: any = {
    header: 'Choose Sub Type'
  };

  customActionCommonOptions: any = {
    header: 'Select...'
  };

  contactInfo = {
    name: '',
    firmName: '',
    mobile: '',
    email: '',
    second_mobile: '',
    second_email: '',
    address: '',
    state: '',
    city: '',
    pincode: '',
  };

  projectInfo = {
    name: '',
    type: '',
    address: '',
    state: '',
    city: '',
    pincode: '',
  };

  ownerInfo = {
    name: '',
    mobile: '',
    email: '',
    address: '',
    state: '',
    city: '',
    pincode: '',
  };

  assigneeInfo = {
    showroom: '',
    person: ''
  };

  purchaseManagerInfo = {
    name: '',
    mobile: '',
    email: '',
    address: '',
    state: '',
    city: '',
    pincode: '',
  };

  assistantInfo: any[] = [];

  additionalInfo = {
    contactState: {},
    contactCity: {},
    projectState: {},
    projectCity: {},
    purchaseManagerState: {},
    purchaseManagerCity: {},
    ownerState: {},
    ownerCity: {},
    showroomId: {},
    marketingId: {}
  };

  defaultSubType: any;

  constructor(
    public router: Router,
    public api: ApiService,
    private http: HttpClient,
    public userData: UserData,
    public storage: Storage,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
  ) {
    this.rawCustomerId = this.route.snapshot.paramMap.get('rawCustomerId');
  }

  ngOnInit() { }

  ionViewDidEnter() {
    this.prepareMasters();
    if (!this.rawCustomerId) {
      this.getCustomerCode();
    } else {
      this.getRawCustomerDetails();
    }
  }

  async prepareMasters() {
    if (this.api.subTypes.length == 0) {
      const storeVariables = true;
      this.api.doStoreMasters(storeVariables);
    }
  }

  async getCustomerCode() {
    const token = await this.userData.getToken();
    // console.log('token', token);
    this.api.getUniqueCustomerCode(token).subscribe((response: any) => {
      // console.log('getUniqueCustomerCode response', response);
      if (response.status == 1) {
        this.leadInfo.code = response.data;
        // console.log(`this.leadInfo.code`, this.leadInfo.code);
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
      } else {
        this.api.presentValidateToast(response.message);
      }
    });
  }

  async getRawCustomerDetails() {

    await this.api.showLoader('Please wait...');

    const token = await this.userData.getToken();
    console.log('token', token);

    this.api.getRawCustomerDetails(token, this.rawCustomerId).subscribe((response: any) => {
      console.log('getCustomerDetails1 response', response);
      if (response.status === 1) {
        this.setDataFormVariables(response.data);
        this.api.dismissLoader();
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }

  submitRawCustomer(form: NgForm) {
    // console.log(`form`, form);
    this.isValidated = true;

    if (this.leadInfo.code == '') {
      this.api.presentValidateToast('Code is required.', 4000, 'danger');
      this.isValidated = false;
    }

    if (this.leadInfo.sub_type == '') {
      this.api.presentValidateToast('Sub Type is required.', 4000, 'danger');
      this.isValidated = false;
    }

    if (this.isValidated) {
      // call api to save raw customer
      const postParams = {
        customer_code: this.leadInfo.code,
        customer_sub_type: this.leadInfo.sub_type,
        customer_name: this.contactInfo.name,
        firm_name: this.contactInfo.firmName,
        customer_mobile: this.contactInfo.mobile,
        customer_phone: this.contactInfo.second_mobile,
        customer_email: this.contactInfo.email,
        secondary_email: this.contactInfo.second_email,
        customer_address: this.contactInfo.address,
        state_id: this.contactInfo.state ? this.contactInfo.state : null,
        city_id: this.contactInfo.city ? this.contactInfo.city : null,
        customer_pincode: this.contactInfo.pincode,
        project_name: this.projectInfo.name,
        project_type: this.projectInfo.type ? this.projectInfo.type : null,
        project_address: this.projectInfo.address,
        project_state_id: this.projectInfo.state ? this.projectInfo.state : null,
        project_city_id: this.projectInfo.city ? this.projectInfo.city : null,
        project_pincode: this.projectInfo.pincode,
        owner_name: this.ownerInfo.name,
        owner_mobile: this.ownerInfo.mobile,
        owner_email: this.ownerInfo.email,
        owner_address: this.ownerInfo.address,
        owner_state_id: this.ownerInfo.state ? this.ownerInfo.state : null,
        owner_city_id: this.ownerInfo.city ? this.ownerInfo.city : null,
        owner_pincode: this.ownerInfo.pincode,
        purchase_manager_name: this.purchaseManagerInfo.name,
        purchase_manager_mobile: this.purchaseManagerInfo.mobile,
        purchase_manager_email: this.purchaseManagerInfo.email,
        purchase_manager_address: this.purchaseManagerInfo.address,
        purchase_manager_state_id: this.purchaseManagerInfo.state ? this.purchaseManagerInfo.state : null,
        purchase_manager_city_id: this.purchaseManagerInfo.city ? this.purchaseManagerInfo.city : null,
        assignee_showroom: this.assigneeInfo.showroom ? this.assigneeInfo.showroom : null,
        assignee_marketing: this.assigneeInfo.person ? this.assigneeInfo.person : null,
        notes: this.leadInfo.notes,
        customer_type: 1,
        country_id: 101,
        assi_name: this.prepareAssistantName(),
        assi_mobile: this.prepareAssistantMobile(),
        assi_email: this.prepareAssistantEmail(),
      };

      console.log(`postParams`, postParams);
      if (this.rawCustomerId) {
        this.updateRowCustomer(postParams, form);
      } else {
        this.addRowCustomer(postParams, form);
      }

    }
  }

  async addRowCustomer(postParams: any, form: NgForm) {

    if (postParams) {

      await this.api.showLoader('Saving...');

      const token = await this.userData.getToken();
      // console.log('token', token);

      const apiName = `api/v1/raw_customers`;
      const apiUrl = this.api.restApiUrl + apiName;
      const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

      this.http.post(apiUrl, postParams, httpOptions).subscribe((response: any) => {
        console.log('addRowCustomer response', response);
        if (response.status == 1) {
          this.api.presentSuccessToast(response.message);
          this.api.dismissLoader();

          // set it to reload dashboard
          localStorage.setItem('refreshContents', '1');
          if (response.data.id) {
            this.router.navigateByUrl(`/customers-detail/${response.data.id}`);
          } else {
            this.router.navigateByUrl(`/customers-dashboard`);
          }
          form.reset();

        } else if (response.statusCode === 401) {
          this.api.clearStorageAndRedirectToLogin(response.message);
          this.api.dismissLoader();

        } else {
          this.api.presentValidateToast(response.message);
          this.api.dismissLoader();
        }
      }, (error: any) => {

        console.log('error', error.error);
        this.api.presentValidateToast(error.error.message);
        this.api.dismissLoader();
      });
    }
  }

  async updateRowCustomer(postParams: any, form: NgForm) {

    if (postParams) {

      await this.api.showLoader('Updating...');

      const token = await this.userData.getToken();
      // console.log('token', token);

      const apiName = `api/v1/raw_customers/${this.rawCustomerId}`;
      const apiUrl = this.api.restApiUrl + apiName;
      const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

      this.http.put(apiUrl, postParams, httpOptions).subscribe((response: any) => {
        console.log('updateRowCustomer response', response);
        if (response.status == 1) {
          this.api.presentSuccessToast(response.message);
          this.api.dismissLoader();

          // set it to reload dashboard
          localStorage.setItem('refreshContents', '1');
          this.router.navigateByUrl(`/customers-detail/${this.rawCustomerId}`);
          form.reset();

        } else if (response.statusCode === 401) {
          this.api.clearStorageAndRedirectToLogin(response.message);
          this.api.dismissLoader();

        } else {
          this.api.presentValidateToast(response.message);
          this.api.dismissLoader();
        }
      }, (error: any) => {

        console.log('error', error.error);
        this.api.presentValidateToast(error.error.message);
        this.api.dismissLoader();
      });
    }
  }

  prepareAssistantName() {
    let isAvailable = true;
    const preparedList = this.assistantInfo.map((item) => {
      if (item.name == '') {
        isAvailable = false;
      }
      if (item.name !== '') {
        return item.name;
      }
    });

    if (!isAvailable) {
      return null;
    }

    if (isAvailable) {
      return preparedList;
    }
  }

  prepareAssistantMobile() {
    let isAvailable = true;
    const preparedList = this.assistantInfo.map((item) => {
      if (item.mobile == '') {
        isAvailable = false;
      }
      if (item.mobile !== '') {
        return item.mobile;
      }
    });

    if (!isAvailable) {
      return null;
    }

    if (isAvailable) {
      return preparedList;
    }
  }

  prepareAssistantEmail() {
    let isAvailable = true;
    const preparedList = this.assistantInfo.map((item) => {
      if (item.email == '') {
        isAvailable = false;
      }
      if (item.email !== '') {
        return item.email;
      }
    });

    if (!isAvailable) {
      return null;
    }

    if (isAvailable) {
      return preparedList;
    }
  }

  changeSubType(event: any) {
    const selectedSubType = event.detail.value;
    console.log(`event.detail.value`, event.detail.value);
    this.api.subTypes.find((item) => {
      if (selectedSubType > 1 && item.id == selectedSubType) {
        this.defaultName = item.name;
      }
    });
    console.log(`defaultSubType`, this.defaultSubType);
    if (selectedSubType > 0 && this.defaultSubType !== selectedSubType) {
      this.resetCustomerInfo();
      this.resetAdditionalInfo();
    }
  }

  backToRawCustomerDashbaord() {
    this.router.navigate(['customers-dashboard']);
  }

  addAssistantControl() {
    this.assistantInfo.push({
      name: '',
      mobile: '',
      email: '',
    });
  }

  removeAssistantControl(i: number) {
    this.assistantInfo.splice(i, 1);
  }

  setDataFormVariables(responseData: any) {
    this.leadInfo.code = responseData?.customer_code;
    this.leadInfo.sub_type = responseData?.customer_sub_type_id ? responseData?.customer_sub_type_id.toString() : '';
    this.leadInfo.notes = responseData?.notes;
    this.defaultSubType = this.leadInfo.sub_type;

    // contacts
    this.contactInfo.name = responseData?.customer_name;
    this.contactInfo.firmName = responseData?.firm_name;
    this.contactInfo.mobile = responseData?.customer_mobile;
    this.contactInfo.email = responseData?.customer_email;
    this.contactInfo.second_mobile = responseData?.customer_phone;
    this.contactInfo.second_email = responseData?.secondary_email;
    this.contactInfo.address = responseData?.customer_address;
    this.contactInfo.state = responseData?.customer_state ? responseData?.customer_state.toString() : '';
    this.contactInfo.city = responseData?.customer_city ? responseData?.customer_city.toString() : '';
    this.contactInfo.pincode = responseData?.customer_pincode;

    // project
    this.projectInfo.name = responseData?.project_name;
    this.projectInfo.type = responseData?.project_type_id ? responseData?.project_type_id.toString() : '';
    this.projectInfo.address = responseData?.project_address;
    this.projectInfo.state = responseData?.project_state_id ? responseData?.project_state_id.toString() : '';
    this.projectInfo.city = responseData?.project_city_id ? responseData?.project_city_id.toString() : '';
    this.projectInfo.pincode = responseData?.project_pincode;

    // owner
    this.ownerInfo.name = responseData?.owner_name;
    this.ownerInfo.mobile = responseData?.owner_mobile;
    this.ownerInfo.email = responseData?.owner_email;
    this.ownerInfo.address = responseData?.owner_address;
    this.ownerInfo.state = responseData?.owner_state_id ? responseData?.owner_state_id.toString() : '';
    this.ownerInfo.city = responseData?.owner_city_id ? responseData?.owner_city_id.toString() : '';
    this.ownerInfo.pincode = responseData?.owner_pincode;

    // purchase manager
    this.purchaseManagerInfo.name = responseData?.purchase_manager_name;
    this.purchaseManagerInfo.mobile = responseData?.purchase_manager_mobile;
    this.purchaseManagerInfo.email = responseData?.purchase_manager_email;
    this.purchaseManagerInfo.address = responseData?.purchase_manager_address;
    this.purchaseManagerInfo.state = responseData?.purchase_manager_state_id ? responseData?.purchase_manager_state_id.toString() : '';
    this.purchaseManagerInfo.city = responseData?.purchase_manager_city_id ? responseData?.purchase_manager_city_id.toString() : '';
    this.purchaseManagerInfo.pincode = responseData?.purchase_manager_pincode;

    // assignee
    this.assigneeInfo.showroom = responseData?.show_room_id ? responseData?.show_room_id.toString() : '';
    this.assigneeInfo.person = responseData?.marketing_id ? responseData?.marketing_id.toString() : '';

    // assitant
    if (responseData.assitant.length && responseData.assitant.length > 0) {
      responseData.assitant.forEach((item) => {
        this.assistantInfo.push({
          name: item.assi_name,
          mobile: item.assi_mobile,
          email: item.assi_email,
        });
      });
    }

    // Additional Info for search
    if (this.contactInfo.state) {
      this.additionalInfo.contactState = { id: this.contactInfo.state, name: responseData?.customer_state_name };
    }
    if (this.contactInfo.city) {
      this.additionalInfo.contactCity = { id: this.contactInfo.city, name: responseData?.customer_city_name };
    }
    if (this.projectInfo.state) {
      this.additionalInfo.projectState = { id: this.projectInfo.state, name: responseData?.project_state_name };
    }
    if (this.projectInfo.city) {
      this.additionalInfo.projectCity = { id: this.projectInfo.city, name: responseData?.project_city_name };
    }
    if (this.ownerInfo.city) {
      this.additionalInfo.ownerCity = { id: this.ownerInfo.city, name: responseData?.owner_city_name };
    }
    if (this.ownerInfo.state) {
      this.additionalInfo.ownerState = { id: this.ownerInfo.state, name: responseData?.owner_state_name };
    }
    if (this.purchaseManagerInfo.city) {
      this.additionalInfo.purchaseManagerCity = { id: this.purchaseManagerInfo.city, name: responseData?.purchase_manager_city_name };
    }
    if (this.purchaseManagerInfo.state) {
      this.additionalInfo.purchaseManagerState = { id: this.purchaseManagerInfo.state, name: responseData?.purchase_manager_state_name };
    }
    if (this.assigneeInfo.showroom) {
      this.additionalInfo.showroomId = { id: this.assigneeInfo.showroom, name: responseData?.show_room };
    }
    if (this.assigneeInfo.person) {
      this.additionalInfo.marketingId = { id: this.assigneeInfo.person, name: responseData?.marketing };
    }
  }

  searchChange(event: { component: IonicSelectableComponent, value: any }, configName: string, configId: number) {
    if (configId === 1) {
      if (configName === 'state') {
        this.contactInfo.state = event.value.id;
      }
      if (configName === 'city') {
        this.contactInfo.city = event.value.id;
      }
    }

    // Project Details
    if (configId === 2) {
      if (configName === 'state') {
        this.projectInfo.state = event.value.id;
      }
      if (configName === 'city') {
        this.projectInfo.city = event.value.id;
      }
    }

    // Owner Details
    if (configId === 3) {
      if (configName === 'state') {
        this.ownerInfo.state = event.value.id;
      }
      if (configName === 'city') {
        this.ownerInfo.city = event.value.id;
      }
    }

    // Purchase Details
    if (configId === 4) {
      if (configName === 'state') {
        this.purchaseManagerInfo.state = event.value.id;
      }
      if (configName === 'city') {
        this.purchaseManagerInfo.city = event.value.id;
      }
    }

    // Showroom and Marketing Assignee
    if (configId === 5) {
      if (configName === 'showroom') {
        this.assigneeInfo.showroom = event.value.id;
      }
      if (configName === 'marketing') {
        this.assigneeInfo.person = event.value.id;
      }
    }

    console.log('searchChange:', event.value.id);
  }

  // Reseting the Additional Info Details
  resetAdditionalInfo() {
    this.additionalInfo = {
      contactState: '',
      contactCity: '',
      projectState: '',
      projectCity: '',
      purchaseManagerState: '',
      purchaseManagerCity: '',
      ownerState: '',
      ownerCity: '',
      showroomId: '',
      marketingId: ''
    };
  }

  // Reseting Customer Info
  resetCustomerInfo() {
    this.contactInfo = {
      name: '',
      firmName: '',
      mobile: '',
      email: '',
      second_mobile: '',
      second_email: '',
      address: '',
      state: '',
      city: '',
      pincode: '',
    };

    this.projectInfo = {
      name: '',
      type: '',
      address: '',
      state: '',
      city: '',
      pincode: '',
    };

    this.ownerInfo = {
      name: '',
      mobile: '',
      email: '',
      address: '',
      state: '',
      city: '',
      pincode: '',
    };

    this.assigneeInfo = {
      showroom: '',
      person: ''
    };

    this.purchaseManagerInfo = {
      name: '',
      mobile: '',
      email: '',
      address: '',
      state: '',
      city: '',
      pincode: '',
    };

    this.assistantInfo = [];
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}
