import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddRawcustomerPage } from './add-rawcustomer.page';

const routes: Routes = [
  {
    path: '',
    component: AddRawcustomerPage
  },
  {
    path: ':rawCustomerId',
    component: AddRawcustomerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddRawcustomerPageRoutingModule {}
