import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddRawcustomerPageRoutingModule } from './add-rawcustomer-routing.module';

import { AddRawcustomerPage } from './add-rawcustomer.page';
import { IonicSelectableModule } from 'ionic-selectable';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddRawcustomerPageRoutingModule,
    IonicSelectableModule,
  ],
  declarations: [AddRawcustomerPage]
})
export class AddRawcustomerPageModule {}
