import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LeadsDetailPageRoutingModule } from './leads-detail-routing.module';

import { LeadsDetailPage } from './leads-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LeadsDetailPageRoutingModule
  ],
  declarations: [LeadsDetailPage]
})
export class LeadsDetailPageModule {}
