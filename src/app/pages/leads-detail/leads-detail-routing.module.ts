import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LeadsDetailPage } from './leads-detail.page';

const routes: Routes = [
  {
    path: '',
    component: LeadsDetailPage
  },
  {
    path: ':display',
    component: LeadsDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LeadsDetailPageRoutingModule {}
