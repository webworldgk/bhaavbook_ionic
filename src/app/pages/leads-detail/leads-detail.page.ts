import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../providers/user-data';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Component({
  selector: 'app-leads-detail',
  templateUrl: './leads-detail.page.html',
  styleUrls: ['./leads-detail.page.scss'],
})
export class LeadsDetailPage implements OnInit {

  leadInfo: any;
  leadFollowUp: any;
  leadReminder: any;
  leadAssociate: any;
  leadGallery: any;
  leadNotes: any;
  leadId: any;

  leadInfoCount: number;
  leadFollowUpCount: number;
  leadReminderCount: number;
  leadAssociateCount: number;
  leadGalleryCount: number;
  leadNotesCount: number;

  dStartDate = '';
  dEndDate = '';
  displayView = 1;

  constructor(
    public router: Router,
    public userData: UserData,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public api: ApiService,
    private photoViewer: PhotoViewer,
  ) {

    this.leadId = this.route.snapshot.paramMap.get('id');
    const displayView = parseInt(this.route.snapshot.paramMap.get('display'));
    if (displayView) {
      this.displayView = displayView;
    }
  }

  ngOnInit() { }

  ionViewWillEnter() {
    this.getLeadDetails();
  }

  ionViewDidEnter() {
    this.getTabDetails();
  }

  getTabDetails() {
    if (this.displayView === 1) {
      this.getLeadDetails();
    }

    if (this.displayView === 2) {
      this.getFollowUpListing();
    }

    if (this.displayView === 3) {
      this.getReminderListing();
    }

    if (this.displayView === 4) {
      this.getAssociatesListing();
    }

    if (this.displayView === 5) {
      this.getGalleryListing();
    }

    if (this.displayView === 6) {
      this.getNotesListing();
    }
  }

  async getNotesListing() {

    await this.api.showLoader();

    const token = await this.userData.getToken();
    // console.log('token', token);

    this.api.getLeadNotes(token, this.leadId, this.dStartDate, this.dEndDate).subscribe((response: any) => {
      console.log('getnotes response', response.data);
      if (response.status === 1) {
        this.leadNotes = response.data;
        this.leadNotesCount = response.data.length;
        this.api.dismissLoader();
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }

  async getGalleryListing() {

    await this.api.showLoader();

    const token = await this.userData.getToken();
    // console.log('token', token);

    this.api.getLeadGallery(token, this.leadId, this.dStartDate, this.dEndDate).subscribe((response: any) => {
      console.log('getgallery response', response.data);
      if (response.status === 1) {
        this.leadGallery = response.data;
        this.leadGalleryCount = response.data.length;
        this.api.dismissLoader();
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }

  async getAssociatesListing() {

    await this.api.showLoader();

    const token = await this.userData.getToken();
    // console.log('token', token);

    this.api.getLeadAssociates(token, this.leadId, this.dStartDate, this.dEndDate).subscribe((response: any) => {
      console.log('getAssociates response', response.data);
      if (response.status === 1) {
        this.leadAssociate = response.data;
        this.leadAssociateCount = response.data.length;
        this.api.dismissLoader();
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }

  async getFollowUpListing() {

    await this.api.showLoader();

    const token = await this.userData.getToken();
    // console.log('token', token);

    this.api.getLeadFollowUp(token, this.leadId, this.dStartDate, this.dEndDate).subscribe((response: any) => {
      console.log('getleadFollowUp response', response.data);
      if (response && response.data) {
        this.leadFollowUp = response.data;
        this.leadFollowUpCount = response.data.length;
        this.api.dismissLoader();
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }

  async getReminderListing() {

    await this.api.showLoader();

    const token = await this.userData.getToken();
    // // console.log('token', token);

    this.api.getLeadReminder(token, this.leadId, this.dStartDate, this.dEndDate).subscribe((response: any) => {
      console.log('getLeadReminder response', response.data);
      if (response && response.data) {
        this.leadReminder = response.data;
        this.leadReminderCount = response.data.length;
        this.api.dismissLoader();
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }

  async getLeadDetails() {
    const token = await this.userData.getToken();
    this.api.getLeadDetails(token, this.leadId).subscribe((response: any) => {
      console.log('getLeadDetails response', response.data);
      if (response.status === 1) {
        this.leadInfo = response.data;
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
      } else {
        this.api.presentValidateToast(response.message);
      }
    });

  }

  segmentChanged(event) {
    console.log(event.detail.value);
    this.displayView = parseInt(event.detail.value);
    this.getTabDetails();
  }

  openPhoto(fileUrl: string, fileName: string) {
    if (fileUrl && fileName) {
      this.photoViewer.show(fileUrl, fileName, { share: false });
    }
  }

  addGallery() {
    console.log(`test`);
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}
