import { Component, OnInit } from '@angular/core';
import { FormGroup, NgForm, Validators } from '@angular/forms';
import { FormBuilder, Validator } from '@angular/forms';
import { ActivatedRoute, Router ,} from '@angular/router';

@Component({
  selector: 'app-otp-page',
  templateUrl: './otp-page.page.html',
  styleUrls: ['./otp-page.page.scss'],
})
export class OtpPagePage implements OnInit {

  ionicForm: FormGroup;
  isSubmitted = false;
  constructor(public formBuilder: FormBuilder,
    public router: Router,
    ) { }
  ngOnInit() {
    this.ionicForm = this.formBuilder.group({
      otp: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
    })
  }
  get errorControl() {
    return this.ionicForm.controls;
  }
  submitForm() {
    this.isSubmitted = true;
    if (!this.ionicForm.valid) {
      console.log('Please provide all the required values!')
      return false;
    } else {
      console.log(this.ionicForm.value)
      this.router.navigateByUrl('/user-selection-page');
    }
  }


}
