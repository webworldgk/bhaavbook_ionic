import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OtpPagePageRoutingModule } from './otp-page-routing.module';

import { OtpPagePage } from './otp-page.page';
import { ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    OtpPagePageRoutingModule
  ],
  declarations: [OtpPagePage]
})
export class OtpPagePageModule {}
