import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, NgModule, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../providers/user-data';

@Component({
  selector: 'app-add-daily-report',
  templateUrl: './add-daily-report.page.html',
  styleUrls: ['./add-daily-report.page.scss'],
})
export class AddDailyReportPage implements OnInit {

  reportId: any;
  pageTitle = 'Add';
  attendaneId: any;
  dailyReport: any;
  reportInputs: any;
  employeeCode: any;
  employeeName: any;
  reportDate: any;
  expenseAmount: number = 0;

  workDetails: any[] = [{
    name: ''
  }];

  petrolDetails: any[] = [];
  dailyExpenses: any[] = [];

  commonDetails = {
    description: '',
    amount: ''
  };

  constructor(
    private http: HttpClient,
    public router: Router,
    public api: ApiService,
    public userData: UserData,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
  ) {
    this.reportId = this.route.snapshot.paramMap.get('reportId');
    console.log(`this.reportId`, this.reportId);
  }

  ngOnInit() { }

  ionViewDidEnter() {
    this.prepareSetup();
    if (this.reportId) {
      this.pageTitle = 'Update';
    }
  }

  async prepareSetup() {

    this.employeeCode = await this.userData.getUserInfoByKey('emp_code');
    this.employeeName = await this.userData.getUserFullName();

    if (this.reportId) {
      this.getDailyReport();
    }

    if (!this.reportId) {
      // this.attendaneId = 1;
      const todaysDate = new Date();
      const dateOptions = { day: '2-digit', month: 'short', year: 'numeric' } as const;
      this.reportDate = todaysDate.toLocaleDateString('en-IN', dateOptions);
      console.log(`this.reportDate`, this.reportDate);
    }

  }

  // Get daily Reports Data using API
  async getDailyReport() {
    const token = await this.userData.getToken();
    this.api.getDailyReport(token, this.reportId).subscribe((response: any) => {
      console.log('getDailyReport response', response);
      if (response.status == 1) {
        this.dailyReport = response.data;
        this.reportDate = this.dailyReport.report_date;
        this.attendaneId = this.dailyReport.attendanceID;
        this.workDetails = this.dailyReport.tasks.map((item) => { return { name: item.task_name } });
        this.petrolDetails = this.dailyReport.travels.map((item) => { return { from: item.travel_from, to: item.travel_to, km: item.travel_km } });
        this.dailyExpenses = this.dailyReport.expenses.map((item) => { return { description: item.expense_name, amount: item.expense_amount } });
        this.commonDetails.description = this.dailyReport.inward_description;
        this.commonDetails.amount = this.dailyReport.inward_amount;

        // update total ammount
        this.expenseAmount = this.dailyReport.expenses.reduce(((acc, item) => acc + item.expense_amount), 0);

      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);

      } else {
        this.api.presentValidateToast(response.message);
      }
    });
  }

  addExpenseAmount() {
    this.expenseAmount = this.dailyExpenses.reduce(((acc, item) => acc + item.amount), 0);
    // console.log(`this.expenseAmount`, this.expenseAmount);
  }

  addWorkDetails() {
    this.workDetails.push({
      name: ''
    });
  }

  removeWorkDetail(i: number) {
    this.workDetails.splice(i, 1);
  }

  addPetrolDetails() {
    this.petrolDetails.push({
      from: '',
      to: '',
      km: '',
    });
  }

  removePetrolDetail(i: number) {
    this.petrolDetails.splice(i, 1);
  }

  addDailyExpense() {
    this.dailyExpenses.push({
      description: '',
      amount: ''
    });
  }

  removeDailyExpense(i: number) {
    this.dailyExpenses.splice(i, 1);
  }

  submitDailyReport(form: NgForm) {

    // console.log('this.workDetails', this.workDetails);
    // console.log('this.petrolDetails', this.petrolDetails);
    // console.log('this.dailyExpenses', this.dailyExpenses);
    // console.log('this.commonDetails', this.commonDetails);

    const dInputReportDate = new Date(this.reportDate);
    const iInputMonth = (dInputReportDate.getMonth() + 1);
    const sInputMonth = iInputMonth.toLocaleString().length == 1 ? '0' + iInputMonth.toLocaleString() : iInputMonth.toLocaleString();
    const sInputDate = dInputReportDate.getDate().toLocaleString().length == 1 ? '0' + dInputReportDate.getDate().toLocaleString() : dInputReportDate.getDate().toLocaleString();
    // console.log([`sInputMonth`, sInputMonth, `sInputDate`, sInputDate]);
    const dReportDate = dInputReportDate.getFullYear() + '-' + sInputMonth + '-' + sInputDate;

    const postParams = {
      attendance_id: this.attendaneId,
      report_date: dReportDate,
      inward_description: this.commonDetails.description,
      inward_amount: this.commonDetails.amount,
      task_name: this.prepareTaskName(),
      expense_name: this.preparedailyExpenseName(),
      expense_amount: this.preparedailyExpenseAmount(),
      travel_from: this.prepareTravelFrom(),
      travel_to: this.prepareTravelTo(),
      travel_km: this.prepareTravelKM(),
    }

    console.log(`postParams`, postParams);
    this.saveDailyReport(postParams, form);
  }

  prepareTaskName() {
    let isAvailable = true;
    const preparedList = this.workDetails.map((item) => {
      if (item.name == '') {
        isAvailable = false;
      }
      if (item.name !== '') {
        return item.name;
      }
    });

    if (!isAvailable) {
      return null;
    }

    if (isAvailable) {
      return preparedList;
    }
  }

  preparedailyExpenseName() {
    let isAvailable = true;
    const preparedList = this.dailyExpenses.map((item) => {
      if (item.description == '') {
        isAvailable = false;
      }
      if (item.description !== '') {
        return item.description;
      }
    });

    if (!isAvailable) {
      return null;
    }

    if (isAvailable) {
      return preparedList;
    }
  }

  preparedailyExpenseAmount() {
    let isAvailable = true;
    const preparedList = this.dailyExpenses.map((item) => {
      if (item.amount == '') {
        isAvailable = false;
      }
      if (item.amount !== '') {
        return item.amount;
      }
    });

    if (!isAvailable) {
      return null;
    }

    if (isAvailable) {
      return preparedList;
    }
  }

  prepareTravelFrom() {
    let isAvailable = true;
    const preparedList = this.petrolDetails.map((item) => {
      if (item.from == '') {
        isAvailable = false;
      }
      if (item.from !== '') {
        return item.from;
      }
    });

    if (!isAvailable) {
      return null;
    }

    if (isAvailable) {
      return preparedList;
    }
  }

  prepareTravelTo() {
    let isAvailable = true;
    const preparedList = this.petrolDetails.map((item) => {
      if (item.to == '') {
        isAvailable = false;
      }
      if (item.to !== '') {
        return item.to;
      }
    });

    if (!isAvailable) {
      return null;
    }

    if (isAvailable) {
      return preparedList;
    }
  }

  prepareTravelKM() {
    let isAvailable = true;
    const preparedList = this.petrolDetails.map((item) => {
      if (item.km == '') {
        isAvailable = false;
      }
      if (item.km !== '') {
        return item.km;
      }
    });

    if (!isAvailable) {
      return null;
    }

    if (isAvailable) {
      return preparedList;
    }
  }

  async saveDailyReport(postParams: any, form: NgForm) {
    console.log(`form`, form);
    if (this.reportId) {
      this.updateDailyReport(postParams, form);
    } else {
      this.addDailyReport(postParams, form);
    }
  }

  async addDailyReport(postParams: any, form: NgForm) {

    await this.api.showLoader('Saving...');

    const token = await this.userData.getToken();
    console.log('token', token);

    const apiName = `api/v1/daily_reports`;
    const apiUrl = this.api.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

    this.http.post(apiUrl, postParams, httpOptions).subscribe((response: any) => {
      console.log('addDailyReport response', response);
      if (response.status == 1) {
        this.api.presentSuccessToast(response.message);
        this.api.dismissLoader();

        // set it to reload dashboard 
        localStorage.setItem('refreshContents', '1');
        form.reset();
        this.router.navigateByUrl(`/daily-report/${this.attendaneId}`);

      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();

      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    }, (error: any) => {

      console.log('error', error.error);
      this.api.presentValidateToast(error.error.message);
      this.api.dismissLoader();
    });
  }

  async updateDailyReport(postParams: any, form: NgForm) {

    await this.api.showLoader('Updating...');

    const token = await this.userData.getToken();
    console.log('token', token);

    const apiName = `api/v1/daily_reports/${this.reportId}`;
    const apiUrl = this.api.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

    this.http.put(apiUrl, postParams, httpOptions).subscribe((response: any) => {
      console.log('updateDailyReport response', response);
      if (response.status == 1) {
        this.api.presentSuccessToast(response.message);
        this.api.dismissLoader();

        // set it to reload dashboard 
        localStorage.setItem('refreshContents', '1');
        this.router.navigateByUrl(`/daily-report/${this.attendaneId}`);
        form.reset();

      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();

      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    }, (error: any) => {

      console.log('error', error.error);
      this.api.presentValidateToast(error.error.message);
      this.api.dismissLoader();
    });
  }

  backToDailyReports() {
    this.router.navigate(['daily-report']);
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}
