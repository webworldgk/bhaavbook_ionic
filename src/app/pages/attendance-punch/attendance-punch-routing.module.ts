import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AttendancePunchPage } from './attendance-punch.page';

const routes: Routes = [
  {
    path: '',
    component: AttendancePunchPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AttendancePunchPageRoutingModule { }
