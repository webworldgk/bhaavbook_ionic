import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, Platform, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../providers/user-data';
import { Md5 } from 'ts-md5/dist/md5';
import { Capacitor } from '@capacitor/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Camera, CameraDirection, CameraResultType, CameraSource, Photo } from '@capacitor/camera';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';


declare var google: any;

@Component({
  selector: 'app-attendance-punch',
  templateUrl: './attendance-punch.page.html',
  styleUrls: ['./attendance-punch.page.scss'],
})
export class AttendancePunchPage implements OnInit {

  mapContent: any;
  address: string;

  @ViewChild('mapContent', { read: ElementRef, static: false }) mapRef: ElementRef;

  latitude: number;
  longitude: number;

  currentDate: any;
  currentTime: string;

  punchType: number = 1;
  punchCode: string;
  selfieUrl: string;
  selfieFile: File;

  punchState = {
    punchIn: null,
    punchOut: null,
  };

  constructor(
    public router: Router,
    public api: ApiService,
    public userData: UserData,
    private platform: Platform,
    public route: ActivatedRoute,
    private geolocation: Geolocation,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private nativeGeocoder: NativeGeocoder,
    private locationAccuracy: LocationAccuracy,
    private androidPermissions: AndroidPermissions,
  ) {
    this.punchCode = this.route.snapshot.paramMap.get('code');
    console.log(`this.punchCode`, this.punchCode);
  }

  ngOnInit() { }

  ionViewDidEnter() {

    this.punchState.punchIn = Md5.hashStr('1') as string;
    this.punchState.punchOut = Md5.hashStr('2') as string;
    if (this.punchState.punchOut == this.punchCode) this.punchType = 2;
    console.log(`this.punchType`, this.punchType);

    this.prepareDates();
    this.checkPermissions();
    this.loadPlaceHolderMap();
  }

  prepareDates() {
    const todaysDate = new Date();
    const dateOptions = { day: '2-digit', month: 'short', year: 'numeric' } as const;
    this.currentDate = todaysDate.toLocaleDateString("en-IN", dateOptions);

    const timeOptions = { hour: '2-digit', minute: '2-digit' } as const;
    this.currentTime = todaysDate.toLocaleString("en-IN", timeOptions);

    //console.log('this.currentDate', this.currentDate);
    //console.log('this.currentTime', this.currentTime);
  }

  checkPermissions() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        if (result.hasPermission) {
          this.enableGPS();
        } else {
          this.locationAccPermission();
        }
      },
      error => {
        if (Capacitor.getPlatform() == 'web') {
          this.api.presentToast('Tried to connect native device, please open the app on native device');
        } else {
          this.api.presentToast(error);
          console.log(`error`, error);
        }
      }
    );
  }

  locationAccPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        this.captureSelfie();
        this.captureCurrentLocationWithMap();
      } else {
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(
            () => {
              this.enableGPS();
            },
            error => {
              if (Capacitor.getPlatform() == 'web') {
                this.api.presentToast('Tried to connect native device, please open the app on native device');
              } else {
                this.api.presentToast(error);
                console.log(`error`, error);
              }
            }
          );
      }
    });
  }

  enableGPS() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => {
        this.captureSelfie();
        this.captureCurrentLocationWithMap();
      },
      error => {
        this.api.presentToast(JSON.stringify(error));
      }
    );
  }

  async captureSelfie() {

    const capturedPhoto = await Camera.getPhoto({
      source: CameraSource.Camera,
      resultType: CameraResultType.DataUrl,
      direction: CameraDirection.Rear,
      allowEditing: false,
      quality: 60,
    });

    const cameraImgDataUrl = capturedPhoto.dataUrl;
    this.selfieUrl = cameraImgDataUrl;
    // console.log('cameraImgDataUrl', this.cameraImgDataUrl);
    // console.log('capturedPhoto', capturedPhoto);

    let sCameraBlobFile: any;
    await fetch(cameraImgDataUrl).then(res => res.blob()).then((blob) => { sCameraBlobFile = blob; console.log(blob); });
    this.selfieFile = sCameraBlobFile;

    console.log('this.selfieFile', this.selfieFile);

    this.captureCurrentLocationWithMap();
  }

  captureCurrentLocationWithMap() {

    this.geolocation.getCurrentPosition().then((response) => {

      console.log('geolocation response', response);

      this.latitude = response.coords.latitude;
      this.longitude = response.coords.longitude;

      const coordinates = new google.maps.LatLng(this.latitude, this.longitude);
      const mapOptions = {
        center: coordinates,
        zoom: 15,
        disableDefaultUI: true,
        draggable: false,
        zoomControl: false,
        scrollwheel: false,
        gestureHandling: "none",
        disableDoubleClickZoom: true
      };

      this.mapContent = new google.maps.Map(this.mapRef.nativeElement, mapOptions);
      this.getAddressFromCoordinates(this.latitude, this.longitude);

      const mapMarker = new google.maps.Marker({
        position: coordinates,
        latitude: this.latitude,
        longitude: this.longitude,
      });

      mapMarker.setMap(this.mapContent);

    }).catch((error) => {
      console.log('Error getting location', error);
      this.api.presentToast(`Error getting location ${error}`);
    });

  }

  loadPlaceHolderMap() {

    // Ahmedabad Lat Long
    const mockLatitude = 23.0225;
    const mockLongitude = 72.5714;

    const coordinates = new google.maps.LatLng(mockLatitude, mockLongitude);
    const mapOptions = {
      center: coordinates,
      zoom: 2,
      disableDefaultUI: true,
      draggable: false,
      zoomControl: false,
      scrollwheel: false,
      gestureHandling: "none",
      disableDoubleClickZoom: true
    };

    this.mapContent = new google.maps.Map(this.mapRef.nativeElement, mapOptions);
    this.getAddressFromCoordinates(mockLatitude, mockLongitude);

    const mapMarker = new google.maps.Marker({
      position: coordinates,
      latitude: mockLatitude,
      longitude: mockLongitude,
    });

    mapMarker.setMap(this.mapContent);
  }

  getAddressFromCoordinates(lattitude: any, longitude: any) {

    console.log('getAddressFromCoordinates ' + lattitude + ' ' + longitude);

    const options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5,
    };

    this.nativeGeocoder.reverseGeocode(lattitude, longitude, options).then((result: NativeGeocoderResult[]) => {
      this.address = '';
      let responseAddress = [];
      for (let [key, value] of Object.entries(result[0])) {
        if (value.length > 0) responseAddress.push(value);
      }
      responseAddress.reverse();
      for (let value of responseAddress) {
        this.address += value + ', ';
      }
      this.address = this.address.slice(0, -2);
    }).catch((error: any) => {
      this.address = 'Address not available!';
    });
  }

  async performPunchInOut() {

    if (!this.latitude || !this.longitude) {
      this.checkPermissions();
    }

    if (!this.selfieUrl || !this.selfieFile) {
      this.captureSelfie();
    }

    if (this.selfieUrl && this.latitude && this.longitude) {

      await this.api.showLoader();

      const postParams = {
        punch_type: this.punchType,
        location: this.address,
        latitude: this.latitude,
        selfy_pic: this.selfieFile,
        selfy_name: this.selfieFile?.name ? this.selfieFile?.name : `P-${this.punchType}.jpg`,
        longitude: this.longitude,
      };

      console.log('postParams', postParams);
      const token = await this.userData.getToken();

      this.api.punchInOut(token, postParams).subscribe((response: any) => {
        console.log('punchInOut response', response);
        if (response.status === 1) {
          this.api.presentSuccessToast(response.message);

          // set it to reload dashboard 
          localStorage.setItem('refreshContents', '1');
          this.router.navigateByUrl('/dashboard');
          this.api.dismissLoader();

        } else if (response.statusCode === 401) {
          this.api.clearStorageAndRedirectToLogin(response.message);
          this.api.dismissLoader();

        } else {
          this.api.presentToast(response.message);
          this.api.dismissLoader();
        }
      });

    }
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}
