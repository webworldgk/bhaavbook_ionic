import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AttendancePunchPageRoutingModule } from './attendance-punch-routing.module';

import { AttendancePunchPage } from './attendance-punch.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AttendancePunchPageRoutingModule
  ],
  declarations: [AttendancePunchPage]
})
export class AttendancePunchPageModule {}
