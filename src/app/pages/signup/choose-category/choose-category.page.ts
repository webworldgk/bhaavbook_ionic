import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from 'src/app/providers/user-data';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-choose-category',
  templateUrl: './choose-category.page.html',
  styleUrls: ['./choose-category.page.scss'],
})
export class ChooseCategoryPage implements OnInit {

  userId: any;
  mobileNumber: any;
  categoryList: any = [];
  inputCategory: any = [];

  userInfo: any;
  isSkipAllowed = false;

  constructor(
    public router: Router,
    public api: ApiService,
    private http: HttpClient,
    public userData: UserData,
    public storage: Storage,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
  ) {
    this.userId = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() { }

  ionViewDidEnter() {
    this.prepareMasters();
    this.getSkipAllowedInformation();
  }

  async prepareMasters() {
    if (this.api.businessTypeList.length == 0) {
      const storeVariables = true;
      this.api.doStoreMasters(storeVariables);
    }
  }

  async submitCategory(form: NgForm) {
    // console.log(`form.value`, form.value);

    let selectedCategories = [];
    const formCategoryList = Object.entries(form.value);
    this.api.categoryList.map((categoryItem) => {
      formCategoryList.map(formValue => {
        let checkKey = `category_${parseInt(categoryItem.id)}`;
        if (formValue[0] == checkKey && formValue[1] === true) {
          selectedCategories.push(parseInt(categoryItem.id));
        }
      });
    });

    this.inputCategory = selectedCategories;
    // console.log('this.inputCategory', this.inputCategory);

    await this.api.showLoader('Please wait...');

    const apiName = `api/v1/auth/register`;
    const apiUrl = this.api.restApiUrl + apiName;
    const postParams = { category_id: this.inputCategory, user_id: this.userId, step: 6 };
    console.log('postParams', postParams);

    this.http.post(apiUrl, postParams).subscribe((response: any) => {
      console.log('submitCategory response', response);
      if (response.status === 1) {
        this.api.presentSuccessToast(response.message);
        this.router.navigateByUrl(`/address/${response.data.id}`);
        this.api.dismissLoader();
        form.reset();
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message, 5000, 'danger');
        this.api.dismissLoader();
      }
    }, (error: any) => {
      console.log('error', error.error);
      this.api.presentValidateToast(error.error.message, 5000, 'danger');
      this.api.dismissLoader();
    });

  }

  async getSkipAllowedInformation() {

    await this.api.showLoader('Please wait...');

    const checkParams = {
      user_id: this.userId,
      step: 6
    }

    const apiName = `api/v1/auth/checkSkipAllowed`;
    const apiUrl = this.api.restApiUrl + apiName;

    this.http.post(apiUrl, checkParams).subscribe((response: any) => {
      console.log('getSkipAllowedInformation Category response', response);
      if (response.status === 1) {
        this.api.dismissLoader();
        this.userInfo = response.data;
        this.isSkipAllowed = (response?.skip_allowed == 1) ? true : false;
        let categories = (response.data?.user_categories) ? response.data?.user_categories : [];
        this.api.categoryList.map((categoryItem) => {
          let isChecked = false;
          categories.map(item => {
            if (item && item.category_id && categoryItem.id == item.category_id.toString()) {
              isChecked = true;
            }
          });
          categoryItem.checked = isChecked;
          this.categoryList.push(categoryItem);
        });
        console.log('this.categoryList', this.categoryList);
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    }, (error: any) => {
      console.log('error', error.error);
      this.api.presentValidateToast(error.error.message);
      this.api.dismissLoader();
    });

  }

  skipChooseCategory() {
    this.router.navigate(['address', this.userId]);
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}
