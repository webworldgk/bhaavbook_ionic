import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from 'src/app/providers/user-data';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.page.html',
  styleUrls: ['./contacts.page.scss'],
})
export class ContactsPage implements OnInit {

  userId: any;
  isValidated = false;

  contactInfo = {
    mobileNumber: '',
    emailAddress: '',
    alternateMobile: '',
    whatsAppNumber: '',
    website: '',
    dateOfEstablish: '',
    gstNo: '',
    panNo: '',
    aadharNo: '',
  }

  userInfo: any;
  isSkipAllowed = false;

  constructor(
    public router: Router,
    public api: ApiService,
    private http: HttpClient,
    public userData: UserData,
    public storage: Storage,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
  ) {
    this.userId = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() { }

  ionViewDidEnter() {
    this.getSkipAllowedInformation();
  }

  submitContacts(form: NgForm) {

    // console.log(`form`, form);
    this.isValidated = true;

    if (!this.isSkipAllowed) {
      if (this.contactInfo.mobileNumber == '') {
        this.api.presentValidateToast('Mobile Number is required.', 4000, 'danger');
        this.isValidated = false;
        return false;
      }

      if (this.contactInfo.emailAddress == '') {
        this.api.presentValidateToast('Email Address is required.', 4000, 'danger');
        this.isValidated = false;
        return false;
      }

      // if (this.contactInfo.dateOfEstablish == '') {
      //   this.api.presentValidateToast('Date Of Establish is required.', 4000, 'danger');
      //   this.isValidated = false;
      //   return false;
      // }
    }

    if (this.isValidated) {

      const postParams = {
        user_id: this.userId,
        mobileNumber: this.contactInfo.mobileNumber,
        phone_number: this.contactInfo.alternateMobile,
        whatsapp: this.contactInfo.whatsAppNumber,
        email: this.contactInfo.emailAddress,
        website: this.contactInfo.website,
        date_of_establish: this.contactInfo.dateOfEstablish,
        gst_no: this.contactInfo.gstNo,
        pan_no: this.contactInfo.panNo,
        aadhar_number: this.contactInfo.aadharNo,
        step: 8,
      };

      console.log(`postParams`, postParams);
      this.addContacts(postParams, form);
    }
  }

  async addContacts(postParams: any, form: NgForm) {

    if (postParams) {

      await this.api.showLoader('Saving...');

      const apiName = `api/v1/auth/register`;
      const apiUrl = this.api.restApiUrl + apiName;
      const httpOptions = { headers: new HttpHeaders({}) };

      this.http.post(apiUrl, postParams, httpOptions).subscribe((response: any) => {
        console.log('addContacts response', response);
        if (response.status === 1) {
          this.api.presentSuccessToast(response.message);
          this.api.dismissLoader();
          this.activateUser();
          form.reset();
        } else if (response.statusCode === 401) {
          this.api.clearStorageAndRedirectToLogin(response.message);
          this.api.dismissLoader();
        } else {
          this.api.presentValidateToast(response.message);
          this.api.dismissLoader();
        }
      }, (error: any) => {
        console.log('error', error.error);
        this.api.presentValidateToast(error.error.message);
        this.api.dismissLoader();
      });
    }
  }

  async getSkipAllowedInformation() {

    await this.api.showLoader('Please wait...');

    const checkParams = {
      user_id: this.userId,
      step: 8
    }

    const apiName = `api/v1/auth/checkSkipAllowed`;
    const apiUrl = this.api.restApiUrl + apiName;

    this.http.post(apiUrl, checkParams).subscribe((response: any) => {
      console.log('getSkipAllowedInformation Address response', response);
      if (response.status == 1) {
        this.api.dismissLoader();
        this.userInfo = response.data;
        this.isSkipAllowed = (response?.skip_allowed == 1) ? true : false;
        this.contactInfo.mobileNumber = response?.data.mobile_number ? response.data.mobile_number : '';
        this.contactInfo.alternateMobile = response?.data.phone_number ? response.data.phone_number : '';
        this.contactInfo.whatsAppNumber = response?.data.whatsapp ? response.data.whatsapp : '';
        this.contactInfo.emailAddress = response?.data.email ? response.data.email : '';
        this.contactInfo.website = response?.data.website ? response.data.website : '';
        this.contactInfo.dateOfEstablish = response?.data.date_of_establish ? response.data.date_of_establish : '';
        this.contactInfo.gstNo = response?.data.gst_no ? response.data.gst_no : '';
        this.contactInfo.panNo = response?.data.pan_no ? response.data.pan_no : '';
        this.contactInfo.aadharNo = response?.data.aadhar_number ? response.data.aadhar_number : '';
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    }, (error: any) => {
      console.log('error', error.error);
      this.api.presentValidateToast(error.error.message);
      this.api.dismissLoader();
    });

  }

  async activateUser() {

    await this.api.showLoader('Activating...');

    const checkParams = {
      user_id: this.userId,
      mobile_number: this.userInfo.mobile_number
    }

    const apiName = `api/v1/auth/activateUser`;
    const apiUrl = this.api.restApiUrl + apiName;

    this.http.post(apiUrl, checkParams).subscribe((response: any) => {
      console.log('activateUser response', response);
      if (response.status == 1) {
        this.api.presentSuccessToast(response.message);
        this.api.dismissLoader();
        this.router.navigateByUrl(`/app-login`, { replaceUrl: true });
        // delete any local variable if you want to
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    }, (error: any) => {
      console.log('error', error.error);
      this.api.presentValidateToast(error.error.message);
      this.api.dismissLoader();
    });

  }

  skipContacts() {
    // this is the last step, so activate the user and redirect to login page
    this.activateUser();
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}
