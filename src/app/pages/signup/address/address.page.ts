import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from 'src/app/providers/user-data';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-address',
  templateUrl: './address.page.html',
  styleUrls: ['./address.page.scss'],
})
export class AddressPage implements OnInit {

  userId: any;
  isValidated = false;

  addressInfo = {
    buildingName: '',
    areaName: '',
    pincode: '',
    stateId: '',
    cityId: '',
  }

  userInfo: any;
  isSkipAllowed = false;

  constructor(
    public router: Router,
    public api: ApiService,
    private http: HttpClient,
    public userData: UserData,
    public storage: Storage,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
  ) {
    this.userId = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() { }

  ionViewDidEnter() {
    this.prepareMasters();
    this.getSkipAllowedInformation();
  }

  async prepareMasters() {
    if (this.api.businessTypeList.length == 0) {
      const storeVariables = true;
      this.api.doStoreMasters(storeVariables);
    }
  }

  submitAddress(form: NgForm) {

    // console.log(`form`, form);
    this.isValidated = true;

    if (this.addressInfo.buildingName == '') {
      this.api.presentValidateToast('Building Name is required.', 4000, 'danger');
      this.isValidated = false;
      return false;
    }

    if (this.addressInfo.areaName == '') {
      this.api.presentValidateToast('Street/Area is required.', 4000, 'danger');
      this.isValidated = false;
      return false;
    }

    if (this.addressInfo.pincode == '') {
      this.api.presentValidateToast('Pincode is required.', 4000, 'danger');
      this.isValidated = false;
      return false;
    }

    if (this.isValidated) {

      const postParams = {
        user_id: this.userId,
        address: this.addressInfo.buildingName,
        area: this.addressInfo.areaName,
        pincode: this.addressInfo.pincode,
        state_id: this.addressInfo.stateId,
        city_id: this.addressInfo.cityId,
        step: 7,
      };

      console.log(`postParams`, postParams);
      this.addAddressInfo(postParams, form);
    }
  }

  async addAddressInfo(postParams: any, form: NgForm) {

    if (postParams) {

      await this.api.showLoader('Saving...');

      const apiName = `api/v1/auth/register`;
      const apiUrl = this.api.restApiUrl + apiName;
      const httpOptions = { headers: new HttpHeaders({}) };

      this.http.post(apiUrl, postParams, httpOptions).subscribe((response: any) => {
        console.log('addAddressInfo response', response);
        if (response.status === 1) {
          this.api.presentSuccessToast(response.message);
          this.api.dismissLoader();
          this.router.navigateByUrl(`/contacts/${response.data.id}`);
          form.reset();
        } else if (response.statusCode === 401) {
          this.api.clearStorageAndRedirectToLogin(response.message);
          this.api.dismissLoader();
        } else {
          this.api.presentValidateToast(response.message);
          this.api.dismissLoader();
        }
      }, (error: any) => {
        console.log('error', error.error);
        this.api.presentValidateToast(error.error.message);
        this.api.dismissLoader();
      });
    }
  }

  async getSkipAllowedInformation() {

    await this.api.showLoader('Please wait...');

    const checkParams = {
      user_id: this.userId,
      step: 7
    }

    const apiName = `api/v1/auth/checkSkipAllowed`;
    const apiUrl = this.api.restApiUrl + apiName;

    this.http.post(apiUrl, checkParams).subscribe((response: any) => {
      console.log('getSkipAllowedInformation Address response', response);
      if (response.status == 1) {
        this.api.dismissLoader();
        this.userInfo = response.data;
        this.isSkipAllowed = (response?.skip_allowed == 1) ? true : false;
        this.addressInfo.buildingName = response?.data.address ? response.data.address : '';
        this.addressInfo.areaName = response?.data.area ? response.data.area : '';
        this.addressInfo.pincode = response?.data.pincode ? response.data.pincode : '';
        this.addressInfo.cityId = response?.data.city_id ? response.data.city_id.toString() : '';
        this.addressInfo.stateId = response?.data.state_id ? response.data.state_id.toString() : '';
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    }, (error: any) => {
      console.log('error', error.error);
      this.api.presentValidateToast(error.error.message);
      this.api.dismissLoader();
    });

  }

  skipAddress() {
    this.router.navigate(['contacts', this.userId]);
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}
