import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BusinessTypePageRoutingModule } from './business-type-routing.module';

import { BusinessTypePage } from './business-type.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BusinessTypePageRoutingModule
  ],
  declarations: [BusinessTypePage]
})
export class BusinessTypePageModule {}
