import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusinessTypePage } from './business-type.page';

const routes: Routes = [
  {
    path: '',
    component: BusinessTypePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusinessTypePageRoutingModule {}
