import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from 'src/app/providers/user-data';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-choose-package',
  templateUrl: './choose-package.page.html',
  styleUrls: ['./choose-package.page.scss'],
})
export class ChoosePackagePage implements OnInit {

  userId: any;
  mobileNumber: any;
  inputPackageType: number;

  userInfo: any;
  isSkipAllowed = false;

  constructor(
    public router: Router,
    public api: ApiService,
    private http: HttpClient,
    public userData: UserData,
    public storage: Storage,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
  ) {
    this.userId = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() { }

  ionViewDidEnter() {
    this.prepareMasters();
    this.getSkipAllowedInformation();
  }

  selectOptions(packageTypeId: any) {
    // console.log('packageTypeId', packageTypeId);
    if (packageTypeId) {
      this.inputPackageType = packageTypeId;
    }
  }

  async prepareMasters() {
    if (this.api.businessTypeList.length == 0) {
      const storeVariables = true;
      this.api.doStoreMasters(storeVariables);
    }
  }

  async submitPackageType(form: NgForm) {
    // console.log(`form`, form);

    await this.api.showLoader('Please wait...');

    const apiName = `api/v1/auth/register`;
    const apiUrl = this.api.restApiUrl + apiName;
    const postParams = { package_id: this.inputPackageType, user_id: this.userId, step: 5 };

    this.http.post(apiUrl, postParams).subscribe((response: any) => {
      console.log('submitPackageType response', response);
      if (response.status === 1) {
        this.api.presentSuccessToast(response.message);
        this.router.navigateByUrl(`/choose-category/${response.data.id}`);
        this.api.dismissLoader();
        form.reset();
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message, 5000, 'danger');
        this.api.dismissLoader();
      }
    }, (error: any) => {
      console.log('error', error.error);
      this.api.presentValidateToast(error.error.message, 5000, 'danger');
      this.api.dismissLoader();
    });

  }

  async getSkipAllowedInformation() {

    await this.api.showLoader('Please wait...');

    const checkParams = {
      user_id: this.userId,
      step: 5
    }

    const apiName = `api/v1/auth/checkSkipAllowed`;
    const apiUrl = this.api.restApiUrl + apiName;

    this.http.post(apiUrl, checkParams).subscribe((response: any) => {
      console.log('getSkipAllowedInformation PackageType response', response);
      if (response.status == 1) {
        this.api.dismissLoader();
        this.userInfo = response.data;
        this.isSkipAllowed = (response?.skip_allowed == 1) ? true : false;
        this.inputPackageType = (response.data?.package_id) ? response.data?.package_id.toString() : 0;
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    }, (error: any) => {
      console.log('error', error.error);
      this.api.presentValidateToast(error.error.message);
      this.api.dismissLoader();
    });

  }

  skipPackageType() {
    this.router.navigate(['choose-category', this.userId]);
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}
