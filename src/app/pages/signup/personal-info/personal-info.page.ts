import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from 'src/app/providers/user-data';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-personal-info',
  templateUrl: './personal-info.page.html',
  styleUrls: ['./personal-info.page.scss'],
})
export class PersonalInfoPage implements OnInit {

  userId: any;
  isValidated = false;
  showPassword = false;
  passwordType = 'password';

  personalInfo = {
    firstName: '',
    lastName: '',
    emailAddress: '',
    mobileNumber: '',
    password: '',
    confirmPassword: '',
  }

  userInfo: any;
  isSkipAllowed = false;

  constructor(
    public router: Router,
    public api: ApiService,
    private http: HttpClient,
    public userData: UserData,
    public storage: Storage,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
  ) {
    this.userId = parseInt(this.route.snapshot.paramMap.get('id'));
  }

  ngOnInit() { }

  ionViewWillEnter() {
    this.getSkipAllowedInformation();
  }

  ionViewDidEnter() {
    this.prepareMasters();
  }

  async prepareMasters() {
    if (this.api.businessTypeList.length == 0) {
      const storeVariables = true;
      this.api.doStoreMasters(storeVariables);
    }
  }

  submitPersonalInfo(form: NgForm) {

    // console.log(`form`, form);
    this.isValidated = true;

    if (this.personalInfo.firstName == '') {
      this.api.presentValidateToast('First Name is required.', 4000, 'danger');
      this.isValidated = false;
      return false;
    }

    if (!this.isSkipAllowed) {
      if (this.personalInfo.password == '') {
        this.api.presentValidateToast('Password is required.', 4000, 'danger');
        this.isValidated = false;
        return false;
      }

      if (this.personalInfo.password !== this.personalInfo.confirmPassword) {
        this.api.presentValidateToast("Password doesn't matched.", 4000, 'danger');
        this.isValidated = false;
        return false;
      }
    }

    if (this.isValidated) {

      const postParams = {
        user_id: this.userId,
        first_name: this.personalInfo.firstName,
        last_name: this.personalInfo.lastName,
        email: this.personalInfo.emailAddress,
        mobile_number: this.personalInfo.mobileNumber,
        password: this.personalInfo.password,
        step: 3,
      };

      console.log(`postParams`, postParams);
      this.addPersonalInfo(postParams, form);
    }
  }

  async addPersonalInfo(postParams: any, form: NgForm) {

    if (postParams) {

      await this.api.showLoader('Saving...');

      const apiName = `api/v1/auth/register`;
      const apiUrl = this.api.restApiUrl + apiName;
      const httpOptions = { headers: new HttpHeaders({}) };

      this.http.post(apiUrl, postParams, httpOptions).subscribe((response: any) => {
        console.log('addPersonalInfo response', response);
        if (response.status === 1) {
          this.api.presentSuccessToast(response.message);
          this.api.dismissLoader();
          this.router.navigateByUrl(`/business-type/${response.data.id}`);
          form.reset();
        } else if (response.statusCode === 401) {
          this.api.clearStorageAndRedirectToLogin(response.message);
          this.api.dismissLoader();
        } else {
          this.api.presentValidateToast(response.message);
          this.api.dismissLoader();
        }
      }, (error: any) => {
        console.log('error', error.error);
        this.api.presentValidateToast(error.error.message, 5000, 'danger');
        this.api.dismissLoader();
      });
    }
  }

  async getSkipAllowedInformation() {

    await this.api.showLoader('Please wait...');

    const checkParams = {
      user_id: this.userId,
      step: 3
    }

    const apiName = `api/v1/auth/checkSkipAllowed`;
    const apiUrl = this.api.restApiUrl + apiName;

    this.http.post(apiUrl, checkParams).subscribe((response: any) => {
      console.log('getSkipAllowedInformation PersonalInfo response', response);
      if (response.status === 1) {
        this.api.dismissLoader();
        this.userInfo = response.data;
        this.isSkipAllowed = (response?.skip_allowed == 1) ? true : false;
        // this.personalInfo.firstName = response.data?.first_name ? response.data?.first_name : '';
        // this.personalInfo.lastName = response.data?.last_name ? response.data?.last_name : '';
        // this.personalInfo.emailAddress = response.data?.email ? response.data?.email : '';
        this.personalInfo.mobileNumber = response.data?.mobile_number ? response.data?.mobile_number : '';
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    }, (error: any) => {
      console.log('error', error.error);
      this.api.presentValidateToast(error.error.message);
      this.api.dismissLoader();
    });

  }

  togglePassword() {
    this.showPassword = !this.showPassword;
    this.passwordType = this.passwordType == 'password' ? 'text' : 'password';
  }

  skipPeronalInfo() {
    this.router.navigate(['business-type', this.userId]);
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}
