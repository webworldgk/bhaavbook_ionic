import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from 'src/app/providers/user-data';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-otp',
  templateUrl: './otp.page.html',
  styleUrls: ['./otp.page.scss'],
})
export class OtpPage implements OnInit {

  inputOtp: number;
  inputMobile: any;
  mockedMobile = 'XXXXXX';

  isResendEnable = false;
  resendTimeInSec = 179;
  displayMinute: any;
  displaySecond: any;
  timerMinute: number;
  timerSecond: number;
  timerInterval: any;

  constructor(
    public router: Router,
    public api: ApiService,
    private http: HttpClient,
    public userData: UserData,
    public storage: Storage,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
  ) {
    this.inputMobile = this.route.snapshot.paramMap.get('mobile');
    if (this.inputMobile) {
      this.mockedMobile += this.inputMobile.slice(-4);
    }
  }

  ngOnInit() { }

  ionViewDidEnter() {
    this.prepareResendOtp();
    this.prepareMasters();
  }

  async prepareMasters() {
    if (this.api.businessTypeList.length == 0) {
      const storeVariables = true;
      this.api.doStoreMasters(storeVariables);
    }
  }

  prepareResendOtp() {
    this.timerMinute = Math.floor(this.resendTimeInSec / 60);
    this.timerSecond = Math.abs((this.timerMinute * 60) - this.resendTimeInSec);
    this.timerInterval = setInterval(() => { this.startCountdown() }, 1000);
    this.applyPads();
  }

  startCountdown() {
    this.timerSecond--;
    // console.log('timerSecond', this.timerSecond);
    if (this.timerSecond == 0) {
      this.timerMinute -= 1;
      if (this.timerMinute > 0) {
        this.timerSecond = 59;
      } else {
        clearInterval(this.timerInterval);
        this.isResendEnable = true;
        this.timerSecond = this.timerMinute = 0;
      }
    }
    this.applyPads();
  }

  applyPads() {
    this.displayMinute = (this.timerMinute < 10) ? '0' + this.timerMinute : this.timerMinute;
    this.displaySecond = (this.timerSecond < 10) ? '0' + this.timerSecond : this.timerSecond;
  }

  async submitOtp(form: NgForm) {
    // console.log(`form`, form);

    if (this.inputOtp <= 0 || !this.inputOtp) {
      this.api.presentValidateToast('The OTP field is required.', 4000, 'danger');
      return false;
    }
    if (this.inputOtp.toString().length !== 6) {
      this.api.presentValidateToast('Please enter valid otp.', 4000, 'danger');
      return false;
    }

    await this.api.showLoader('Verify OTP...');

    const apiName = `api/v1/auth/register`;
    const apiUrl = this.api.restApiUrl + apiName;
    const postParams = { mobile_number: this.inputMobile, otp: this.inputOtp, step: 2 };

    this.http.post(apiUrl, postParams).subscribe((response: any) => {
      console.log('submitOtp response', response);
      if (response.status === 1) {
        this.api.presentSuccessToast(response.message);
        this.router.navigateByUrl(`/personal-info/${response.data.id}`);
        this.api.dismissLoader();
        form.reset();
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message, 5000, 'danger');
        this.api.dismissLoader();
      }
    }, (error: any) => {
      console.log('error', error.error);
      this.api.presentValidateToast(error.error.message, 5000, 'danger');
      this.api.dismissLoader();
    });

  }

  resendOtp() {
    if (this.isResendEnable) {
      console.log('resend available');
    }
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}
