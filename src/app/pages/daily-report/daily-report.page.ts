import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../providers/user-data';

@Component({
  selector: 'app-daily-report',
  templateUrl: './daily-report.page.html',
  styleUrls: ['./daily-report.page.scss'],
})
export class DailyReportPage implements OnInit {

  dailyReportList: any;
  dailyReportCount: number;
  isBack: boolean;

  constructor(
    public router: Router,
    public api: ApiService,
    public userData: UserData,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
  ) {
    this.isBack = (this.route.snapshot.paramMap.get('back') == '1') ? true : false;
    console.log(`this.isBack`, this.isBack);
  }

  ngOnInit() {
    if (localStorage.getItem('refreshContents') !== '1') {
      this.getDailyReportsList();
    }
  }

  ionViewWillEnter() {
    if (localStorage.getItem('refreshContents') == '1') {
      this.getDailyReportsList();
    }
  }

  // Get daily Reports Data using API
  async getDailyReportsList() {

    await this.api.showLoader();

    const token = await this.userData.getToken();
    this.api.getDailyReports(token).subscribe((response: any) => {
      console.log('getDailyReports response', response);
      if (response.status == 1) {
        this.dailyReportList = response.data;
        this.dailyReportCount = response.data.length;
        localStorage.setItem('refreshContents', '0');
        this.api.dismissLoader();
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });

  }

  addDailyReport() {
    this.router.navigate(['add-daily-report']);
  }

  viewDailyReport(reportId: any) {
    this.router.navigate(['add-daily-report', reportId]);
  }

  async condirmRemoveDailyReport(reportId: any) {
    const alert = await this.alertCtrl.create({
      header: 'Confirm Remove',
      message: 'Are you sure you want to remove?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: (no) => {
            console.log('Confirm Cancel: ', no);
          }
        }, {
          text: 'Remove',
          handler: () => {
            console.log('Confirm Okay');
            this.removeDailyReport(reportId);
          }
        }
      ]
    });

    await alert.present();
  }

  async removeDailyReport(reportId: any) {

    if (reportId) {

      await this.api.showLoader('Removing...');

      const token = await this.userData.getToken();
      this.api.deleteDailyReport(token, reportId).subscribe((response: any) => {
        console.log('deleteDailyReport response', response);
        if (response.status == 1) {
          this.api.presentSuccessToast(response.message);
          localStorage.setItem('refreshContents', '1');
          this.getDailyReportsList();
          this.api.dismissLoader();
        } else if (response.statusCode === 401) {
          this.api.clearStorageAndRedirectToLogin(response.message);
          this.api.dismissLoader();
        } else {
          this.api.presentValidateToast(response.message);
          this.api.dismissLoader();
        }
      });
    }
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}
