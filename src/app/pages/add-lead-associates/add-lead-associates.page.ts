import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../providers/user-data';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-add-lead-associates',
  templateUrl: './add-lead-associates.page.html',
  styleUrls: ['./add-lead-associates.page.scss'],
})
export class AddLeadAssociatesPage implements OnInit {

  token: any;
  leadId: any;
  isValidated: boolean;

  associatesList: any[] = [];
  associatesCount: number;

  searchedCustomers: any[] = [];
  searchedCustomerCount: number;

  subType: string;
  customerPercentages: any[] = [];
  customerHandledBy: any[] = [];
  searchTerm: string;

  customActionCommonOptions: any = {
    header: 'Select...'
  };

  constructor(
    public router: Router,
    public api: ApiService,
    private http: HttpClient,
    public userData: UserData,
    public storage: Storage,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
  ) {
    this.leadId = this.route.snapshot.paramMap.get('leadId');
    console.log(`this.leadId`, this.leadId);
  }

  ngOnInit() { }

  ionViewDidEnter() {
    this.prepareMasters();
  }

  async prepareMasters() {
    if (this.api.subTypes.length == 0) {
      const storeVariables = true;
      this.api.doStoreMasters(storeVariables);
    }

    this.token = await this.userData.getToken();
    // this.testCase();
  }

  searchByText(event: any) {
    this.searchTerm = event.detail?.value;
    console.log(`this.searchTerm`, this.searchTerm);
    if (this.searchTerm) {
      const searchParams = {
        ctype: parseInt(this.subType),
        searchTerm: this.searchTerm,
      };
      this.searchLeadAssociates(searchParams);
    }
  }

  async searchLeadAssociates(searchParams: any) {

    const apiName = `api/v1/leadassociates/searchassociates`;
    const apiUrl = this.api.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: this.token }) };

    this.http.post(apiUrl, searchParams, httpOptions).subscribe((response: any) => {
      console.log('searchLeadAssociates response', response);
      if (response.status == 1) {
        this.searchedCustomers = response.data;
        this.searchedCustomerCount = response.data.length;
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
      } else {
        this.api.presentValidateToast(response.message);
      }
    }, (error: any) => {
      console.log('error', error.error);
      this.api.presentValidateToast(error.error.message);
    });
  }

  changeProjectType(event: any) {
    this.searchedCustomers = [];
    this.searchedCustomerCount = 0;
    this.searchTerm = '';
  }

  chooseCustomer(customerId: number) {

    console.log(`customerId`, customerId);
    const customerFound = this.associatesList.some(item => item.customer_id === customerId);
    console.log(`customerFound`, customerFound);

    if (customerFound === true) {
      this.api.presentValidateToast('Already Added!');
    }

    // insert only if customer does not exist in the list
    if (customerFound === false) {

      const apiName = `api/v1/leadassociates/fetchcustomerdetail`;
      const apiUrl = this.api.restApiUrl + apiName;
      const httpOptions = { headers: new HttpHeaders({ Authorization: this.token }) };
      const postParams = { customer_id: customerId };

      this.http.post(apiUrl, postParams, httpOptions).subscribe((response: any) => {
        console.log('fetchCustomerDetail response', response);
        if (response.status === 1) {
          if (response.data && response.data) {
            this.associatesList.push(response.data);
            this.searchedCustomers = [];
            this.searchedCustomerCount = 0;
          }
          this.associatesCount = this.associatesList.length;
          console.log(`this.associatesList`, this.associatesList);
        } else if (response.statusCode === 401) {
          this.api.clearStorageAndRedirectToLogin(response.message);
        } else {
          this.api.presentValidateToast(response.message);
        }
      }, (error: any) => {
        console.log('error', error.error);
        this.api.presentValidateToast(error.error.message);
      });
    }
  }

  removeCustomer(customerKey: number) {
    this.associatesList.splice(customerKey, 1);
    this.associatesCount = this.associatesList.length;
  }

  submitLeadAssociates(form: NgForm) {

    const formData = form.form.value;
    console.log(`formData`, formData);

    this.isValidated = true;

    if (!this.associatesCount || this.associatesCount == 0) {
      this.api.presentValidateToast('Customer is required.', 4000, 'danger');
      this.isValidated = false;
    }

    if (this.isValidated) {

      const formValues = Object.entries(formData);
      console.log(`formValues`, formValues);

      const customerIds = this.associatesList.reduce((previousValue, item) => {
        previousValue.push(item.customer_id);
        return previousValue;
      }, []);

      const profitPercentageList = this.associatesList.reduce((previousValue, item, currentIndex) => {
        formValues.map(formItem => {
          if (formItem && formItem[0] === `assi_percentage_${currentIndex}`) {
            let assignValue = formItem[1] ? formItem[1] : ''
            previousValue.push(assignValue);
          }
        });
        return previousValue;
      }, []);

      const handledByList = this.associatesList.reduce((previousValue, item, currentIndex) => {
        formValues.map(formItem => {
          if (formItem && formItem[0] === `assi_archi_${currentIndex}`) {
            let assignValue = formItem[1] ? formItem[1] : ''
            previousValue.push(assignValue);
          }
        });
        return previousValue;
      }, []);

      const postParams = {
        lead_id: this.leadId,
        customer_id: customerIds,
        profit_per: profitPercentageList,
        handled_by: handledByList,
      }

      console.log(`postParams`, postParams);
      this.addLeadAssociates(postParams, form);
    }

  }

  async addLeadAssociates(postParams: any, form: NgForm) {

    if (postParams) {

      await this.api.showLoader('Saving...');

      const apiName = `api/v1/leadassociates`;
      const apiUrl = this.api.restApiUrl + apiName;
      const httpOptions = { headers: new HttpHeaders({ Authorization: this.token }) };

      this.http.post(apiUrl, postParams, httpOptions).subscribe((response: any) => {
        console.log('addLeadAssociates response', response);
        if (response.status == 1) {
          this.api.presentSuccessToast(response.message);
          this.api.dismissLoader();
          this.router.navigateByUrl(`/leads-detail/${this.leadId}/4`);
          form.reset();
        } else if (response.statusCode === 401) {
          this.api.clearStorageAndRedirectToLogin(response.message);
          this.api.dismissLoader();
        } else {
          this.api.presentValidateToast(response.message);
          this.api.dismissLoader();
        }
      }, (error: any) => {
        console.log('error', error.error);
        this.api.presentValidateToast(error.error.message);
        this.api.dismissLoader();
      });
    }
  }

  backToLeadDashbaord() {
    if (this.leadId) {
      this.router.navigate(['leads-detail', this.leadId, 4]); // 4. Associates
    } else {
      this.router.navigate(['leads-dashboard']);
    }
  }

  testCase() {
    const myList: any[] = [{
      assi_archi: [],
      customer_firm_name: null,
      customer_id: 1,
      customer_name: 'Kamlesh',
      customer_sub_type: 'Project',
    }, {
      assi_archi: [],
      customer_firm_name: null,
      customer_id: 15,
      customer_name: 'shubham parekh',
      customer_sub_type: 'Project',
    }, {
      assi_archi: [],
      customer_firm_name: null,
      customer_id: 16,
      customer_name: 'parekh',
      customer_sub_type: 'Project',
    }];

    const formValue = {
      assi_archi_0: undefined,
      assi_archi_1: undefined,
      assi_archi_2: undefined,
      assi_percentage_0: 2,
      assi_percentage_1: 5,
      assi_percentage_2: 6,
      subType: '1',
    };

    const formValues = Object.entries(formValue);
    console.log('formValues', formValues);

    const profit_percentages = myList.reduce((previousValue, item, currentIndex) => {
      console.log(`currentIndex`, currentIndex);
      formValues.map(formItem => {
        if (formItem && formItem[0] === `assi_percentage_${currentIndex}`) {
          let assignValue = formItem[1] ? formItem[1] : ''
          console.log(`assignValue`, assignValue);
          previousValue.push(assignValue);
        }
      });
      return previousValue;
    }, []);

    const handledByList = myList.reduce((previousValue, item, currentIndex) => {
      console.log(`currentIndex`, currentIndex);
      formValues.map(formItem => {
        if (formItem && formItem[0] === `assi_archi_${currentIndex}`) {
          let assignValue = formItem[1] ? formItem[1] : ''
          console.log(`assignValue`, assignValue);
          previousValue.push(assignValue);
        }
      });
      return previousValue;
    }, []);

    console.log(`myList`, myList);
    console.log(`formValue`, formValue);
    console.log(`profit_percentages`, profit_percentages);
    console.log(`handledByList`, handledByList);
  }

  async ionViewDidLeave() {
    this.associatesList = [];
    this.associatesList = [];
    this.searchedCustomers = [];
    this.associatesCount = 0;
    this.associatesCount = 0;
    this.searchedCustomerCount = 0;
    this.subType = '';
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}
