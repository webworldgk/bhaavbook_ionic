import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddLeadAssociatesPageRoutingModule } from './add-lead-associates-routing.module';

import { AddLeadAssociatesPage } from './add-lead-associates.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddLeadAssociatesPageRoutingModule
  ],
  declarations: [AddLeadAssociatesPage]
})
export class AddLeadAssociatesPageModule {}
