import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddLeadAssociatesPage } from './add-lead-associates.page';

const routes: Routes = [
  {
    path: '',
    component: AddLeadAssociatesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddLeadAssociatesPageRoutingModule {}
