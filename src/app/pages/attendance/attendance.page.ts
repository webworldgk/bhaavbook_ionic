import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../providers/user-data';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.page.html',
  styleUrls: ['./attendance.page.scss'],
})
export class AttendancePage implements OnInit {

  attendanceList: any;
  dInputStartDate: string;
  dInputEndDate: string;
  dMaxDate: string;
  attendanceCount: number;


  dStartDate: string;
  dEndDate: string;

  status: any;
  statusLabelName: any;
  statusLabel = {
    1: "Absent",
    2: "Present",
    3: "Late",
    4: "MisPunch",
    5: "OverTime"
  };

  isEnableSearchByDateRange: boolean = false;
  isBack: boolean;

  constructor(
    public router: Router,
    public api: ApiService,
    public userData: UserData,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    private loadingCtrl: LoadingController
  ) {
    this.isBack = (this.route.snapshot.paramMap.get('back') == '1') ? true : false;
    console.log(`this.isBack`, this.isBack);

    this.status = this.route.snapshot.paramMap.get('status');
    this.setStatusLabel();
  }

  ngOnInit() {
    this.prepareDates();
    this.getAttendanceList();
  }

  ionViewDidEnter() {
    this.prepareDates();
    if (localStorage.getItem('refreshContents') == '1') {
      this.getAttendanceList();
    }
  }

  prepareDates() {
    const currentDate = new Date();

    var firstDay = new Date(currentDate.getFullYear(), currentDate.getMonth(), 2).toISOString();
    var lastDay = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 1).toISOString();

    // console.log([firstDay, lastDay]);
    this.dInputStartDate = firstDay.split('T')[0];
    this.dInputEndDate = lastDay.split('T')[0];

    console.log('prepareDates dInputStartDate', this.dInputStartDate);
    console.log('prepareDates dInputEndDate', this.dInputEndDate);
  }

  async getAttendanceList() {

    await this.api.showLoader();

    this.formateDates();

    console.log('dStartDate', this.dStartDate);
    console.log('dEndDate', this.dEndDate);

    const token = await this.userData.getToken();

    this.api.getAttendances(token, this.dStartDate, this.dEndDate, this.status).subscribe((response: any) => {
      console.log('getAttendances response', response);
      if (response.status == 1) {
        this.attendanceList = response.data;
        this.attendanceCount = response.data.length;
        this.api.dismissLoader();

        // reset in order to not refresh again
        localStorage.setItem('refreshContents', '0');
      } else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();

      } else {
        this.api.dismissLoader();
        const messageStr = response?.message ? response?.message : 'Error while requesting for Attendances.';
        this.api.presentValidateToast(messageStr);
      }
    });
  }

  openAttendaceDetail(id: number) {
    // console.log('id', id);
    if (id > 0) {
      this.router.navigateByUrl(`/attendance-detail/${id}`);
    }
  }

  formateDates() {
    this.dStartDate = this.dInputStartDate;
    this.dEndDate = this.dInputEndDate;
    if (this.dInputStartDate && this.dInputStartDate.length > 10) {
      this.dEndDate = this.dInputEndDate.split('T')[0];
    }
    if (this.dInputEndDate && this.dInputEndDate.length > 10) {
      this.dEndDate = this.dInputEndDate.split('T')[0];
    }
  }

  openDateRangeFilters() {
    this.isEnableSearchByDateRange = true;
  }

  applyDateRangeFilters() {
    console.log('dInputStartDate', this.dInputStartDate);
    console.log('dInputEndDate', this.dInputEndDate);

    this.getAttendanceList();
    this.isEnableSearchByDateRange = false;
  }

  cancelDateRangeFilters() {
    this.prepareDates();
    this.getAttendanceList();
    this.isEnableSearchByDateRange = false;
  }

  setStatusLabel() {
    if (this.statusLabel[this.status]) {
      this.statusLabelName = this.statusLabel[this.status];
    }
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}
