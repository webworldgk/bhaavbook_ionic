import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../providers/user-data';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.page.html',
  styleUrls: ['./reports.page.scss'],
})
export class ReportsPage implements OnInit {

  values = [];

  isBack: boolean;

  constructor(
    public userData: UserData,
    public router: Router,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public api: ApiService,
  ) {
    this.isBack = (this.route.snapshot.paramMap.get('back') == '1') ? true : false;
    console.log(`this.isBack`, this.isBack);
  }

  ngOnInit() {
  }

  removevalue(i) {
    this.values.splice(i, 1);
  }

  addvalue() {
    this.values.push({ values: '' });
  }

}
