import { Component, OnInit } from '@angular/core';
import { FormGroup, NgForm, Validators } from '@angular/forms';
import { FormBuilder, Validator } from '@angular/forms';
import { ActivatedRoute, Router ,} from '@angular/router';

@Component({
  selector: 'app-package-page',
  templateUrl: './package-page.page.html',
  styleUrls: ['./package-page.page.scss'],
})
export class PackagePagePage implements OnInit {

  ionicForm: FormGroup;
  defaultDate = "2022-01-30";
  defaultDate1 = "2022-06-30";
  isSubmitted = false;
  constructor(public formBuilder: FormBuilder,
    public router: Router,) { }
  ngOnInit() {
    this.ionicForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(2)]],
      email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      dob: [this.defaultDate],
      mobile: ['', [Validators.required, Validators.pattern('^[0-9]+$')]]
    })
  }
  getDate(e) {
    let date = new Date(e.target.value).toISOString().substring(0, 10);
    this.ionicForm.get('dob').setValue(date, {
      onlyself: true
    })
  }
  get errorControl() {
    return this.ionicForm.controls;
  }
  submitForm() {
    this.isSubmitted = true;
    if (!this.ionicForm.valid) {
      console.log('Please provide all the required values!')
      return false;
    } else {
      console.log(this.ionicForm.value)
    }
  }

  onsubmit(){
    this.router.navigateByUrl('/dashboard');
  }

}
