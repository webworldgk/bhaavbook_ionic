import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PackagePagePage } from './package-page.page';

const routes: Routes = [
  {
    path: '',
    component: PackagePagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PackagePagePageRoutingModule {}
