import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PackagePagePageRoutingModule } from './package-page-routing.module';

import { PackagePagePage } from './package-page.page';
import { ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    PackagePagePageRoutingModule
  ],
  declarations: [PackagePagePage]
})
export class PackagePagePageModule {}
