import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddLeadReminderPage } from './add-lead-reminder.page';

const routes: Routes = [
  {
    path: '',
    component: AddLeadReminderPage
  },
  {
    path: ':leadId',
    component: AddLeadReminderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddLeadReminderPageRoutingModule {}
