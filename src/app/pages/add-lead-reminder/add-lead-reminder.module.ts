import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddLeadReminderPageRoutingModule } from './add-lead-reminder-routing.module';

import { AddLeadReminderPage } from './add-lead-reminder.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddLeadReminderPageRoutingModule
  ],
  declarations: [AddLeadReminderPage]
})
export class AddLeadReminderPageModule {}
