import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DailyReportDetailsPageRoutingModule } from './daily-report-details-routing.module';

import { DailyReportDetailsPage } from './daily-report-details.page';

import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DailyReportDetailsPageRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [DailyReportDetailsPage]
})
export class DailyReportDetailsPageModule {}
