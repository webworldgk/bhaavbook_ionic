import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../providers/user-data';
import { NavController } from '@ionic/angular';
import { ActionSheetController } from '@ionic/angular';


import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-daily-report-details',
  templateUrl: './daily-report-details.page.html',
  styleUrls: ['./daily-report-details.page.scss'],
})
export class DailyReportDetailsPage implements OnInit {


  dailyReportView: any;
  user: any;
  summary: any;
  attendance: any;
  public myForm: FormGroup;
  public myForm1: FormGroup;
  public myForm2: FormGroup;
  private textid = 1;

  private txt1 = 1;
  private txt2 = 1;
  private txt3 = 1;

  private tx1 = 1;
  private tx2 = 1;



  constructor(
    public userData: UserData,
    public router: Router,
    public toastCtrl: ToastController,
    public api: ApiService,
    private formBuilder: FormBuilder,
    public navCtrl: NavController,

  ) {

    this.myForm = formBuilder.group({
      textbox1: ['', Validators.required]
    });

    this.myForm1 = formBuilder.group({
      textbox1: ['', Validators.required]
    });

    this.myForm2 = formBuilder.group({
      textbox1: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.getDailyReportView();
    this.getProfileDetails();
  }

  async getProfileDetails() {

    await this.api.showLoader();

    // const token = await this.userData.getToken();
    // console.log('token', token);
    // this.api.getProfileInfo(token).subscribe((response: any) => {
    //   console.log('getProfileInfo response', response);
    //   if (response && response.data) {
    //     this.user = response.data.user;
    //     this.summary = response.data.summary;
    //     this.attendance = response.data.attendance;
    //     this.api.dismissLoader();
    //   } else if (response.statusCode === 401) {
    //     this.api.clearStorageAndRedirectToLogin(response.message);
    //     this.api.dismissLoader();
    //   }else {
    //     this.api.presentValidateToast(response.message);
    //     this.api.dismissLoader();
    //   }
    // });
  }

  async getDailyReportView() {

    await this.api.showLoader();

    const token = await this.userData.getToken();

    // this.api.getDailyReportsView(token).subscribe((response: any) => {
    //   //console.log('getAttendances response', response);
    //   if (response.status == 1) {
    //     this.dailyReportView = response.data;
    //     console.log(this.dailyReportView);
    //     this.api.dismissLoader();
    //   } else if (response.statusCode === 401) {
    //     this.api.clearStorageAndRedirectToLogin(response.message);
    //     this.api.dismissLoader();
    //   }else {
    //     this.api.presentValidateToast(response.message);
    //     this.api.dismissLoader();
    //   }
    // });
  }


  onSubmitbtn() {
    // console.log('hii');
    this.router.navigate(['update-daily-report']);
  }

  onBack() {
    this.router.navigate(['daily-report']);
  }

  addControl() {
    this.textid++;
    this.myForm.addControl('textbox' + this.textid, new FormControl('', Validators.required));
  }

  removeControl(control) {
    this.myForm.removeControl(control.key);
  }


  addPetrol() {
    this.txt1++;
    this.txt2++;
    this.txt3++;
    this.myForm1.addControl('textbox' + this.txt1, new FormControl('', Validators.required));
    this.myForm1.addControl('textbox' + this.txt2, new FormControl('', Validators.required));
    this.myForm1.addControl('textbox' + this.txt3, new FormControl('', Validators.required));
  }

  removeControl1(control1) {
    this.myForm1.removeControl(control1.key);
  }


  addExpanse() {
    this.tx1++;
    this.tx2++;
    this.myForm2.addControl('textbox' + this.tx1, new FormControl('', Validators.required));
    this.myForm2.addControl('textbox' + this.tx2, new FormControl('', Validators.required));
  }

  removeControl2(control2) {
    this.myForm2.removeControl(control2.key);
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }
}
