import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RaiseTicketPagePageRoutingModule } from './raise-ticket-page-routing.module';

import { RaiseTicketPagePage } from './raise-ticket-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RaiseTicketPagePageRoutingModule
  ],
  declarations: [RaiseTicketPagePage]
})
export class RaiseTicketPagePageModule {}
