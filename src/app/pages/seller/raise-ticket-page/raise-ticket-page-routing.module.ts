import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RaiseTicketPagePage } from './raise-ticket-page.page';

const routes: Routes = [
  {
    path: '',
    component: RaiseTicketPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RaiseTicketPagePageRoutingModule {}
