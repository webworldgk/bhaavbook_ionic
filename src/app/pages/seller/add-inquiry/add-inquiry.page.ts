import { HttpClient, HttpHeaders } from "@angular/common/http";
import { NgForm } from "@angular/forms";
import { Component, OnInit } from "@angular/core";
import { Capacitor } from "@capacitor/core";
import { ActivatedRoute, Router } from "@angular/router";
import { LoadingController, ToastController } from "@ionic/angular";
import { ApiService } from "src/app/service/api.service";
import { UserData } from "../../../providers/user-data";
import { Storage } from "@ionic/storage";

@Component({
  selector: 'app-add-inquiry',
  templateUrl: './add-inquiry.page.html',
  styleUrls: ['./add-inquiry.page.scss'],
})
export class AddInquiryPage implements OnInit {

  code:any;
  remark: any;
  isValidated: boolean;
  from: any;
  to: any;
  Id: any;
  productInfo ={
    id: '',
    product_id:'',
    remarks: '',
    rate: '',
    description: '',
    hsn_description: '',
    category_name: '',
    unit_name:'',
    product_pic: '',
    hsn_code:'',
    item_code:'',
   }
  constructor(
    public router: Router,
    public api: ApiService,
    private http: HttpClient,
    public userData: UserData,
    public storage: Storage,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController
  ) { 
    this.Id = this.route.snapshot.paramMap.get('id');
    console.log(`this.Id`, this.Id);
  }

  ngOnInit() {
  }

    
  submitInquiry(form: NgForm) {
    // console.log(`form`, form);
    this.Id = this.route.snapshot.paramMap.get("id");
    console.log(`this.product_id`, this.Id);
    this.isValidated = true;


    if (this.remark == "") {
      this.api.presentValidateToast(
        "Product Name is required.",
        4000,
        "danger"
      );
      this.isValidated = false;
    }

    if (this.isValidated) {
      this.Id = this.route.snapshot.paramMap.get("id");
      const postParams = new FormData();
      postParams.append('product_id', this.Id);
      postParams.append('remarks', this.remark);
      // postParams.append('inquiry_from',this.from);
      // postParams.append('inquiry_to',this.to);
    

      console.log(`postParams`, postParams);
      this.addInquiry(postParams, form);
    }
  }

  async addInquiry(postParams: any, form: NgForm) {
    if (postParams) {
      await this.api.showLoader("Saving...");

      const token = await this.userData.getToken();
      // console.log('token', token);

      const apiName = `api/v1/inquiry`;
      const apiUrl = this.api.restApiUrl + apiName;
      const httpOptions = {
        headers: new HttpHeaders({ Authorization: token }),
      };

      this.http.post(apiUrl, postParams, httpOptions).subscribe(
        (response: any) => {
          console.log("addInquiry response", response);
          if (response.status == 1) {
            this.api.presentSuccessToast(response.message);
            this.api.dismissLoader();
            this.router.navigateByUrl("inquiry-listing");
            // this.router.navigateByUrl(`/customers-detail/${this.customerId}/2`);
            form.reset();
          } else if (response.statusCode === 401) {
            this.api.clearStorageAndRedirectToLogin(response.message);
            this.api.dismissLoader();
          } else {
            this.api.presentValidateToast(response.message);
            this.api.dismissLoader();
          }
        },
        (error: any) => {
          console.log("error", error.error);
          this.api.presentValidateToast(error.error.message);
          this.api.dismissLoader();
        }
      );
    }
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }


  backToRawCustomerDashbaord() {
    this.router.navigate(["dashboard"]);
  }
  inquirylist(){
    this.router.navigate(["inquiry-listing"]);
  }


}
