import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddInquiryPageRoutingModule } from './add-inquiry-routing.module';

import { AddInquiryPage } from './add-inquiry.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddInquiryPageRoutingModule
  ],
  declarations: [AddInquiryPage]
})
export class AddInquiryPageModule {}
