import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddInquiryPage } from './add-inquiry.page';

const routes: Routes = [
  {
    path: '',
    component: AddInquiryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddInquiryPageRoutingModule {}
