import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../../providers/user-data';
import { AlertController, IonContent } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-seller-dashboard',
  templateUrl: './seller-dashboard.page.html',
  styleUrls: ['./seller-dashboard.page.scss'],
})
export class SellerDashboardPage implements OnInit {
  @ViewChild(IonContent) content: IonContent;
  page: number;
  maximumPages: number;
  product: any;
  isSearchResults: boolean;
  isSearchValidated: boolean;
  productCount: number;

  searchParams = {
    product_name: "",
    category_id: "",
    hsn_code: "",
    hsn_description: "",
    description: "",
    published:"",
    category:"",
    search:"",
  };
  defaultName: any;
  isEnableSearch: boolean;
  customActionSubTypeOptions: any = {
    header: "Choose Sub Type",
  };

  customActionCommonOptions: any = {
    header: "Select...",
  };
  product_name: any;
  category_id:any;
  hsn_code:any;
  hsn_description:any;
  category:any;
  description:any;
  published:any
  leadId: string;
  
  constructor(
    public router: Router,
    public api: ApiService,
    public userData: UserData,
    public alertCtrl: AlertController,
    private http: HttpClient,
    public route: ActivatedRoute,
  ) { 
    this.leadId = this.route.snapshot.paramMap.get('id');
  }

  loadMore(infiniteScroll){
    this.page++;
    this.getProductDetails(infiniteScroll);

    if(this.page === this.maximumPages){
      infiniteScroll.target.disabled = true;
    }
  }

 
  ngOnInit() {
    this.getProductDetails();
    }


    ionViewDidEnter() {
     // this.getDashboardDetails();
      if (localStorage.getItem('refreshContents') == '1') {
        this.getProductDetails();
      }
    }


  async getProductDetails(infiniteScroll?) {

    await this.api.showLoader();

    const token = await this.userData.getToken();
    console.log('token', token)
    this.api.getProductInfo(token, this.page, this.searchParams.product_name,this.searchParams.category_id,this.searchParams.hsn_code,this.searchParams.hsn_description,this.searchParams.category,this.searchParams.description,this.searchParams.published,this.searchParams.search).subscribe((response: any) => {
      console.log('getProductInfo response', response);
      if (response.status === 1) {
        this.product = response.data;
        this.productCount = response.data.length;
        // console.log(this.productCount);
        // this.product =   (this.product && Array.isArray(this.product)) ? this.product.concat(response.data): response.data;

        if(infiniteScroll){
          infiniteScroll.target.complete();
        }
        this.api.dismissLoader();
           // reset in order to not refresh again
           localStorage.setItem('refreshContents', '0');
      } 
      else if (response.statusCode === 401) {
        this.api.clearStorageAndRedirectToLogin(response.message);
        this.api.dismissLoader();

      } else if (response.message === 'Server Error') {
        this.api.serverErrorToast();
        this.api.dismissLoader();

      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }

  async condirmRemoveDailyReport(id: any) {
    const alert = await this.alertCtrl.create({
      header: 'Confirm Remove',
      message: 'Are you sure you want to remove?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: (no) => {
            console.log('Confirm Cancel: ', no);
          }
        }, {
          text: 'Remove',
          handler: () => {
            console.log('Confirm Okay');
            this.removeDailyReport(id);
          }
        }
      ]
    });

    await alert.present();
  }

  async removeDailyReport(id: any) {

    if (id) {

      await this.api.showLoader('Removing...');

      const token = await this.userData.getToken();
      this.api.deleteDailyReport(token, id).subscribe((response: any) => {
        console.log('deleteDailyReport response', response);
        if (response.status == 1) {
          this.api.presentSuccessToast(response.message);
          localStorage.setItem('refreshContents', '1');
          this.getProductDetails();
          this.api.dismissLoader();
        } else if (response.statusCode === 401) {
          this.api.clearStorageAndRedirectToLogin(response.message);
          this.api.dismissLoader();
        } else {
          this.api.presentValidateToast(response.message);
          this.api.dismissLoader();
        }
      });
    }
  }
  async confirmLogout() {
    const alert = await this.alertCtrl.create({
      header: 'Confirm sign out',
      message: 'Are you sure you want to sign out?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: (no) => {
            console.log('Confirm Cancel: ', no);
          }
        }, {
          text: 'Sign Out',
          handler: () => {
            console.log('Confirm Okay');
            this.api.logout();
          }
        }
      ]
    });

    await alert.present();
  }

  doRefresh(event) {
    console.log('Begin async operation');
    this.getProductDetails();
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}
