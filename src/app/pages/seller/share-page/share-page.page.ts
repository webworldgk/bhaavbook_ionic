import { Component, OnInit } from '@angular/core';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';

@Component({
  selector: 'app-share-page',
  templateUrl: './share-page.page.html',
  styleUrls: ['./share-page.page.scss'],
})
export class SharePagePage implements OnInit {

  constructor(private socialSharing: SocialSharing) {}

  ngOnInit(): void {

  }
  sendShare(message, subject, url) {
    this.socialSharing.share(message, subject, null, url);
  }

}
