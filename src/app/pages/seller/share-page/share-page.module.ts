import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SharePagePageRoutingModule } from './share-page-routing.module';

import { SharePagePage } from './share-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharePagePageRoutingModule
  ],
  declarations: [SharePagePage]
})
export class SharePagePageModule {}
