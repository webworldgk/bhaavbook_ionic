import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SharePagePage } from './share-page.page';

const routes: Routes = [
  {
    path: '',
    component: SharePagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SharePagePageRoutingModule {}
