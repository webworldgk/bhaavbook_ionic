


import { HttpClient, HttpHeaders } from "@angular/common/http";
import { NgForm } from "@angular/forms";
import { Component, OnInit } from "@angular/core";
import { Capacitor } from "@capacitor/core";
import { ActivatedRoute, Router } from "@angular/router";
import { LoadingController, ToastController } from "@ionic/angular";
import { ApiService } from "src/app/service/api.service";
import { UserData } from "../../../providers/user-data";
import { Storage } from "@ionic/storage";

@Component({
  selector: 'app-add-order',
  templateUrl: './add-order.page.html',
  styleUrls: ['./add-order.page.scss'],
})
export class AddOrderPage implements OnInit {
  Id: any;
  order_qty: any;
  remarks: any;
  isValidated: boolean;
  data: any;

  constructor(
    public router: Router,
    public api: ApiService,
    private http: HttpClient,
    public userData: UserData,
    public storage: Storage,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController
  ) { 
    this.Id = this.route.snapshot.paramMap.get('id');
    console.log(`this.Id`, this.Id);
  }

  ngOnInit() {
  }

  
  ionViewDidEnter() {
    this.getOrderDetails();
  }

  async getOrderDetails() {

    await this.api.showLoader();

    const token = await this.userData.getToken();
    console.log('token', token)
    this.api.getProductDetail(token, this.Id).subscribe((response: any) => {
      console.log('getOrderDetails response', response);
      if (response.status === 1) {
        this.data = response.data;
       // this.product =   (this.product && Array.isArray(this.product)) ? this.product.concat(response.data): response.data;
       console.log(this.data);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }
    
  submitInquiry(form: NgForm) {
    // console.log(`form`, form);
    this.Id = this.route.snapshot.paramMap.get("id");
    console.log(`this.product_id`, this.Id);
    this.isValidated = true;

    if (this.order_qty == "") {
      this.api.presentValidateToast(
        "Quantity is required.",
        4000,
        "danger"
      );
      this.isValidated = false;
    }

    if (this.remarks == "") {
      this.api.presentValidateToast(
        "remark is required.",
        4000,
        "danger"
      );
      this.isValidated = false;
    }

    if (this.isValidated) {
      this.Id = this.route.snapshot.paramMap.get("id");
      const postParams = new FormData();
      postParams.append('product_id', this.Id);
      postParams.append('order_qty',this.order_qty);
      postParams.append('remarks', this.remarks);

      console.log(`postParams`, postParams);
      this.addOrder(postParams, form);
    }
  }

  async addOrder(postParams: any, form: NgForm) {
    if (postParams) {
      await this.api.showLoader("Saving...");

      const token = await this.userData.getToken();
      // console.log('token', token);

      const apiName = `api/v1/orders`;
      const apiUrl = this.api.restApiUrl + apiName;
      const httpOptions = {
        headers: new HttpHeaders({ Authorization: token }),
      };

      this.http.post(apiUrl, postParams, httpOptions).subscribe(
        (response: any) => {
          console.log("addOrder response", response);
          if (response.status == 1) {
            this.api.presentSuccessToast(response.message);
            this.api.dismissLoader();
            this.router.navigateByUrl("my-order");
            // this.router.navigateByUrl(`/customers-detail/${this.customerId}/2`);
            form.reset();
          } else if (response.statusCode === 401) {
            this.api.clearStorageAndRedirectToLogin(response.message);
            this.api.dismissLoader();
          } else {
            this.api.presentValidateToast(response.message);
            this.api.dismissLoader();
          }
        },
        (error: any) => {
          console.log("error", error.error);
          this.api.presentValidateToast(error.error.message);
          this.api.dismissLoader();
        }
      );
    }
  }
  inquirylist(){
    this.router.navigate(["inquiry-listing"]);
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }
  backToRawCustomerDashbaord() {
    this.router.navigate(["dashboard"]);
  }
  orderlist(){
    this.router.navigate(["my-order"]);
  }

}

