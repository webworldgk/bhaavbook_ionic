import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../../providers/user-data';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertController } from '@ionic/angular';
import { Storage } from "@ionic/storage";

@Component({
  selector: 'app-setting-page',
  templateUrl: './setting-page.page.html',
  styleUrls: ['./setting-page.page.scss'],
})
export class SettingPagePage implements OnInit {

  
  isToggled: boolean;
  isToggleBtnChecked: any;
  presentAlert: any;
  settingdata: any;
  isToggledbutton: string;
  isValidated: boolean;
  details: any;
  defaultName: any;
  convertedBoolValue: any;
  isToggleNotification: any;
  isTogglePrice: any;


  constructor(
    public router: Router,
    public api: ApiService,
    public userData: UserData,
    public route: ActivatedRoute,
    public alertCtrl: AlertController,
    private http: HttpClient,
    public storage: Storage
  ) {
    this.isToggled = false;
   }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.getSettingDetails();
  }
  
  // public notify() {
  //   console.log("Toggled: "+ this.isToggled); 
  // }

  // public notifybutton() {
  //   console.log("Toggled: "+ this.isToggledbutton); 
  // }

  async getSettingDetails() {

    await this.api.showLoader();

    const token = await this.userData.getToken();
    console.log("token", token);
    this.api.getSettingInfo(token).subscribe((response: any) => {
      console.log("getSeetingInfo response", response);
      if (response.status === 1) {
        this.settingdata = response.data;
        console.log(this.settingdata);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }
  
  getToggleModel(id:any){
    if(id==1){
      return 'isToggleNotification';
    }else{
      return 'isTogglePrice';
    }
  }

  async submitStatus(id: any,value: any) {

    console.log('setting',this.settingdata);
    this.convertedBoolValue = value == 1 ? 0 : 1;
	  console.log('value',this.isToggleNotification);
  

   this.isValidated = true;
     
   
   const token = await this.userData.getToken();
   console.log('token', token);
  
   if (this.isValidated) {
    const postParams = {
      setting_id: id,
      set_value: this.convertedBoolValue,
    };
  
     console.log(postParams);
       this.addStatus(postParams);
    }
    
  
  }
  
  async addStatus(postParams: any) {
  
   if (postParams) {
  
     console.log(`postParams`, postParams);
     await this.api.showLoader('Saving...');
  
     const token = await this.userData.getToken();
     // console.log('token', token);
  
     const apiName = `api/v1/settings/update-setting`;
     const apiUrl = this.api.restApiUrl + apiName;
     const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
  
     this.http.post(apiUrl, postParams, httpOptions).subscribe((response: any) => {
       console.log('status response', response);
       if (response.status == 1) {
        //  this.statusData = response.data;
        //  console.log("status data", this.statusData);
         this.api.presentSuccessToast(response.message);
         this.api.dismissLoader();
    
       } else if (response.statusCode === 401) {
         this.api.clearStorageAndRedirectToLogin(response.message);
         this.api.dismissLoader();
       } else {
         this.api.presentValidateToast(response.message);
         this.api.dismissLoader();
       }
     }, (error: any) => {
       console.log('error', error.error);
       this.api.presentValidateToast(error.error.message);
       this.api.dismissLoader();
     });
   }
  }


}
