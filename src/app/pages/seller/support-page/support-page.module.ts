import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SupportPagePageRoutingModule } from './support-page-routing.module';

import { SupportPagePage } from './support-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SupportPagePageRoutingModule
  ],
  declarations: [SupportPagePage]
})
export class SupportPagePageModule {}
