import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FollowingSearchPage } from './following-search.page';

const routes: Routes = [
  {
    path: '',
    component: FollowingSearchPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FollowingSearchPageRoutingModule {}
