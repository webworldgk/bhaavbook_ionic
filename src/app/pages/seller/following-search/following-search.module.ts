import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FollowingSearchPageRoutingModule } from './following-search-routing.module';

import { FollowingSearchPage } from './following-search.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FollowingSearchPageRoutingModule
  ],
  declarations: [FollowingSearchPage]
})
export class FollowingSearchPageModule {}
