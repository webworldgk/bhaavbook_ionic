import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FollowerPandingRequestPageRoutingModule } from './follower-panding-request-routing.module';

import { FollowerPandingRequestPage } from './follower-panding-request.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FollowerPandingRequestPageRoutingModule
  ],
  declarations: [FollowerPandingRequestPage]
})
export class FollowerPandingRequestPageModule {}
