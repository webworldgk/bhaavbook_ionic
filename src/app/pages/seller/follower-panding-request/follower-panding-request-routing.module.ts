import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FollowerPandingRequestPage } from './follower-panding-request.page';

const routes: Routes = [
  {
    path: '',
    component: FollowerPandingRequestPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FollowerPandingRequestPageRoutingModule {}
