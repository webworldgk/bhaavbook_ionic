import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../../providers/user-data';
import { AlertController } from '@ionic/angular';
import { Storage } from "@ionic/storage";

@Component({
  selector: 'app-followup-page',
  templateUrl: './following.page.html',
  styleUrls: ['./following.page.scss'],
})
export class FollowingPage implements OnInit {


  data: any;
  id:any;
  followingCount: number;
 
  constructor(
    public router: Router,
    public api: ApiService,
    public userData: UserData,
    public alertCtrl: AlertController,
    public route: ActivatedRoute,
    public activatedRoute: ActivatedRoute,
    public storage: Storage,
  ) { 
    const userInfo = this.storage.get("userInfo");
    console.log("savecat_userInfo", userInfo);
  }


  ngOnInit() {
  }

  ionViewDidEnter() {
    this.getFollowingDetails();
  }

  async getFollowingDetails() {
    const userInfo = this.storage.get("userInfo");
    console.log("savecat_userInfo", userInfo);
    await this.api.showLoader();

    const token = await this.userData.getToken();
    console.log('token', token)
    this.api.getFollowingInfo(token).subscribe((response: any) => {
      console.log('getFollowingInfo response', response);
      if (response.status === 1) {
        this.data = response.data;
        this.followingCount=response.data.length;
        console.log(this.data);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
      });

  }
  async condirmRemoveDailyReport(id: any) {
    const alert = await this.alertCtrl.create({
      header: "Confirm Unfollow",
      message: "Are you sure you want to unfollow?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: (no) => {
            console.log("Confirm Cancel: ", no);
          },
        },
        {
          text: "unfollow",
          handler: () => {
            console.log("Confirm Okay");
            this.removeDailyReport(id);
          },
        },
      ],
    });

    await alert.present();
  }

  async removeDailyReport(id: any) {
    if (id) {
      await this.api.showLoader("Removing...");

      const token = await this.userData.getToken();
      this.api.deleteFollowing(token, id).subscribe((response: any) => {
        console.log("deleteFollowing response", response);
        if (response.status == 1) {
          this.api.presentSuccessToast(response.message);
          localStorage.setItem("refreshContents", "1");
          this.getFollowingDetails();
          this.api.dismissLoader();
        } else if (response.statusCode === 401) {
          this.api.clearStorageAndRedirectToLogin(response.message);
          this.api.dismissLoader();
        } else {
          this.api.presentValidateToast(response.message);
          this.api.dismissLoader();
        }
      });
    }
  }




}
