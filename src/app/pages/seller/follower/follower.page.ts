import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../../providers/user-data';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-follower',
  templateUrl: './follower.page.html',
  styleUrls: ['./follower.page.scss'],
})
export class FollowerPage implements OnInit {

  data: any;
  follower_id: any;
  followeCount: number;
  status= 1; //status accepted
 
  constructor(
    public router: Router,
    public api: ApiService,
    public userData: UserData,
    public alertCtrl: AlertController,
    public route: ActivatedRoute,
    public activatedRoute: ActivatedRoute,
  ) { 
    this.follower_id = this.route.snapshot.paramMap.get('follower_id');
    console.log("id", this.follower_id);
  }


  ngOnInit() {
  }

  ionViewDidEnter() {
    this.getFollowerDetails();
  }

  async getFollowerDetails() {

    // await this.api.showLoader();

    const token = await this.userData.getToken();
    console.log('token', token)
    this.api.getFollowerInfo(token,this.status).subscribe((response: any) => {
      console.log('getFollowerInfo response', response);
      if (response.status === 1) {
        this.data = response.data;
        this.followeCount=response.data.length;
        console.log(this.data);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
      });

  }
  async condirmRemoveDailyReport(id: any) {
    const alert = await this.alertCtrl.create({
      header: "Confirm Remove",
      message: "Are you sure you want to remove?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: (no) => {
            console.log("Confirm Cancel: ", no);
          },
        },
        {
          text: "Remove",
          handler: () => {
            console.log("Confirm Okay");
            this.removeFollower(id);
          },
        },
      ],
    });

    await alert.present();
  }

  async removeFollower(id: any) {
    if (id) {
      await this.api.showLoader("Removing...");

      const token = await this.userData.getToken();
      this.api.deleteFollower(token, id).subscribe((response: any) => {
        console.log("deleteDailyReport response", response);
        if (response.status == 1) {
          this.api.presentSuccessToast(response.message);
          localStorage.setItem("refreshContents", "1");
          this.getFollowerDetails();
          this.api.dismissLoader();
        } else if (response.statusCode === 401) {
          this.api.clearStorageAndRedirectToLogin(response.message);
          this.api.dismissLoader();
        } else {
          this.api.presentValidateToast(response.message);
          this.api.dismissLoader();
        }
      });
    }
  }


}
