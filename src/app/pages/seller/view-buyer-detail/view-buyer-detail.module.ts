import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewBuyerDetailPageRoutingModule } from './view-buyer-detail-routing.module';

import { ViewBuyerDetailPage } from './view-buyer-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewBuyerDetailPageRoutingModule
  ],
  declarations: [ViewBuyerDetailPage]
})
export class ViewBuyerDetailPageModule {}
