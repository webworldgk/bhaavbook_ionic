import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewBuyerDetailPage } from './view-buyer-detail.page';

const routes: Routes = [
  {
    path: '',
    component: ViewBuyerDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewBuyerDetailPageRoutingModule {}
