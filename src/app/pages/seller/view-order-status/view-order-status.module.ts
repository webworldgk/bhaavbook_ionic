import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewOrderStatusPageRoutingModule } from './view-order-status-routing.module';

import { ViewOrderStatusPage } from './view-order-status.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewOrderStatusPageRoutingModule
  ],
  declarations: [ViewOrderStatusPage]
})
export class ViewOrderStatusPageModule {}
