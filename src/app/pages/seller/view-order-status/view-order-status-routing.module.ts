import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewOrderStatusPage } from './view-order-status.page';

const routes: Routes = [
  {
    path: '',
    component: ViewOrderStatusPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewOrderStatusPageRoutingModule {}
