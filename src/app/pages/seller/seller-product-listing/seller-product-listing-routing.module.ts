import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SellerProductListingPage } from './seller-product-listing.page';

const routes: Routes = [
  {
    path: '',
    component: SellerProductListingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SellerProductListingPageRoutingModule {}
