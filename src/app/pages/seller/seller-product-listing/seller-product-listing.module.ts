import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SellerProductListingPageRoutingModule } from './seller-product-listing-routing.module';

import { SellerProductListingPage } from './seller-product-listing.page';
import { SwiperModule } from 'swiper/angular';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SwiperModule,
    SellerProductListingPageRoutingModule
  ],
  declarations: [SellerProductListingPage]
})
export class SellerProductListingPageModule {}
