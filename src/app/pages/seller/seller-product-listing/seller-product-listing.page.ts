import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../../providers/user-data';
import { AlertController, IonContent } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-seller-product-listing',
  templateUrl: './seller-product-listing.page.html',
  styleUrls: ['./seller-product-listing.page.scss'],
})
export class SellerProductListingPage implements OnInit {
  @ViewChild(IonContent) content: IonContent;
  searchTerm: any;
  searchedCustomers: any;
  searchedCustomerCount: any;
  isSearchValidated: boolean;
  isSearchResults: boolean;
 
  searchParams = {
    search:"",
  };
  isEnableSearch: boolean;
  follower: any;
  productCount: any;

  constructor(
    public router: Router,
    public api: ApiService,
    public userData: UserData,
    public alertCtrl: AlertController,
    private http: HttpClient,
  ) { }


  ngOnInit() {
  }

  ionViewDidEnter() {
    
  }


  scrollToTop() {
    this.content.scrollToTop(400);
  }

  openSearchBox() {
    this.scrollToTop();
    this.isEnableSearch = true;
  }


  hideSearchBox() {
    this.isEnableSearch = false;
    this.isSearchResults = false;
  }


  resetSearchBox() {
    this.searchParams = {
      search:"",
    };
  }
  applySearch(event: any) {
    this.getSearchProduct();
  }

  async getSearchProduct() {
    this.isSearchValidated = true;

    if (
      this.searchParams.search == ""
    ) {
      this.isSearchValidated = false;
    }

    if (this.isSearchValidated === true) {
      this.isSearchResults = true;

      // await this.api.showLoader("Searching...");

      const token = await this.userData.getToken();

      const apiName = `/api/v1/products?common_search=${this.searchParams.search}`;
      const apiUrl = this.api.restApiUrl + apiName;
      const httpOptions = {
        headers: new HttpHeaders({ Authorization: token }),
      };

      console.log(`this.searchParams`, this.searchParams);

      this.http.get(apiUrl, httpOptions).subscribe(
        (response: any) => {
          console.log("getSearchfollower response", response);
          if (response.status === 1) {
            this.follower = response.data;
            this.productCount = response.data.length;
            this.isEnableSearch = false;
            this.api.dismissLoader();
          } else if (response.statusCode === 401) {
            this.api.clearStorageAndRedirectToLogin(response.message);
            this.api.dismissLoader();
          } else {
            this.api.presentValidateToast(response.message);
            this.api.dismissLoader();
          }
        },
        (error: any) => {
          console.log("error", error.error);
          this.api.presentValidateToast(error.error.message);
          this.api.dismissLoader();
        }
      );
    }
  }
  async confirmLogout() {
    const alert = await this.alertCtrl.create({
      header: 'Confirm sign out',
      message: 'Are you sure you want to sign out?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: (no) => {
            console.log('Confirm Cancel: ', no);
          }
        }, {
          text: 'Sign Out',
          handler: () => {
            console.log('Confirm Okay');
            this.api.logout();
          }
        }
      ]
    });

    await alert.present();
  }

  resetSearch() {
    this.resetSearchBox();
    this.hideSearchBox();
    // this.getSearchProduct();
  }


  doRefresh(event) {
    console.log('Begin async operation');
    this.resetSearch();
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }
  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }
}
