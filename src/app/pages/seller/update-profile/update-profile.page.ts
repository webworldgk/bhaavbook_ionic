import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Component, OnInit, } from '@angular/core';
import { Capacitor } from '@capacitor/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../../providers/user-data';
import { Storage } from '@ionic/storage';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.page.html',
  styleUrls: ['./update-profile.page.scss'],
})
export class UpdateProfilePage implements OnInit {

  
  profilePic: any;
  aadharCard: any;
  old_joining_date: any;
  isValidated: boolean;
  Id: any;
  id: any;

  profileInfo = {
    id: '',
    first_name: '',
    last_name: '',
    email_address: '',
    joining_date: '',
    mobile_number: '',
    alternate_number: '',
    city_name: '',
    department: '',
    product_pic: '',
    aadhar_card: '',
    pan_card: '',
    profile_pic_preview: '',
    website:'',
    whatsapp:'',
    gst_no:'',
    aadhar_number:'',
    pan_number:'',
    address:'',
    area:'',
    pincode:'',
    state_id:'',
    city_id:'',
    state:'',
    city:'',
  };

  customActionCommonOptions: any = {
    header: 'Select...'
  };
  productpic: any;

  async prepareMasters() {
    if (this.api.businessTypeList.length == 0) {
      const storeVariables = true;
      this.api.doStoreMasters(storeVariables);
    }
  }
  data: any;
  panCard: any;

  constructor(
    public router: Router,
    public api: ApiService,
    private http: HttpClient,
    public userData: UserData,
    public storage: Storage,
    public route: ActivatedRoute,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private photoViewer: PhotoViewer,
  ) {
    this.Id = this.route.snapshot.paramMap.get('id');
    console.log(`this.Id`, this.Id);
  }

  ngOnInit() { }

 
  ionViewDidEnter() {
    // this.getUserView();
    this.populateUserInfo();
    this.prepareMasters();
  }


  async populateUserInfo() {
    await this.storage.get('userInfo').then((userData) => {
      console.log(`userData`, userData);
      if (userData) {

        this.profileInfo.first_name = userData.first_name;
        this.profileInfo.last_name = userData.last_name;
        this.profileInfo.email_address = userData.email;
        this.profileInfo.mobile_number = userData.mobile_number;
        this.profileInfo.address = userData.address;
        this.profileInfo.aadhar_number = userData.aadhar_number;
        this.profileInfo.pan_number = userData.pan_no;
        this.profileInfo.gst_no = userData.gst_no;
        this.profileInfo.whatsapp = userData.whatsapp;
        this.profileInfo.website = userData.website;
        this.profileInfo.address = userData.address;
        this.profileInfo.area = userData.area;
        this.profileInfo.pincode = userData.pincode;
        this.profileInfo.profile_pic_preview = userData.product_pic;
       
        // this.profileInfo.state = this.userData.state_id.toString();
        // this.profileInfo.city = this.userData.city_id.toString();
      }
    });
  }

   submitUpdateProfile(form: NgForm) {
    // console.log(`form`, form);
    
    this.isValidated = true;

    if (!this.profileInfo.first_name) {
      this.api.presentValidateToast('First Name is required.', 4000, 'danger');
      this.isValidated = false;
    }

    if (!this.profileInfo.last_name) {
      this.api.presentValidateToast('Last Name is required.', 4000, 'danger');
      this.isValidated = false;
    }

    if (this.isValidated) {

      console.log(`this.profileInfo`, this.profileInfo);

      const postParams = new FormData();
      postParams.append("first_name", this.profileInfo.first_name);
      postParams.append("last_name", this.profileInfo.last_name);
      postParams.append("mobile_number", this.profileInfo.mobile_number);
      postParams.append("address", this.profileInfo.address);
      postParams.append("location", this.profileInfo.city_name);
      postParams.append("aadhar_number", this.profileInfo.aadhar_number);
      postParams.append("pan_no", this.profileInfo.pan_number);
      postParams.append("area", this.profileInfo.area);
      postParams.append("pincode", this.profileInfo.pincode);
      postParams.append("whatsapp", this.profileInfo.whatsapp);
      postParams.append("gst_no", this.profileInfo.gst_no);
      postParams.append("website", this.profileInfo.website);
      postParams.append("email", this.profileInfo.email_address);
      postParams.append("state_id", this.profileInfo.state);
      postParams.append("city_id", this.profileInfo.city);      


      if (this.productpic) {
        postParams.append("product_pic", this.productpic, this.productpic?.name);
      }

      this.updateProfile(postParams);
    }

  }

  async updateProfile(postParams: any) {

    const userInfo = await this.storage.get("userInfo");
    console.log("savecat_userInfo", userInfo);
    if (postParams) {

      await this.api.showLoader('Updating...');

      const token = await this.userData.getToken();
      // console.log('token', token);

      const apiName = `api/v1/auth/update-profile`;
      const apiUrl = this.api.restApiUrl + apiName;
      const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

      this.http.post(apiUrl, postParams, httpOptions).subscribe((response: any) => {
        console.log('updateProfile response', response);
        if (response.status == 1) {
          this.api.presentSuccessToast(response.message);
          this.api.dismissLoader();

          // reset the user stored information
          this.userData.setUserInfo(response.data);
          this.api.userName = response.data?.first_name + ' ' + response.data?.last_name;
          this.api.email = response.data.email;
          this.api.userPicture = response.data.picture;
          this.router.navigateByUrl(`/seller-profile`);

        } else if (response.statusCode === 401) {
          this.api.clearStorageAndRedirectToLogin(response.message);
          this.api.dismissLoader();
        } else {
          this.api.presentValidateToast(response.message);
          this.api.dismissLoader();
        }
      }, (error: any) => {
        console.log('error', error.error);
        this.api.presentValidateToast(error.error.message);
        this.api.dismissLoader();
      });
    }
  }

  onProfileChange(fileEvent: any) {
    if (fileEvent.target.files[0]) {
      this.productpic = fileEvent.target.files[0];
      const reader = new FileReader();
      reader.onload = (event: any) => {
        this.profileInfo.profile_pic_preview = event.target.result;
      }
      reader.readAsDataURL(fileEvent.target.files[0]);
      console.log(`this.product_pic`, this.productpic);
    }
  }

  previewImage(photoCode: number) {
    console.log(`photoCode`, photoCode);
    if (Capacitor.isNativePlatform()) {
      if (photoCode === 1 && this.profileInfo.profile_pic_preview) {
        this.photoViewer.show(this.profileInfo.profile_pic_preview, 'Image', { share: false });
      }

    }
  }

  async ionViewDidLeave() {
    if (this.api.loading) {
      await this.api.dismissLoader();
    }
  }

}
