import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FollowerSearchPageRoutingModule } from './follower-search-routing.module';

import { FollowerSearchPage } from './follower-search.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FollowerSearchPageRoutingModule
  ],
  declarations: [FollowerSearchPage]
})
export class FollowerSearchPageModule {}
