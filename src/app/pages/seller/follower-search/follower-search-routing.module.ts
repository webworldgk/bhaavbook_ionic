import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FollowerSearchPage } from './follower-search.page';

const routes: Routes = [
  {
    path: '',
    component: FollowerSearchPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FollowerSearchPageRoutingModule {}
