import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { UserData } from '../../../providers/user-data';
import { AlertController } from '@ionic/angular';
import { NgForm } from "@angular/forms";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Storage } from "@ionic/storage";


@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.page.html',
  styleUrls: ['./product-list.page.scss'],
})
export class ProductListPage implements OnInit {
  public show:boolean = false;
  public buttonName:any = 'Show';

  product: any;
  data: any;
  leadId: string;
  id:any;

  disabled = false;
  status_id: any;

  bloquear: boolean = false;
  isValidated = false;
  isenabled: boolean = true;

  addstatus = {
    category: "",
    statusList: {},
    code: "",
    status_update_remarks: "",
  };
  customActionSubTypeOptions: any = {
    header: "Choose status",
  };

  customActionCommonOptions: any = {
    header: "Choose Unit",
  };
  defaultName: any;
  statusList:{};
  unitList:{};
  
  constructor(
    public router: Router,
    public api: ApiService,
    public userData: UserData,
    public route: ActivatedRoute,
    public alertCtrl: AlertController,
    private http: HttpClient,
    public storage: Storage,
  ) { 
    this.id = this.route.snapshot.paramMap.get('id');
  }

  // loadMore(infiniteScroll){
  //   this.page++;
  //   this.getOrderDetails(infiniteScroll);

  //   if(this.page === this.maximumPages){
  //     infiniteScroll.target.disabled = true;
  //   }
  // }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.prepareMasters();
    this.getOrderDetails();
  }

  async prepareMasters() {
    if (this.api.statusList.length == 0) {
      const storeVariables = true;
      this.api.doStoreMasters(storeVariables);
    }
    if (this.api.unitList.length == 0) {
      const storeVariables = true;
      this.api.doStoreMasters(storeVariables);
    }
  }

  async getOrderDetails() {

    await this.api.showLoader();

    const token = await this.userData.getToken();
    console.log('token', token)
    this.api.getProductDetail(token, this.id).subscribe((response: any) => {
      console.log('getOrderDetails response', response);
      if (response.status === 1) {
        this.data = response.data;
       // this.product =   (this.product && Array.isArray(this.product)) ? this.product.concat(response.data): response.data;
       console.log(this.data);
        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }
  async submitFollowUp(form: NgForm) {
 
    const userInfo = await this.storage.get("userInfo");
     console.log("savecat_userInfo", userInfo);
     this.isValidated = true;
 
     if (this.addstatus.statusList == "") {
       this.api.presentValidateToast("status is required.", 4000, "danger");
       this.isValidated = false;
     }
 
     if (this.isValidated) {
       const postParams = {
         order_id: this.id,
         status_id: this.addstatus.statusList
           ? this.addstatus.statusList
           : null,
           status_update_remarks: this.addstatus.status_update_remarks
           ? this.addstatus.status_update_remarks
           : null,
       };
 
       console.log(`postParams`, postParams);
       this.addFollowUp(postParams, form);
     }
     this.disabled = true;
   }
 
   async addFollowUp(postParams: any, form: NgForm) {
     if (postParams) {
       await this.api.showLoader("Saving...");
 
       const token = await this.userData.getToken();
       // console.log('token', token);
 
       
       const apiName = `api/v1/orders/updateorderstatus`;
       const apiUrl = this.api.restApiUrl + apiName;
       const httpOptions = {
         headers: new HttpHeaders({ Authorization: token }),
       };
 
       this.http.post(apiUrl, postParams, httpOptions).subscribe(
         (response: any) => {
           console.log("addFollowUp response", response);
           if (response.status == 1) {
             this.api.presentSuccessToast(response.message);
             this.api.dismissLoader();
          
             this.router.navigateByUrl(`/order-list`);
             form.reset();
           } else if (response.statusCode === 401) {
             this.api.clearStorageAndRedirectToLogin(response.message);
             this.api.dismissLoader();
           } else {
             this.api.presentValidateToast(response.message);
             this.api.dismissLoader();
           }
         },
         (error: any) => {
           console.log("error", error.error);
           this.api.presentValidateToast(error.error.message);
           this.api.dismissLoader();
         }
       );
     }
   }
 
   toggle() {
    this.show = !this.show;

    // CHANGE THE NAME OF THE BUTTON.
    if(this.show)  
      this.buttonName = "Hide";
    else
      this.buttonName = "Show";
  }
  changeSubType(event: any) {
    const selectedSubType = event.detail.value;
    console.log(`event.detail.value`, event.detail.value);
    this.api.statusList.find((item) => {
      if (selectedSubType > 1 && item.id == selectedSubType) {
        this.defaultName = item.name;
      }
    });
  }


}
