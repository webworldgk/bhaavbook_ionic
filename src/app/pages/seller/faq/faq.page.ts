import { Component, OnInit, ViewChild } from '@angular/core';
import { IonAccordionGroup } from '@ionic/angular';
import { ActivatedRoute, Router } from "@angular/router";
import { ApiService } from "src/app/service/api.service";
import { UserData } from "../../../providers/user-data";
import { AlertController } from "@ionic/angular";
import { NgForm } from "@angular/forms";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Storage } from "@ionic/storage";


@Component({
  selector: 'app-faq',
  templateUrl: './faq.page.html',
  styleUrls: ['./faq.page.scss'],
})
export class FaqPage implements OnInit {
  @ViewChild(IonAccordionGroup, { static: true }) accordionGroup: IonAccordionGroup;

  data1: any;
  product: any;
  productCount: number;

  constructor(
    public router: Router,
    public api: ApiService,
    public userData: UserData,
    public route: ActivatedRoute,
    public alertCtrl: AlertController,
    private http: HttpClient,
    public storage: Storage
  ) {}

  ngOnInit() {}

  ionViewDidEnter() {
    this.getFaqDetails();
  }
  async getFaqDetails() {

    await this.api.showLoader();

    const token = await this.userData.getToken();
    console.log("token", token);
    this.api.getFaqInfo(token).subscribe((response: any) => {
      console.log("getFaqInfo response", response);
      if (response && response.data) {
        this.product = response.data;
        this.productCount = response.data.length;
        console.log(this.product);
        console.log(this.productCount);
            // this.meta = response.meta;

        this.api.dismissLoader();
      } else {
        this.api.presentValidateToast(response.message);
        this.api.dismissLoader();
      }
    });
  }

}
