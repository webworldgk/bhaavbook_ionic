import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InquiryListingPage } from './inquiry-listing.page';

const routes: Routes = [
  {
    path: '',
    component: InquiryListingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InquiryListingPageRoutingModule {}
