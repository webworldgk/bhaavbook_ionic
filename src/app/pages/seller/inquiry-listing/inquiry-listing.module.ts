import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InquiryListingPageRoutingModule } from './inquiry-listing-routing.module';

import { InquiryListingPage } from './inquiry-listing.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InquiryListingPageRoutingModule
  ],
  declarations: [InquiryListingPage]
})
export class InquiryListingPageModule {}
