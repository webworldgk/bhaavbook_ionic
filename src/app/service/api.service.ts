import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { LoadingController, ToastController } from "@ionic/angular";
import { Storage } from "@ionic/storage";
import { Observable, throwError } from "rxjs";
import { map } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { UserOptions } from "../interfaces/user-options";
import { UserData } from "../providers/user-data";
import { AppVersion } from "@awesome-cordova-plugins/app-version/ngx";
import { Capacitor } from "@capacitor/core";

@Injectable({
  providedIn: "root",
})
export class ApiService {
  loading: any;
  restApiUrl: string;
  httpOptions: any;

  userName: string;
  userRole: string;
  email: string;
  userPicture: string;
  userType: string;
  mobile_number: string;
  establishdate: string;
  address: string;
  userTypeName: string;


  subTypes: any[] = [];
  outcomeList: any[] = [];
  showroomList: any[] = [];
  priorityList: any[] = [];
  leadSourceList: any[] = [];
  projectTypeList: any[] = [];
  marketingPersonList: any[] = [];

  userid: string;
  appMenuPages: any[] = [];
  userTypeapp: any;

  // email: string;
  unitList: any[] = [];
  cityList: any[] = [];
  stateList: any[] = [];
  statusList: any[] = [];
  categoryList: any[] = [];
  packageTypeList: any[] = [];
  businessTypeList: any[] = [];
  supportTicketType: any[] = [];
 
  constructor(
    private router: Router,
    private http: HttpClient,
    private storage: Storage,
    private userData: UserData,
    private appVersion: AppVersion,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController
  ) {
    this.startIonicStorage();
    this.restApiUrl = environment.apiUrl;
    // this.restApiUrl = environment.liveApiUrl;
    this.prepareHttpOptions();
    //this.prepareSavedMenuPages();
  }

  async startIonicStorage() {
    await this.storage.create();
  }

  async prepareHttpOptions() {
    this.httpOptions = {
      headers: new HttpHeaders({
        Authorization: await this.userData.getToken(),
      }),
    };
  }

  // async prepareSavedMenuPages() {
  //   if (this.appMenuPages.length == 0) {
  //     if (await this.userData.isLoggedIn()) {
  //       const localMenuPages = await this.userData.getMobilePermission();
  //       if (localMenuPages) {
  //         this.appMenuPages = localMenuPages.reduce((previousList, item) => {
  //           if (item.basemenu == 1) {
  //             let newNode = {
  //               title: item.name,
  //               url: '/' + item.url,
  //               icon: item.icon
  //             };
  //             previousList.push(newNode);
  //           }
  //           return previousList;
  //         }, []);
  //       }
  //       // console.log(`localMenuPages`, localMenuPages);
  //       // console.log(`this.appMenuPages`, this.appMenuPages);
  //     }
  //     // console.log(`this.appMenuPages.length`, this.appMenuPages.length);
  //   }
  // }

  public perfomLogin(userLogin: UserOptions): Observable<any> {
    const postData = new FormData();
    postData.append("loginType", userLogin.loginType.toString());
    postData.append("mobile_number", userLogin.mobile_number);
    postData.append("password", userLogin.password);
    postData.append("mobilePin", userLogin.mobilePin);
    postData.append("deviceToken", userLogin.deviceToken);
    // console.log(userLogin);

    const apiName = `api/v1/auth/login`;
    const apiUrl = this.restApiUrl + apiName;

    return this.http
      .post(apiUrl, postData, this.httpOptions)
      .pipe(map((response: any) => response));
  }

  // public getDashboardInfo(token: any): Observable<any> {

  //   const apiName = `api/v1/users/2`;
  //   const apiUrl = this.restApiUrl + apiName;
  //   const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

  //   return this.http
  //     .get(apiUrl, httpOptions)
  //     .pipe(map((response: any) => response));
  // }

  public getAttendances(
    token: string,
    startDate: string,
    endDate: string,
    status: any
  ): Observable<any> {
    const apiName = `api/v1/attendances`;
    const apiUrl =
      this.restApiUrl +
      apiName +
      `?status=${status}&startdate=${startDate}&enddate=${endDate}`;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    // console.log('apiUrl', apiUrl);

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  public getAttendanceDetails(
    token: any,
    attendanceId: number
  ): Observable<any> {
    const apiName = `api/v1/attendances`;
    const apiUrl = this.restApiUrl + apiName + `/${attendanceId}`;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    // console.log('apiUrl', apiUrl);

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  /**
   * @vishakha
   * 05-10-2021
   * @param token  - Authentication token
   *
   * @returns
   */
  public getDailyReports(token: string): Observable<any> {
    const apiName = `api/v1/daily_reports`;
    const apiUrl = this.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    // console.log('apiUrl', apiUrl);

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  public getDailyReport(token: string, attendanceId: any): Observable<any> {
    const apiName = `api/v1/daily_reports/${attendanceId}`;
    const apiUrl = this.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    // console.log('apiUrl', apiUrl);

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  // public getDailyReportsView(token: any): Observable<any> {
  //   const apiName = `api/v1/products/1`;
  //   const apiUrl = this.restApiUrl + apiName;

  //   return this.http
  //     .get(apiUrl, {
  //       headers: new HttpHeaders({
  //         Authorization: token,
  //       }),
  //     })
  //     .pipe(map((response: any) => response));
  // }

  public getDailyReportsUpdate(token: any): Observable<any> {
    const apiName = `api/v1/products/1`;
    const apiUrl = this.restApiUrl + apiName;

    return this.http
      .get(apiUrl, {
        headers: new HttpHeaders({
          Authorization: token,
        }),
      })
      .pipe(map((response: any) => response));
  }

  // 2-2-2022

  public getProductUpdate(token: any): Observable<any> {
    const apiName = `api/v1/products`;
    const apiUrl = this.restApiUrl + apiName;

    return this.http
      .get(apiUrl, {
        headers: new HttpHeaders({
          Authorization: token,
        }),
      })
      .pipe(map((response: any) => response));
  }

  async logout() {
    const loading = await this.loadingCtrl.create({
      message: "Logging out...",
    });

    await loading.present();

    const apiName = `api/v1/auth/logout`;
    const apiUrl = this.restApiUrl + apiName;
    const token = await this.userData.getToken();
    const headers = { headers: new HttpHeaders({ Authorization: token }) };

    this.http.post(apiUrl, {}, headers).subscribe(
      (response: any) => {
        console.log("perfomLogout response", response);
        if (response.status == 1) {
          this.presentToast(response.message, 4000, "success");
          this.userData.logout().then(() => {
            return this.router.navigateByUrl("/app-login");
          });
          loading.dismiss();
        }
      },
      (error: any) => {
        console.log("error", error.error);
        const errorMessage = error.error.message
          ? error.error.message
          : "Error while requesting for Logout";
        this.presentToast(errorMessage);
        loading.dismiss();
      }
    );
  }

  async presentToast(
    messageStr: string,
    durationMSec = 4000,
    cssClass = "danger"
  ) {
    const toast = await this.toastCtrl.create({
      message: messageStr,
      duration: durationMSec,
      color: cssClass,
    });

    await toast.present();
  }

  async presentTopToast(
    messageStr: string,
    durationMSec = 4000,
    cssClass = "danger"
  ) {
    const toast = await this.toastCtrl.create({
      position: "top",
      message: messageStr,
      duration: durationMSec,
      color: cssClass,
      buttons: [
        {
          text: "OK",
          role: "cancel",
          handler: () => {
            // console.log('Cancel clicked');
          },
        },
      ],
    });

    await toast.present();
  }

  public punchInOut(token: any, postParams: any): Observable<any> {
    const postData = new FormData();
    postData.append("punch_type", postParams.punch_type);
    postData.append("location", postParams.location);
    postData.append("latitude", postParams.latitude);
    postData.append("longitude", postParams.longitude);
    if (postParams?.selfy_pic) {
      postParams = { ...postParams, ...{ name: postParams.selfy_name } };
      postData.append(
        "selfy_pic",
        postParams?.selfy_pic,
        postParams.selfy_name
      );
    }

    const apiName = `api/v1/attendances/punch`;
    const apiUrl = this.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

    return this.http
      .post(apiUrl, postData, httpOptions)
      .pipe(map((response: any) => response));
  }

  /**
   * @GK
   * Date:23-OCT-2021
   * @param token -- token for authentication
   * @returns
   */

  public getCustomerDashboard(token: any): Observable<any> {
    const apiName = `api/v1/raw_customers/dashboard`;
    const apiUrl = this.restApiUrl + apiName;

    return this.http
      .get(apiUrl, {
        headers: new HttpHeaders({
          Authorization: token,
        }),
      })
      .pipe(map((response: any) => response));
  }

  /**
   * @GK
   * Date: 23-10-2021
   * Get the Raw Customer Listing Data
   * @param token
   * @returns
   */
  public getCustomerListing(token: any, postParams: any): Observable<any> {
    const apiName = `api/v1/raw_customers`;
    const startDate = postParams?.dStartDate ? postParams.dStartDate : "";
    const endDate = postParams?.dEndDate ? postParams.dEndDate : "";
    const apiUrl =
      this.restApiUrl +
      apiName +
      `?customerSubType=${postParams?.customerSubType}&startdate=${startDate}&enddate=${endDate}`;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    // console.log('apiUrl', apiUrl);

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  /**
   * @gk
   * 30-10-2021
   * Get details of Raw Customer
   * @param token
   * @param rawCustomerId
   * @returns
   */
  public getRawCustomerDetails(
    token: any,
    rawCustomerId: number
  ): Observable<any> {
    const apiName = `api/v1/raw_customers`;
    const apiUrl = this.restApiUrl + apiName + `/${rawCustomerId}`;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    // console.log('apiUrl', apiUrl);

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  /**
   * @gk
   * 30-10-2021
   * Get details of Raw Customer Follow Up
   * @param token
   * @param rawCustomerId
   * @returns
   */
  public getRawCustomerFollowUp(
    token: any,
    rawCustomerId: number,
    startDate: string,
    endDate: string
  ): Observable<any> {
    const apiName = `api/v1/raw_customers_followup`;
    const apiUrl =
      this.restApiUrl +
      apiName +
      `?customer_id=${rawCustomerId}&followupfor=3&startdate=${startDate}&enddate=${endDate}`;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    // console.log('apiUrl', apiUrl);

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  /**
   * @gk
   * 30-10-2021
   * Get details of Raw Customer Reminder
   * @param token
   * @param rawCustomerId
   * @returns
   */
  public getRawCustomerReminder(
    token: any,
    rawCustomerId: number,
    startDate: string,
    endDate: string
  ): Observable<any> {
    const apiName = `api/v1/raw_customers_reminder`;
    const apiUrl =
      this.restApiUrl +
      apiName +
      `?customer_id=${rawCustomerId}&reminderfor=3&startdate=${startDate}&enddate=${endDate}`;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    // console.log('apiUrl', apiUrl);

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  /**
   * @GK
   * Date:31-OCT-2021
   * @param token -- token for authentication
   * @returns
   */
  public getLeadDashboard(token: any): Observable<any> {
    const apiName = `api/v1/leads/dashboard`;
    const apiUrl = this.restApiUrl + apiName;

    return this.http
      .get(apiUrl, {
        headers: new HttpHeaders({
          Authorization: token,
        }),
      })
      .pipe(map((response: any) => response));
  }

  /**
   * @GK
   * Date: 31-10-2021
   * Get the Lead Listing Data
   * @param token
   * @returns
   */
  public getLeadListing(
    token: any,
    leadStatus: any,
    startDate: string,
    endDate: string
  ): Observable<any> {
    const apiName = `api/v1/leads`;
    const apiUrl =
      this.restApiUrl +
      apiName +
      `?leadStatus=${leadStatus}&startdate=${startDate}&enddate=${endDate}`;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    // console.log('apiUrl', apiUrl);

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  async doStoreMasters(storeVariables: boolean = false) {
    let isMasterAvailable = true;
    const mastersList = await this.userData.getMasters();

    if (mastersList) {
      // console.log('mastersList', mastersList);
      await this.refreshMastersFromStorage(mastersList);

      if (this.businessTypeList.length == 0) {
        isMasterAvailable = false;
      }
    }

    // console.log('isMasterAvailable', isMasterAvailable);
    if (storeVariables === true || isMasterAvailable === false) {
      const token = await this.userData.getToken();
      this.getMasters(token).subscribe((response: any) => {
        console.log('getMasters response', response);
        if (response.status === 1) {
          if (response && response.data) {
            this.userData.saveMasters(response.data);
            this.refreshMastersFromStorage(response.data);
              if (this.cityList.length == 0) {
                for (const [inputId, inputName] of Object.entries(
                  response.data?.cities
                )) {
                  this.cityList.push({ id: inputId, name: inputName });
                }
              }
              if (this.stateList.length == 0) {
                for (const [inputId, inputName] of Object.entries(
                  response.data?.states
                )) {
                  this.stateList.push({ id: inputId, name: inputName });
                }
              }
              if (this.categoryList.length == 0) {
                for (const [inputId, inputName] of Object.entries(
                  response.data?.categories
                )) {
                  this.categoryList.push({ id: inputId, name: inputName });
                }
              }
              if (this.unitList.length == 0) {
                for (const [inputId, inputName] of Object.entries(
                  response.data?.units
                )) {
                  this.unitList.push({ id: inputId, name: inputName });
                }
              }
              if (this.statusList.length == 0) {
                for (const [inputId, inputName] of Object.entries(
                  response.data?.orderStatus
                )) {
                  this.statusList.push({ id: inputId, name: inputName });
                }
              }
              if (this.supportTicketType.length == 0) {
                for (const [inputId, inputName] of Object.entries(
                  response.data?.supportTicketTypeId
                )) {
                  this.supportTicketType.push({ id: inputId, name: inputName });
                }
              }
              if (this.businessTypeList.length == 0) {
                for (const [inputId, inputName] of Object.entries(
                  response.data?.businesstype
                )) {
                  this.businessTypeList.push({ id: inputId, name: inputName });
                }
              }
              if (this.packageTypeList.length == 0) {
                for (const [inputId, inputName] of Object.entries(
                  response.data?.package
                )) {
                  this.packageTypeList.push({ id: inputId, name: inputName });
                }
              }
          }
        }
      });
    }
  }

  public getMasters(token: any): Observable<any> {

    const apiName = `api/v1/auth/register/masters`;
    const apiUrl = this.restApiUrl + apiName;
    const httpOptions = {
      headers: new HttpHeaders({ Authorization: token ? token : "" }),
    };

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  async getStart() {
    const token = await this.userData.getToken();
    this.validateToken(token).subscribe((response: any) => {
      // console.log('validateToken response', response);
      if (response.status == 0) {
        this.clearStorageAndRedirectToLogin(response.message);
      }
    });
  }

  public storeMasters(token: any): Observable<any> {
        const apiName = `api/v1/auth/register/masters`;
        const apiUrl = this.restApiUrl + apiName;
        const httpOptions = {
          headers: new HttpHeaders({ Authorization: token ? token : "" }),
        };
    
        return this.http
          .get(apiUrl, httpOptions)
          .pipe(map((response: any) => response));
      }

  clearStorageAndRedirectToLogin(message: string) {
    if (message) {
      this.presentTopToast(message);
      this.userData.logout().then(() => {
        return this.router.navigateByUrl("/app-login");
      });
    }
  }

  public validateToken(token: any): Observable<any> {
    const apiName = `api/v1/auth/authenticate`;
    const apiUrl = this.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  /**
   * @kuldip
   * 08-11-2021
   * Get details of lead
   * @param token
   * @param leadId
   * @returns
   */
  public getLeadDetails(token: any, leadId: number): Observable<any> {
    const apiName = `api/v1/leads`;
    const apiUrl = this.restApiUrl + apiName + `/${leadId}`;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    // console.log('apiUrl', apiUrl);

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  public getLeadFollowUp(
    token: any,
    leadId: number,
    startDate: string,
    endDate: string
  ): Observable<any> {
    const apiName = `api/v1/leads_followup`;
    const apiUrl =
      this.restApiUrl +
      apiName +
      `?lead_id=${leadId}&followupfor=3&startdate=${startDate}&enddate=${endDate}`;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    // console.log('apiUrl', apiUrl);

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  /**
   * @kuldip
   * 08-11-2021
   * Get details of Raw Customer Reminder
   * @param token
   * @param rawCustomerId
   * @returns
   */
  public getLeadReminder(
    token: any,
    leadId: number,
    startDate: string,
    endDate: string
  ): Observable<any> {
    const apiName = `api/v1/leads_reminder`;
    const apiUrl =
      this.restApiUrl +
      apiName +
      `?lead_id=${leadId}&reminderfor=3&startdate=${startDate}&enddate=${endDate}`;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    // console.log('apiUrl', apiUrl);

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  /**
   * @kuldip
   * 08-11-2021
   * Get details of lead associates
   * @param token
   * @param rawCustomerId
   * @returns
   */
  public getLeadAssociates(
    token: any,
    leadId: number,
    startDate: string,
    endDate: string
  ): Observable<any> {
    const apiName = `api/v1/leadassociates`;
    const apiUrl =
      this.restApiUrl +
      apiName +
      `?lead_id=${leadId}&startdate=${startDate}&enddate=${endDate}`;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    // console.log('apiUrl', apiUrl);

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  public getLeadGallery(
    token: any,
    leadId: number,
    startDate: string,
    endDate: string
  ): Observable<any> {
    const apiName = `api/v1/leadgalleries`;
    const apiUrl =
      this.restApiUrl +
      apiName +
      `?lead_id=${leadId}&startdate=${startDate}&enddate=${endDate}`;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    // console.log('apiUrl', apiUrl);

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  public getLeadNotes(
    token: any,
    leadId: number,
    startDate: string,
    endDate: string
  ): Observable<any> {
    const apiName = `api/v1/leadnotes`;
    const apiUrl =
      this.restApiUrl +
      apiName +
      `?lead_id=${leadId}&startdate=${startDate}&enddate=${endDate}`;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    // console.log('apiUrl', apiUrl);

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  public getUniqueCustomerCode(token: any): Observable<any> {
    const apiName = `api/v1/raw_customers/uniqueCustomerCode`;
    const apiUrl = this.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    // console.log('apiUrl', apiUrl);

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  async presentSuccessToast(messageStr: string, durationMSec = 5000) {
    const toast = await this.toastCtrl.create({
      header: "Success",
      message: messageStr,
      duration: durationMSec,
      animated: true,
      position: "top",
      color: "success",
      buttons: [
        {
          text: "ok",
          role: "cancel",
          handler: () => {
            console.log("ok clicked");
          },
        },
      ],
    });

    await toast.present();

    const { role } = await toast.onDidDismiss();
    // console.log('onDidDismiss resolved with role', role);
  }

  async presentValidateToast(
    messageStr: string,
    durationMSec = 4000,
    cssClass = "dark"
  ) {
    const toast = await this.toastCtrl.create({
      message: messageStr,
      duration: durationMSec,
      color: cssClass,
      position: "top",
      buttons: [
        {
          text: "ok",
          role: "cancel",
          handler: () => {
            console.log("ok clicked");
          },
        },
      ],
    });

    await toast.present();
  }

  async serverErrorToast() {
    const toast = await this.toastCtrl.create({
      message: "Internal Server Error, Please contact to Administrator.",
      duration: 4000,
      color: "danger",
      buttons: [
        {
          text: "ok",
          role: "cancel",
          handler: () => {
            console.log("ok clicked");
          },
        },
      ],
    });

    await toast.present();
  }

  /**
   * @GK
   * Date: 16-11-2021
   * Get the Raw Customer Reminder Listing Data
   * @param token
   * @returns
   */
  public getCustomerReminderListings(
    token: any,
    reminderFor: number,
    startDate: string,
    endDate: string
  ): Observable<any> {
    const apiName = `api/v1/raw_customers_reminder`;
    const apiUrl =
      this.restApiUrl +
      apiName +
      `?reminderfor=${reminderFor}&startdate=${startDate}&enddate=${endDate}`;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    // console.log('apiUrl', apiUrl);

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  /**
   * @GK
   * Date: 16-11-2021
   * Get the Raw Customer Follow Up Listings
   * @param token
   * @returns
   */
  public getCustomerFollowUpListings(
    token: any,
    reminderFor: number,
    startDate: string,
    endDate: string
  ): Observable<any> {
    const apiName = `api/v1/raw_customers_followup`;
    const apiUrl =
      this.restApiUrl +
      apiName +
      `?followupfor=${reminderFor}&startdate=${startDate}&enddate=${endDate}`;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    // console.log('apiUrl', apiUrl);

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  /**
   * @GK
   * Date: 16-11-2021
   * Get the Lead Reminder Listing Data
   * @param token
   * @returns
   */
  public getLeadReminderListings(
    token: any,
    reminderFor: number,
    startDate: string,
    endDate: string
  ): Observable<any> {
    const apiName = `api/v1/leads_reminder`;
    const apiUrl =
      this.restApiUrl +
      apiName +
      `?reminderfor=${reminderFor}&startdate=${startDate}&enddate=${endDate}`;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    // console.log('apiUrl', apiUrl);

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  /**
   * @GK
   * Date: 16-11-2021
   * Get the Lead Follow Up Listings
   * @param token
   * @returns
   */
  public getLeadFollowUpListings(
    token: any,
    reminderFor: number,
    startDate: string,
    endDate: string
  ): Observable<any> {
    const apiName = `api/v1/leads_followup`;
    const apiUrl =
      this.restApiUrl +
      apiName +
      `?followupfor=${reminderFor}&startdate=${startDate}&enddate=${endDate}`;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    // console.log('apiUrl', apiUrl);

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  async showLoader(messageStr = "Please wait...") {
    this.loading = await this.loadingCtrl.create({
      message: messageStr,
      spinner: "bubbles",
    });

    await this.loading.present();
  }

  async dismissLoader() {
    await this.loading.dismiss();
  }

  public getProfileInfo(token: any): Observable<any> {
    const apiName = `api/v1/users/2`;
    const apiUrl = this.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  // list of view category 12-2-22

  public getCategoryInfo(token: any, page: number): Observable<any> {
    const apiName = `api/v1/usercategory`;
    const apiUrl = this.restApiUrl + apiName + `?page=${page}`;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  public getPriceInfo(token: any, id: any): Observable<any> {
    const apiName = `api/v1/products/product-price-history/${id}`;
    const apiUrl = this.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    console.log(apiUrl);
    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  // deletecategory

  public deleteCategory(token: string, id: any): Observable<any> {
    const apiName = `api/v1/usercategory/${id}`;
    const apiUrl = this.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    // console.log('apiUrl', apiUrl);

    return this.http
      .delete(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  public getProduct(token: any, id: any): Observable<any> {
    const apiName = `api/v1/products/update-product-price/${id}`;
    const apiUrl = this.restApiUrl + apiName;

    return this.http
      .get(apiUrl, {
        headers: new HttpHeaders({
          Authorization: token,
        }),
      })
      .pipe(map((response: any) => response));
  }

  public updateItem(token: any, id: any): Observable<any> {
    const apiName = `api/v1/products/${id}`;
    const apiUrl = this.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  // 2-2-2022 update product
  public getUpdateProductInfo(token: any, id: any): Observable<any> {
    const apiName = `api/v1/products/${id}`;
    const apiUrl = this.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  // price update
  public getDailyReportsView(token: any, id: any): Observable<any> {
    const apiName = `api/v1/products/${id}`;
    const apiUrl = this.restApiUrl + apiName;

    return this.http
      .get(apiUrl, {
        headers: new HttpHeaders({
          Authorization: token,
        }),
      })
      .pipe(map((response: any) => response));
  }

  public updatePrice(token: any, id: any, postParams: any): Observable<any> {
    const postData = new FormData();
    postData.append("price", postParams.price);

    const apiName = `api/v1/products/update-product-price`;
    const apiUrl = this.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

    return this.http
      .post(apiUrl, postData, httpOptions)
      .pipe(map((response: any) => response));
  }

  /**
   * @vishakha
   * 02-02-2022
   * Get details of product
   * @param token
   * @param page
   * @returns
   */

  public getProductInfo(
    token: any,
    page: number,
    product_name: string,
    category_id: any,
    hsn_code: any,
    hsn_description: any,
    category: any,
    description: any,
    published: any,
    search: any
  ): Observable<any> {
    const apiName = `api/v1/products`;
    const apiUrl =
      this.restApiUrl +
      apiName +
      `?page=${page}&product_name=${product_name}&category_id=${category_id}&hsn_code=${hsn_code}&hsn_description=${hsn_description}&category=${category}&description=${description}&published=${published}&search=${search}`;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

    console.log("apiUrl", apiUrl);
    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  // my-product delete

  public deleteDailyReport(token: string, id: any): Observable<any> {
    const apiName = `api/v1/products/${id}`;
    const apiUrl = this.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    // console.log('apiUrl', apiUrl);

    return this.http
      .delete(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }
  /**
   * @vishakha
   * 28-02-2022
   * Get details of inquiry
   * @param token
   * @returns
   */
  public getInquiryInfo(token: any): Observable<any> {
    const apiName = `api/v1/inquiry`;
    const apiUrl = this.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }
  /**
   * @vishakha
   * 28-02-2022
   * delete inquiry
   * @param token
   * @param id
   * @returns
   */

  public deleteInquiry(token: string, id: any): Observable<any> {
    const apiName = `api/v1/inquiry/${id}`;
    const apiUrl = this.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    // console.log('apiUrl', apiUrl);

    return this.http
      .delete(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }
  /**
   * @vishakha
   * 01-03-2022
   * delete order
   * @param token
   * @param id
   * @returns
   */

  public deleteOrder(token: string, id: any): Observable<any> {
    const apiName = `api/v1/orders/${id}`;
    const apiUrl = this.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    // console.log('apiUrl', apiUrl);

    return this.http
      .delete(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }
  /**
   * @vishakha
   * 01-03-2022
   * get order list
   * @param token
   * @param id
   * @returns
   */

  public getOrderInfo(token: any, id: any): Observable<any> {
    const apiName = `api/v1/orders/${id}`;
    const apiUrl = this.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }
  /**
   * @vishakha
   * 01-03-2022
   * get order list
   * @param token
   * @param id
   * @returns
   */

  public getProductDetail(token: any, id: any): Observable<any> {
    const apiName = `api/v1/products/${id}`;
    const apiUrl = this.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  public getOrderInfo1(
    token: any,
    page: number,
    product_name: string,
    category: any,
    description: any
  ): Observable<any> {
    const apiName = `api/v1/orders`;
    const apiUrl =
      this.restApiUrl +
      apiName +
      `?page=${page}&product_name=${product_name}&category=${category}&description=${description}`;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

    // console.log(apiUrl);
    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  public updateUser(token: any, id: any): Observable<any> {
    const apiName = `api/v1/users`;
    const apiUrl = this.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  /**
   * @vishakha
   * 07-03-2022
   * Get buyer dashboard
   * @param token
   * @returns
   */
  public getDashboardInfo(token: any): Observable<any> {
    const apiName = `api/v1/products/dashboard`;
    const apiUrl = this.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  /**
   * @vishakha
   * 04-04-2022
   * get latest order list
   * @param token
   * @param id
   * @returns
   */
  public getLatestOrderdInfo(token: any, id: any): Observable<any> {
    const apiName = `api/v1/orders/${id}`;
    const apiUrl = this.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  /**
   * @vishakha
   * 11-04-2022
   * Get details of follower
   * @param token
   * @returns
   */

  public getFollowerInfo(token: any,status: any): Observable<any> {
    const apiName = `api/v1/followers`;
    const apiUrl = this.restApiUrl + apiName +`?status=${status}`;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

    console.log("apiUrl", apiUrl);
    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }
  /**
   * @vishakha
   * 11-04-2022
   * Get details of following
   * @param token
   * @returns
   */

  public getFollowingInfo(token: any): Observable<any> {
    const apiName = `api/v1/following`;
    const apiUrl = this.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

    // console.log('apiUrl', apiUrl);
    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  /**
   * @vishakha
   * 09-04-2022
   * delete details of following
   * @param token
   * @param page
   * @returns
   */
  public deleteFollowing(token: string, id: any): Observable<any> {
    const apiName = `api/v1/following/${id}`;
    const apiUrl = this.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    // console.log('apiUrl', apiUrl);

    return this.http
      .delete(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  public getFollower(token: string): Observable<any> {
    const apiName = `api/v1/followers/status-update`;
    const apiUrl = this.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    // console.log('apiUrl', apiUrl);

    return this.http
      .post(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  /**
   * @vishakha
   * 09-04-2022
   * delete details of following
   * @param token
   * @param page
   * @returns
   */
  public deleteFollower(token: string, id: any): Observable<any> {
    const apiName = `api/v1/followers/${id}`;
    const apiUrl = this.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
    // console.log('apiUrl', apiUrl);

    return this.http
      .delete(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  /**
   * @vishakha
   * 01-03-2022
   * get faq list
   * @param token
   * @returns
   */

  public getFaqInfo(token: any): Observable<any> {
    const apiName = `api/v1/faqs`;
    const apiUrl = this.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

   /**
   * @vishakha
   * 01-03-2022
   * get order list
   * @param token
   * @returns
   */

    public getPageInfo(token: any,id: any): Observable<any> {
      const apiName = `api/v1/pages/1`;
      const apiUrl = this.restApiUrl + apiName;
      const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
  
      return this.http
        .get(apiUrl, httpOptions)
        .pipe(map((response: any) => response));
    }
    
   /**
   * @vishakha
   * 01-03-2022
   * get order list
   * @param token
   * @returns
   */

    public getPrivacyInfo(token: any,id: any): Observable<any> {
      const apiName = `api/v1/pages/2`;
      const apiUrl = this.restApiUrl + apiName;
      const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
  
      return this.http
        .get(apiUrl, httpOptions)
        .pipe(map((response: any) => response));
    }

    public getSearchInfo(token: any,id: any): Observable<any> {
      const apiName = `api/v1/pages/2`;
      const apiUrl = this.restApiUrl + apiName;
      const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };
  
      return this.http
        .get(apiUrl, httpOptions)
        .pipe(map((response: any) => response));
    }

  //  /**
  // * @vishakha
  // * 09-04-2022
  // * Get details of product
  // * @param token
  // * @param page
  // * @returns
  // */

  // public getProductSeller(token: any, page: number): Observable<any> {
  //   const apiName = `api/v1/products`;
  //   const apiUrl = this.restApiUrl + apiName + `?page=${page}`;
  //   const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

  
   /**
  * @vishakha
  * 20-04-2022
  * Get details of setting
  * @param token
  * @param page
  * @returns
  */

  public getSettingInfo(token: any): Observable<any> {
    const apiName = `api/v1/settings`;
    const apiUrl = this.restApiUrl + apiName;
    const httpOptions = { headers: new HttpHeaders({ Authorization: token }) };

    return this.http
      .get(apiUrl, httpOptions)
      .pipe(map((response: any) => response));
  }

  async getDeviceInfo() {
    if (Capacitor.isNativePlatform) {
      return {
        appName: await this.appVersion.getAppName(),
        packageName: await this.appVersion.getPackageName(),
        versionCode: await this.appVersion.getVersionCode(),
        versionNumber: await this.appVersion.getVersionNumber(),
      };
    } else {
      return {};
    }
  }

  async refreshMastersFromStorage(masterList: any) {
    if (masterList) {
      if (this.cityList.length == 0) {
        for (const [inputId, inputName] of Object.entries(masterList?.cities)) {
          this.cityList.push({ id: inputId, name: inputName });
        }
      }
      if (this.stateList.length == 0) {
        for (const [inputId, inputName] of Object.entries(masterList?.states)) {
          this.stateList.push({ id: inputId, name: inputName });
        }
      }
      if (this.categoryList.length == 0) {
        for (const [inputId, inputName] of Object.entries(masterList?.categories)) {
          this.categoryList.push({ id: inputId, name: inputName });
        }
      }
      if (this.unitList.length == 0) {
        for (const [inputId, inputName] of Object.entries(masterList?.units)) {
          this.unitList.push({ id: inputId, name: inputName });
        }
      }
      if (this.statusList.length == 0) {
        for (const [inputId, inputName] of Object.entries(masterList?.orderStatus)) {
          this.statusList.push({ id: inputId, name: inputName });
        }
      }
      if (this.businessTypeList.length == 0) {
        for (const [inputId, inputName] of Object.entries(masterList?.businesstype)) {
          this.businessTypeList.push({ id: inputId, name: inputName });
        }
      }
      if (this.packageTypeList.length == 0) {
        for (const [inputId, inputName] of Object.entries(masterList?.package)) {
          this.packageTypeList.push({ id: inputId, name: inputName });
        }
      }
    }
  }

}
