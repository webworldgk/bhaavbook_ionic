import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthService } from './providers/auth.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full',
  },
  {
    path: 'app-login',
    loadChildren: () => import('./pages/app-login/app-login.module').then((m) => m.AppLoginPageModule),
  },
  {
    path: 'registration-page',
    loadChildren: () => import('./pages/registration-page/registration-page.module').then(m => m.RegistrationPagePageModule)
  },
  {
    path: 'otp-page',
    loadChildren: () => import('./pages/otp-page/otp-page.module').then(m => m.OtpPagePageModule)
  },
  {
    path: 'user-selection-page',
    loadChildren: () => import('./pages/user-selection-page/user-selection-page.module').then(m => m.UserSelectionPagePageModule)
  },
  {
    path: 'package-page',
    loadChildren: () => import('./pages/package-page/package-page.module').then(m => m.PackagePagePageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./pages/buyer/dashboard/dashboard.module').then(m => m.DashboardPageModule),
    canLoad: [AuthService],
  },
  {
    path: 'my-product',
    loadChildren: () => import('./pages/buyer/my-product/my-product.module').then(m => m.MyProductPageModule),
    canLoad: [AuthService],
  },
  {
    path: 'add-category',
    loadChildren: () => import('./pages/buyer/add-category/add-category.module').then(m => m.AddCategoryPageModule),
    canLoad: [AuthService],
  },
  {
    path: 'update-product/:id',
    loadChildren: () => import('./pages/buyer/update-product/update-product.module').then(m => m.UpdateProductPageModule),
    canLoad: [AuthService],
  },
  {
    path: 'registration-page',
    loadChildren: () => import('./pages/registration-page/registration-page.module').then(m => m.RegistrationPagePageModule),
    canLoad: [AuthService],
  },
  {
    path: 'view-category',
    loadChildren: () => import('./pages/buyer/view-category/view-category.module').then(m => m.ViewCategoryPageModule),
    canLoad: [AuthService],
  },
  {
    path: 'today-product',
    loadChildren: () => import('./pages/buyer/today-product/today-product.module').then(m => m.TodayProductPageModule),
    canLoad: [AuthService],
  },
  {
    path: 'update-profile-page',
    loadChildren: () => import('./pages/buyer/update-profile-page/update-profile-page.module').then(m => m.UpdateProfilePagePageModule),
    canLoad: [AuthService],
  },
  {
    path: 'order-list',
    loadChildren: () => import('./pages/buyer/order-list/order-list.module').then(m => m.OrderListPageModule)
  },
  {
    path: 'supports',
    loadChildren: () => import('./pages/buyer/supports/supports.module').then(m => m.SupportsPageModule),
    canLoad: [AuthService],
  },
  {
    path: 'sharepage',
    loadChildren: () => import('./pages/buyer/sharepage/sharepage.module').then(m => m.SharepagePageModule),
    canLoad: [AuthService],
  },
  {
    path: 'aboutpage',
    loadChildren: () => import('./pages/buyer/aboutpage/aboutpage.module').then(m => m.AboutpagePageModule),
    canLoad: [AuthService],
  },
  {
    path: 'settingpage',
    loadChildren: () => import('./pages/buyer/settingpage/settingpage.module').then(m => m.SettingpagePageModule),
    canLoad: [AuthService],
  },
  {
    path: 'follower-page',
    loadChildren: () => import('./pages/buyer/follower-page/follower-page.module').then(m => m.FollowerPagePageModule),
    canLoad: [AuthService],
  },
  {
    path: 'followup-page',
    loadChildren: () => import('./pages/buyer/followup-page/followup-page.module').then(m => m.FollowupPagePageModule),
    canLoad: [AuthService],
  },
  {
    path: 'price-edit-page/:id',
    loadChildren: () => import('./pages/buyer/price-edit-page/price-edit-page.module').then(m => m.PriceEditPagePageModule),
    canLoad: [AuthService],
  },
  {
    path: 'otp-page',
    loadChildren: () => import('./pages/otp-page/otp-page.module').then(m => m.OtpPagePageModule),
    canLoad: [AuthService],
  },
  {
    path: 'user-selection-page',
    loadChildren: () => import('./pages/user-selection-page/user-selection-page.module').then(m => m.UserSelectionPagePageModule),
    canLoad: [AuthService],
  },
  {
    path: 'package-page',
    loadChildren: () => import('./pages/package-page/package-page.module').then(m => m.PackagePagePageModule),
    canLoad: [AuthService],
  },
  {
    path: 'profile',
    loadChildren: () => import('./pages/buyer/profile/profile.module').then(m => m.ProfilePageModule),
    canLoad: [AuthService],
  },
  {
    path: 'add-product',
    loadChildren: () => import('./pages/buyer/add-product/add-product.module').then(m => m.AddProductPageModule),
    canLoad: [AuthService],
  },
  {
    path: 'view-price-history/:id',
    loadChildren: () => import('./pages/buyer/view-price-history/view-price-history.module').then(m => m.ViewPriceHistoryPageModule)
  },
  {
    path: 'add-inquiry/:id',
    loadChildren: () => import('./pages/seller/add-inquiry/add-inquiry.module').then(m => m.AddInquiryPageModule)
  },
  {
    path: 'inquiry-listing',
    loadChildren: () => import('./pages/seller/inquiry-listing/inquiry-listing.module').then(m => m.InquiryListingPageModule)
  },
  {
    path: 'seller-product-listing',
    loadChildren: () => import('./pages/seller/seller-product-listing/seller-product-listing.module').then(m => m.SellerProductListingPageModule)
  },
  {
    path: 'inquiry-list',
    loadChildren: () => import('./pages/buyer/inquiry-list/inquiry-list.module').then(m => m.InquiryListPageModule)
  },
  {
    path: 'my-order',
    loadChildren: () => import('./pages/seller/my-order/my-order.module').then(m => m.MyOrderPageModule)
  },
  {
    path: 'add-order/:id',
    loadChildren: () => import('./pages/seller/add-order/add-order.module').then(m => m.AddOrderPageModule)
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./pages/forgot-password/forgot-password.module').then( m => m.ForgotPasswordPageModule)
  },
  {
    path: 'view-order-history/:id',
    loadChildren: () => import('./pages/buyer/view-order-history/view-order-history.module').then( m => m.ViewOrderHistoryPageModule)
  },
  {
    path:'',
    redirectTo: '/seller-dashboard',
    pathMatch: 'full',
  },
  {
    path: 'seller-dashboard',
    loadChildren: () => import('./pages/seller/seller-dashboard/seller-dashboard.module').then( m => m.SellerDashboardPageModule)
  },
  {
      path: 'reset-password-otp/:mobile_number',
      loadChildren: () => import('./pages/reset-password-otp/reset-password-otp.module').then(m => m.ResetPasswordOtpPageModule)
  },
  {
      path: 'new-password/:mobile_number',
      loadChildren: () => import('./pages/new-password/new-password.module').then(m => m.NewPasswordPageModule)
  },
  {
    path: 'view-product-detail/:id',
    loadChildren: () => import('./pages/buyer/view-product-detail/view-product-detail.module').then( m => m.ViewProductDetailPageModule)
  },
  {
    path: 'notification',
    loadChildren: () => import('./pages/buyer/notification/notification.module').then( m => m.NotificationPageModule)
  },
  {
    path: 'faq-page',
    loadChildren: () => import('./pages/buyer/faq-page/faq-page.module').then( m => m.FaqPagePageModule)
  },
  {
    path: 'privacy-policy-page',
    loadChildren: () => import('./pages/buyer/privacy-policy-page/privacy-policy-page.module').then( m => m.PrivacyPolicyPagePageModule)
  },
  {
    path: 'update-profile',
    loadChildren: () => import('./pages/seller/update-profile/update-profile.module').then((m) => m.UpdateProfilePageModule),
  },
  {
    path: 'privacy-policy',
    loadChildren: () => import('./pages/seller/privacy-policy/privacy-policy.module').then( m => m.PrivacyPolicyPageModule)
  },
  {
    path: 'faq',
    loadChildren: () => import('./pages/seller/faq/faq.module').then( m => m.FaqPageModule)
  },
  {
    path: 'support-page',
    loadChildren: () => import('./pages/seller/support-page/support-page.module').then( m => m.SupportPagePageModule)
  },
  {
    path: 'share-page',
    loadChildren: () => import('./pages/seller/share-page/share-page.module').then( m => m.SharePagePageModule)
  },
  {
    path: 'about-page',
    loadChildren: () => import('./pages/seller/about-page/about-page.module').then( m => m.AboutPagePageModule)
  },
  {
    path: 'setting-page',
    loadChildren: () => import('./pages/seller/setting-page/setting-page.module').then( m => m.SettingPagePageModule)
  },
  {
    path: 'product-list/:id',
    loadChildren: () => import('./pages/seller/product-list/product-list.module').then( m => m.ProductListPageModule)
  },

  // signup process
  {
    path: 'signup',
    loadChildren: () => import('./pages/signup/mobile/mobile.module').then(m => m.MobilePageModule)
  },
  {
    path: 'otp/:mobile',
    loadChildren: () => import('./pages/signup/otp/otp.module').then(m => m.OtpPageModule)
  },
  {
    path: 'personal-info/:id',
    loadChildren: () => import('./pages/signup/personal-info/personal-info.module').then(m => m.PersonalInfoPageModule)
  },
  {
    path: 'business-type/:id',
    loadChildren: () => import('./pages/signup/business-type/business-type.module').then(m => m.BusinessTypePageModule)
  },
  {
    path: 'choose-package/:id',
    loadChildren: () => import('./pages/signup/choose-package/choose-package.module').then(m => m.ChoosePackagePageModule)
  },
  {
    path: 'choose-category/:id',
    loadChildren: () => import('./pages/signup/choose-category/choose-category.module').then(m => m.ChooseCategoryPageModule)
  },
  {
    path: 'address/:id',
    loadChildren: () => import('./pages/signup/address/address.module').then(m => m.AddressPageModule)
  },
  {
    path: 'contacts/:id',
    loadChildren: () => import('./pages/signup/contacts/contacts.module').then(m => m.ContactsPageModule)
  },
  {
    path: 'search-follower',
    loadChildren: () => import('./pages/buyer/search-follower/search-follower.module').then( m => m.SearchFollowerPageModule)
  },
  {
    path: 'confirm-follower',
    loadChildren: () => import('./pages/buyer/confirm-follower/confirm-follower.module').then( m => m.ConfirmFollowerPageModule)
  },
  {
    path: 'follower',
    loadChildren: () => import('./pages/seller/follower/follower.module').then( m => m.FollowerPageModule)
  },
  {
    path: 'following',
    loadChildren: () => import('./pages/seller/following/following.module').then( m => m.FollowingPageModule)
  },
  {
    path: 'follower-panding-request',
    loadChildren: () => import('./pages/seller/follower-panding-request/follower-panding-request.module').then( m => m.FollowerPandingRequestPageModule)
  },
  {
    path: 'view-buyer-detail/:id',
    loadChildren: () => import('./pages/seller/view-buyer-detail/view-buyer-detail.module').then( m => m.ViewBuyerDetailPageModule)
  },
  {
    path: 'view-order-status/:id',
    loadChildren: () => import('./pages/seller/view-order-status/view-order-status.module').then( m => m.ViewOrderStatusPageModule)
  },
  {
    path: 'raise-ticket',
    loadChildren: () => import('./pages/buyer/raise-ticket/raise-ticket.module').then( m => m.RaiseTicketPageModule)
  },
  {
    path: 'raise-ticket-page',
    loadChildren: () => import('./pages/seller/raise-ticket-page/raise-ticket-page.module').then( m => m.RaiseTicketPagePageModule)
  },
  {
    path: 'follower-search',
    loadChildren: () => import('./pages/seller/follower-search/follower-search.module').then( m => m.FollowerSearchPageModule)
  },
  {
    path: 'search-following',
    loadChildren: () => import('./pages/buyer/search-following/search-following.module').then( m => m.SearchFollowingPageModule)
  },
  {
    path: 'seller-profile',
    loadChildren: () => import('./pages/seller/seller-profile/seller-profile.module').then( m => m.SellerProfilePageModule)
  },
  {
    path: 'following-search',
    loadChildren: () => import('./pages/seller/following-search/following-search.module').then( m => m.FollowingSearchPageModule)
  },
  {
    path: 'view-user-detail/:id',
    loadChildren: () => import('./pages/buyer/view-user-detail/view-user-detail.module').then( m => m.ViewUserDetailPageModule)
  },






];



@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
