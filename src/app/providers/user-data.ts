import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
    providedIn: 'root'
})

export class UserData {
    favorites: string[] = [];
    HAS_LOGGED_IN = 'hasLoggedIn';

    constructor(public storage: Storage) { }

    hasFavorite(sessionName: string): boolean {
        return (this.favorites.indexOf(sessionName) > -1);
    }

    addFavorite(sessionName: string): void {
        this.favorites.push(sessionName);
    }

    removeFavorite(sessionName: string): void {
        const index = this.favorites.indexOf(sessionName);
        if (index > -1) {
            this.favorites.splice(index, 1);
        }
    }

    login(username: string, loginResponse: any): Promise<any> {
        return this.storage.set(this.HAS_LOGGED_IN, true).then(() => {
            this.setUsername(username);
            if (loginResponse) {
                if (loginResponse.data) this.setUserInfo(loginResponse.data);
                if (loginResponse.token) this.setToken(loginResponse.token);
            }
            return window.dispatchEvent(new CustomEvent('user:login'));
        });
    }

    signup(username: string): Promise<any> {
        return this.storage.set(this.HAS_LOGGED_IN, true).then(() => {
            this.setUsername(username);
            return window.dispatchEvent(new CustomEvent('user:signup'));
        });
    }

    logout(): Promise<any> {
        return this.storage.remove(this.HAS_LOGGED_IN).then(() => {
            localStorage.clear();
            this.storage.remove('username');
            this.storage.remove('userInfo');
            this.storage.remove('masters');
            this.storage.remove('menuList');
            return this.storage.remove('token');
        }).then(() => {
            window.dispatchEvent(new CustomEvent('user:logout'));
        });
    }

    setUsername(username: string): Promise<any> {
        return this.storage.set('username', username);
    }

    async getUsername(): Promise<string> {
        return this.storage.get('username').then((value) => {
            return value;
        });
    }

    setToken(token: string): Promise<any> {
        const jwtToken = 'Bearer ' + token;
        return this.storage.set('token', jwtToken);
    }

    async getToken(): Promise<string> {
        return this.storage.get('token').then((value) => {
            return value;
        });
    }

    setUserInfo(userInfo: any): Promise<any> {
        return this.storage.set('userInfo', userInfo);
    }

    async getUserInfo(): Promise<any[]> {
        return this.storage.get('userInfo').then((userInfo) => {
            return userInfo;
        });
    }

    async getUserInfoByKey(userKey: string): Promise<string> {
        return this.storage.get('userInfo').then((userInfo) => {
            return (userKey && userInfo[userKey]) ? userInfo[userKey] : '';
        });
    }

    async getUserFullName(): Promise<string> {
        return this.storage.get('userInfo').then((value) => {
            return value?.first_name + ' ' + value?.last_name;
        });
    }

    async getUserAddress(): Promise<string> {
        return this.storage.get('userInfo').then((value) => {
            return value?.address + ' ' + value?.area   + ' ' + value?.city_name + ' ' + value?.state_name +  ' ' + value?.pincode;
        });
    }

    async isLoggedIn(): Promise<boolean> {
        return this.storage.get(this.HAS_LOGGED_IN).then((value) => {
            return value === true;
        });
    }

    saveMasters(masters: any): Promise<any> {
        return this.storage.set('masters', masters);
    }

    setMobilePermission(menuList: any): Promise<any> {
        return this.storage.set('menuList', menuList);
    }

    async getMobilePermission(): Promise<any[]> {
        return await this.storage.get('menuList').then((menuList) => {
            return menuList;
        });
    }

    set(key: string, value: any) {
        this.storage.set(key, value);
    }

    remove(key: string) {
        this.storage.remove(key);
    }

    get(key: string) {
        return this.storage.get(key);
    }

    async getMasters(): Promise<any[]> {
        return await this.storage.get('masters').then((mastersList) => {
            return mastersList;
        });
    }

}
