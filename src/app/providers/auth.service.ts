import { Injectable } from '@angular/core';
import { CanLoad, Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ApiService } from '../service/api.service';
import { UserData } from './user-data';

@Injectable({
    providedIn: 'root'
})

export class AuthService implements CanLoad {

    constructor(
        public api: ApiService,
        private router: Router,
        private storage: Storage,
        private userData: UserData
    ) { }

    canLoad() {
        return this.storage.get(this.userData.HAS_LOGGED_IN).then((value) => {
            if (value && value === true) {
                // this.api.getStart();
                console.log('canLoad', value)
                return true;
            } else {
                this.router.navigateByUrl('/app-login');
                return false;
            }
        });
    }
}
