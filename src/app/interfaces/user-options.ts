
export interface UserOptions {
  loginType: number;
  mobile_number: string;
  password: string;
  mobilePin: string;
  deviceToken: string;
}
