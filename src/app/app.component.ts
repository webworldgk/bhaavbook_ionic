import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SwUpdate } from '@angular/service-worker';
import { AlertController, LoadingController, MenuController, Platform, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Storage } from '@ionic/storage';
import { UserData } from './providers/user-data';
import { ApiService } from './service/api.service';
import { Capacitor } from '@capacitor/core';
import { AppUpdate } from '@ionic-native/app-update/ngx';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {

  loggedIn = false;
  dark = false;
  
  deviceInfo: any;
  appPages:any;

  public appPages1 = [
    
    { title: 'Category', url: '/view-category', icon: 'person-circle'},
    { title: 'Follower', url: '/follower-page', icon: 'document-text' },
    { title: 'Following', url: '/followup-page', icon: 'people' },
    { title: 'Supports', url: '/supports', icon: 'chatbubbles'},
    { title: 'Share App', url: '/sharepage', icon: 'share-social' },
    { title: 'About Us', url: '/aboutpage', icon: 'share' },
    { title: 'FAQ', url: '/faq-page', icon: 'help'},
    { title: 'Privacy-Policy', url: '/privacy-policy-page', icon: 'book' },
    { title: 'Settings', url: '/settingpage', icon: 'settings' },
    
  ];
  
  public appPages2 = [
    
    { title: 'My Account', url: '/update-profile', icon: 'person-circle'},
    { title: 'Follower', url: '/follower', icon: 'document-text' },
    { title: 'Following', url: '/following', icon: 'people' },
    { title: 'Supports', url: '/support-page', icon: 'chatbubbles'},
    { title: 'Share App', url: '/share-page', icon: 'share-social' },
    { title: 'About Us', url: '/about-page', icon: 'share' },
    { title: 'FAQ', url: '/faq', icon: 'help'},
    { title: 'Privacy-Policy', url: '/privacy-policy', icon: 'book' },
    { title: 'Settings', url: '/setting-page', icon: 'settings' },
  ];

  constructor(
    private menu: MenuController,
    private platform: Platform,
    private router: Router,
    public api: ApiService,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storage: Storage,
    private userData: UserData,
    private swUpdate: SwUpdate,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private appUpdate: AppUpdate
  ) {

    
    this.initializeApp();
  }

  async ngOnInit() {
    await this.storage.create();

    this.checkLoginStatus();
    this.listenForLoginEvents();
    this.setUpUserInfo();

    if (await this.userData.isLoggedIn()) {
      this.api.userTypeapp = await this.userData.getUserInfoByKey('user_type');
      this.appPages = (this.api.userTypeapp === 1) ? this.appPages1 : this.appPages2;
      console.log("usertypr",this.api.userTypeapp);
    }

    this.swUpdate.available.subscribe(async (res) => {
      const toast = await this.toastCtrl.create({
        message: 'Update available!',
        position: 'bottom',
        buttons: [
          {
            role: 'cancel',
            text: 'Reload',
          },
        ],
      });

      await toast.present();

      toast
        .onDidDismiss()
        .then(() => this.swUpdate.activateUpdate())
        .then(() => window.location.reload());
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      // const updateUrl = 'https://YOUR_DOMAIN.com/app/APK/my_app_update.xml';
      // this.appUpdate.checkAppUpdate(updateUrl).then(update => {
      //   alert("Update Status:  "+update.msg);
      // }).catch(error=>{
      //   alert("Error: "+error.msg);
      // });
      //this.doubleBackToExitApp();
    });
  }

  doubleBackToExitApp() {
    let counter = 0;
    this.platform.backButton.subscribe(() => {
      counter++;
      if (counter === 1) {
        this.api.presentToast('Press back again to exit Bhaavbook App.', 3000, 'dark');
        return;
      }
      if (counter === 2) {
        navigator['app'].exitApp();
      }
    });
  }

  checkLoginStatus() {
    return this.userData.isLoggedIn().then((loggedIn) => {
      return this.updateLoggedInStatus(loggedIn);
    });
  }

  updateLoggedInStatus(loggedIn: boolean) {
    setTimeout(() => { this.loggedIn = loggedIn; }, 300);
  }

  listenForLoginEvents() {
    window.addEventListener('user:login', () => {
      this.updateLoggedInStatus(true);
    });

    window.addEventListener('user:signup', () => {
      this.updateLoggedInStatus(true);
    });

    window.addEventListener('user:logout', () => {
      this.updateLoggedInStatus(false);
    });
  }

  async setUpUserInfo() {
    if (await this.userData.isLoggedIn()) {
    
      this.api.userName = await this.userData.getUserFullName();
      this.api.userid = await this.userData.getUserInfoByKey('id');
      this.api.userRole = await this.userData.getUserInfoByKey('role');
      this.api.email = await this.userData.getUserInfoByKey('email');
      this.api.userPicture = await this.userData.getUserInfoByKey('picture');
      this.api.userType = await this.userData.getUserInfoByKey('user_type');
      this.api.userTypeName = await this.userData.getUserInfoByKey('user_type_name');
      this.api.mobile_number = await this.userData.getUserInfoByKey('mobile_number');
      this.api.establishdate = await this.userData.getUserInfoByKey('date_of_establish');
      this.api.address = await this.userData.getUserAddress();

      
      console.log("type", this.api.userType);
      if (Capacitor.isNativePlatform) {
        this.deviceInfo = await this.api.getDeviceInfo();
        console.log('deviceInfo', this.deviceInfo);
      }
    }
  }

  async confirmLogout() {
    const alert = await this.alertCtrl.create({
      header: 'Confirm sign out',
      message: 'Are you sure you want to sign out?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: (no) => {
            console.log('Confirm Cancel: ', no);
          }
        }, {
          text: 'Sign Out',
          handler: () => {
            console.log('Confirm Okay');
            this.api.logout();
          }
        }
      ]
    });

    await alert.present();
  }

  ngOnDestroy() {
    this.platform.backButton.unsubscribe();
  }

}
